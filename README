Bounded-Length String Variables for Gecode
==========================================

Adds a BLS string variable type and propagators to Gecode 4.4.0

Installation
============

Build gecode with string variables:

    cd 4.4.0
    ./configure --enable-stringvars --disable-flatzinc
    make

(FlatZinc support for MiniZinc string variables is under construction.)

Usage
=====

To use the new string type, you have to include the following in your source:

    #include <gecode/string.hh>

You can then build and link with Gecode as usual. 

On *nix systems, you need to include the library flag `-lgecodestring` when linking.
If order is important, then place it immediately prior to the `-lgecodeset` flag.

MiniZinc
========

The "experiments/minizinc" folder contains the code used in [1].
See the README file in the corresponding folder.
The executable `fzn2gecode_s` is a small parser for running flatzinc with string predicates
in gecode, using BLS variables for the MiniZinc string variables.


References
==========

[1] Roberto Amadini, Pierre Flener, Justin Pearson, Joseph D. Scott, Peter J.
    Stuckey and Guido Tack. MiniZinc with Strings. LOPSTR 2016.