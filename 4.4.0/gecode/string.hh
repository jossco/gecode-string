/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *  $Date: 2016-04-22 03:57:09 +0200 (Fri, 22 Apr 2016) $ by $Author: jossco $
 *  $Revision: 14979 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __GECODE_STRING_HH__
#define __GECODE_STRING_HH__

#include <climits>
#include <cfloat>
#include <iostream>

#include <vector>
#include <map>

#include <gecode/kernel.hh>
#include <gecode/int.hh>

#ifdef GECODE_HAS_SET_VARS
#include <gecode/set.hh>
#endif

#include <gecode/iter.hh>


/*
 * Configure linking
 *
 */
#if !defined(GECODE_STATIC_LIBS) && \
    (defined(__CYGWIN__) || defined(__MINGW32__) || defined(_MSC_VER))

#ifdef GECODE_BUILD_STRING
#define GECODE_STRING_EXPORT __declspec( dllexport )
#else
#define GECODE_STRING_EXPORT __declspec( dllimport )
#endif

#else

#ifdef GECODE_GCC_HAS_CLASS_VISIBILITY
#define GECODE_STRING_EXPORT __attribute__ ((visibility("default")))
#else
#define GECODE_STRING_EXPORT
#endif

#endif

// Configure auto-linking
#ifndef GECODE_BUILD_STRING
#define GECODE_LIBRARY_NAME "String"
#include <gecode/support/auto-link.hpp>
#endif

/**
 * \namespace Gecode::String
 * \brief Bounded-length strings
 *
 * The Gecode::String namespace contains all functionality required
 * to program propagators and branchers for bounded-length strings,
 * i.e., bounded-length sequences of symbols (mapped to natural numbers).
 * In addition, all propagators and branchers for bounded-length strings
 * provided by %Gecode are contained as nested namespaces.
 *
 */

#include <gecode/string/exception.hpp>

namespace Gecode { namespace String {

  /**
   * \brief Numerical limits for string variables
   *
   * \ingroup TaskModelStringVars
   */
  namespace Limits {
    const int max = (INT_MAX / 2) - 1;
    const int min = 0;
  }
  // namespace Limits {
//     /// Largest allowed integer value
//     const int max = INT_MAX - 1;
//     /// Smallest allowed integer value
//     const int min = -max;
//     /// Infinity for integers
//     const int infinity = max + 1;
//     /// Largest allowed long long integer value
//     const long long int llmax =  LLONG_MAX - 1;
//     /// Smallest allowed long long integer value
//     const long long int llmin = -llmax;
//     /// Infinity for long long integers
//     const long long int llinfinity = llmax + 1;
//     /// Return whether \a n is in range
//     bool valid(int n);
//     /// Return whether \a n is in range
//     bool valid(long long int n);
//     /// Check whether \a n is in range, otherwise throw out of limits with information \a l
//     void check(int n, const char* l);
//     /// Check whether \a n is in range, otherwise throw out of limits with information \a l
//     void check(long long int n, const char* l);
//     /// Check whether \a n is in range and strictly positive, otherwise throw out of limits with information \a l
//     void positive(int n, const char* l);
//     /// Check whether \a n is in range and strictly positive, otherwise throw out of limits with information \a l
//     void positive(long long int n, const char* l);
//     /// Check whether \a n is in range and nonnegative, otherwise throw out of limits with information \a l
//     void nonnegative(int n, const char* l);
//     /// Check whether \a n is in integer range and nonnegative, otherwise throw out of limits exception with information \a l
//     void nonnegative(long long int n, const char* l);
//     /// Check whether adding \a n and \a m would overflow
//     bool overflow_add(int n, int m);
//     /// Check whether adding \a n and \a m would overflow
//     bool overflow_add(long long int n, long long int m);
//     /// Check whether subtracting \a m from \a n would overflow
//     bool overflow_sub(int n, int m);
//     /// Check whether subtracting \a m from \a n would overflow
//     bool overflow_sub(long long int n, long long int m);
//     /// Check whether multiplying \a n and \a m would overflow
//     bool overflow_mul(int n, int m);
//     /// Check whether multiplying \a n and \a m would overflow
//     bool overflow_mul(long long int n, long long int m);
//   }

}}

//#include <gecode/string/limits.hpp>

namespace Gecode { namespace String {
#ifdef GECODE_SUPPORT_MSVC_64
  /// Basetype for bits
  typedef unsigned __int64 BitSet;
#else
  /// Basetype for bits
  typedef unsigned long int BitSet;
#endif
  typedef unsigned int Value;
  
  const unsigned int Numbits = static_cast<unsigned int>(CHAR_BIT * sizeof(BitSet)) - 1;
  const BitSet Empty = static_cast<BitSet>(1UL) << (Numbits);
  const BitSet Full = ~(static_cast<BitSet>(0UL));
    
  enum Status {
    BSS_NONE,
    BSS_ONE,
    BSS_SOME,
    BSS_ALL
  };
  
  extern inline BitSet 
  unionof(BitSet b1, BitSet b2) {
    return b1 | b2 | Empty;
  }
  
  extern inline BitSet 
  interof(BitSet b1, BitSet b2) {
    return (b1 & b2) | Empty;
  }
  
  extern inline BitSet 
  minusof(BitSet b1, BitSet b2) {
    return (b1 & ~b2) | Empty;
  }
  
  extern inline BitSet 
  singletonfor(int v, bool buffered=true) {
    if (v <=0 || static_cast<Value>(v) >= Numbits) return Empty;
    return buffered ? ((static_cast<BitSet>(1UL) << (v-1)) | Empty) : (static_cast<BitSet>(1UL) << (v-1));
  }
  
  extern inline BitSet 
  negationof(BitSet b1) {
    return ~b1 | Empty;
  }
  
  extern inline BitSet
  intervalfor(int l, int u) {
    BitSet dom = Empty;
    for (int i = l; i <= u; i++) {
      dom = unionof(dom,singletonfor(i));
    }
    return dom;
  }
  
  template <class I>
  BitSet
  vals2bitset(I& i) {
    BitSet bs = Empty;
    while(i()) {
      bs = unionof(bs, singletonfor(i.val()));
      ++i;
    }
    return bs;
  }
}}

#include <gecode/string/var-imp.hpp>

namespace Gecode {
  /*
  * String Variable
  *
  */
  class StringVar : public VarImpVar<String::StringVarImp> {

  protected:
    using VarImpVar<String::StringVarImp>::x;
  public:
    StringVar(void) {}
    
    StringVar(const StringVar& y) 
      : VarImpVar<String::StringVarImp>(y.varimp()) {}
    
    StringVar(String::StringVarImp* y) 
      : VarImpVar<String::StringVarImp>(y) {}
    
    GECODE_STRING_EXPORT StringVar(Space& home, 
           int min, 
           int max, 
           int width=String::Numbits, 
           String::BitSet mask=String::Full, 
           int blocksize=16);
    
    // access operations
    bool assigned(void) const;
    int size(void) const;
    bool in(int i, int n) const;
    int charwidth(int i) const;
    bool charfixed(int i) const;
    int charval(int i) const;
    int charmin(int i) const;
    int charmax(int i) const;
    bool lengthfixed(void) const;
    int lengthmin(void) const;
    int lengthmax(void) const;
    int lengthmed(void) const;
    int lengthval(void) const;
    int width(void) const;
    bool mandatory(int l) const;
    bool forbidden(int l) const;
    int next(int i, int v) const;
    String::BitSet domain(int i) const;
  };
  
 /**
  * \brief Print string variable \a x
  * \relates Gecode::StringVar
  */
  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const StringVar& x);
}

#include <gecode/string/view.hpp>

#include <gecode/string/array-traits.hpp>

namespace Gecode {
  
  /// \brief Passing string variables
  class StringVarArgs : public VarArgArray<StringVar> {
  public:
    StringVarArgs(void) {}
    explicit StringVarArgs(int n) : VarArgArray<StringVar>(n) {}
    StringVarArgs(const StringVarArgs& a) : VarArgArray<StringVar>(a) {}
    StringVarArgs(const VarArray<StringVar>& a) : VarArgArray<StringVar>(a) {}
    GECODE_STRING_EXPORT StringVarArgs(Space& home, 
                                       int n, 
                                       int lmin, 
                                       int lmax, 
                                       int width=String::Numbits, 
                                       String::BitSet mask=String::Full, 
                                       int blocksize=16);
  };

  /// \brief string variable array
  class StringVarArray : public VarArray<StringVar> {
  public:
    StringVarArray(void) {}
    StringVarArray(const StringVarArray& a)
      : VarArray<StringVar>(a) {}
    StringVarArray(Space& home, int n)
      : VarArray<StringVar>(home,n) {}
    GECODE_STRING_EXPORT StringVarArray(Space& home, 
                                        int n, 
                                        int lmin, 
                                        int lmax, 
                                        int width=String::Numbits, 
                                        String::BitSet mask=String::Full, 
                                        int blocksize=16);
    GECODE_STRING_EXPORT StringVarArray(Space& home, const StringVarArgs& a)
      : VarArray<StringVar>(home,a) {}
  };
}

#include <gecode/string/array.hpp>

namespace Gecode {
  GECODE_STRING_EXPORT void
  equal(Home home, StringVar _x, StringVar _y);

  GECODE_STRING_EXPORT void
  equal(Home home, StringVar _x, IntArgs _y);
  
  GECODE_STRING_EXPORT void
  equal(Home home, IntArgs _x, StringVar _y);
  
  GECODE_STRING_EXPORT void
  equal(Home home, StringVar _x, StringVar _y, BoolVar _b);
  
  GECODE_STRING_EXPORT void
  equal(Home home, StringVar _x, const IntArgs& _y, BoolVar _b);
  
  GECODE_STRING_EXPORT void
  equal(Home home, const IntArgs& _x, StringVar _y, BoolVar _b);
  
  GECODE_STRING_EXPORT void 
  nequal(Home home, StringVar _x, StringVar _y);
  
  GECODE_STRING_EXPORT void
  nequal(Home home, StringVar _x, IntArgs _y);
  
  GECODE_STRING_EXPORT void
  nequal(Home home, IntArgs _x, StringVar _y);
  
  GECODE_STRING_EXPORT void
  length(Home home, StringVar _x, IntVar _l);
  
  GECODE_STRING_EXPORT void
  length(Home home, StringVar _x, int l);
  
  GECODE_STRING_EXPORT void
  length(Home home, const StringVarArgs& x, const IntVarArgs& l);

  GECODE_STRING_EXPORT void 
  concat(Home home, StringVar x, StringVar y, StringVar z);
  
  GECODE_STRING_EXPORT void 
  concat(Home home, StringVar x, StringVar y, const IntArgs& z);
  
  GECODE_STRING_EXPORT void 
  concat(Home home, StringVar x, const IntArgs& y, StringVar z);
  
  GECODE_STRING_EXPORT void 
  concat(Home home, const IntArgs& x, StringVar y, StringVar z);
  
  GECODE_STRING_EXPORT void
  channel(Home home, StringVar x, int index, IntVar i);

  GECODE_STRING_EXPORT void
  channel(Home home, StringVar x, int index, int i);
  
  GECODE_STRING_EXPORT void
  channel(Home home, StringVar x, int index, IntVar i, BoolVar b);
  
	GECODE_STRING_EXPORT void
  channel(Home home, StringVar x, int index, int i, BoolVar b);
  
 /** \brief Post propagator for \f$c = x[i]\f$
  *
  * If \f$ i > max(|x|)\f$, then \a c = 0.
  */
  GECODE_STRING_EXPORT void
  character(Home home, StringVar x, IntVar c, IntVar i);
  
 /** \brief Post propagator for \f$c = x[i]\f$
  *
  * If \f$ i > max(|x|)\f$, then \a c = 0.
  */
  GECODE_STRING_EXPORT void
  character(Home home, StringVar x, int c, IntVar i);
  
 /** \brief Post propagator for \f$c = x[i]\f$
  *
  * If \f$ i > max(|x|)\f$, then \a c = 0.
  */
  GECODE_STRING_EXPORT void
  character(Home home, StringVar x, IntVar c, int i);
  
 /** \brief Post propagator for \f$c = x[i]\f$
  *
  * If \f$ i > max(|x|)\f$, then \a c = 0.
  */
  GECODE_STRING_EXPORT void
  character(Home home, StringVar x, int c, int i);
  
 /** \brief Post propagator for \f$c = x[i]\f$
  *
  * If \f$ i > max(|x|)\f$, then \a c = 0.
  */
  GECODE_STRING_EXPORT void
  character(Home home, IntArgs& x, IntVar c, IntVar i);
  
 /** \brief Post propagator for \f$c = x[i]\f$
  *
  * If \f$ i > max(|x|)\f$, then \a c = 0.
  */
  GECODE_STRING_EXPORT void
  character(Home home, IntArgs& x, IntVar c, int i);
  
 /** \brief Post propagator for \f$c = x[i]\f$
  *
  * If \f$ i > max(|x|)\f$, then \a c = 0.
  */
  GECODE_STRING_EXPORT void
  character(Home home, IntArgs& x, int c, IntVar i);

  GECODE_STRING_EXPORT void 
  reverse(Home home, StringVar x, StringVar y);
  
  GECODE_STRING_EXPORT void 
  reverse(Home home, StringVar x, const IntArgs& y);
  
  GECODE_STRING_EXPORT void 
  reverse(Home home, const IntArgs& x, StringVar y);
  
 /** \brief Post propagator for \a d is the distance between \a x and \a y 
  *
  * \a d is the Hamming distance between the smaller of either \a x or \a y, 
  * and the same-sized prefix of the other
  */
  GECODE_STRING_EXPORT void
  distance(Home home, StringVar x, StringVar y, IntRelType irt, int d);
  
 /** \brief Post propagator for \a x is a morphism of \a y
  *
  * Forall \f$0 \leq i < |x|\f$: if \f$x[i] == from[j]\f$
  * then \f$y[i] == to[j]\f$.
  */
  GECODE_STRING_EXPORT void 
  morph(Home home, StringVar _x, StringVar _y, const IntArgs& from, const IntArgs& to);
  
 /** \brief Post propagator for \f$x[i:i+|y|-1] = y\f$
  *
  * If \f$ |y| = 0 \f$, then the propagator only ensures 
  * that \f$i < max(|x|)\f$.
  */
  GECODE_STRING_EXPORT void
  substring(Home home, StringVar x, StringVar y, IntVar i);
  
 /** \brief Post propagator for \f$x[i:i+|y|-1] = y\f$
  *
  * If \f$ |y| = 0 \f$, then the propagator only ensures 
  * that \f$i < max(|x|)\f$.
  */
  GECODE_STRING_EXPORT void
  substring(Home home,const IntArgs& x, IntVar y, IntVar i);
  
 /** \brief Post propagator for \f$x[i:i+|y|-1] = y\f$
  *
  * If \f$ |y| = 0 \f$, then the propagator only ensures 
  * that \f$i < max(|x|)\f$.
  */
  GECODE_STRING_EXPORT void
  substring(Home home, StringVar x, const IntArgs& y, IntVar i);
  
 /** \brief Post propagator for \f$x[i:i+|y|-1] = y\f$
  *
  * If \f$ |y| = 0 \f$, then the propagator only ensures 
  * that \f$i < max(|x|)\f$.
  */
  GECODE_STRING_EXPORT void
  substring(Home home, StringVar x, StringVar y, int i);
  
 /** \brief Post propagator for \f$x[i:i+|y|-1] = y\f$
  *
  * If \f$ |y| = 0 \f$, then the propagator only ensures 
  * that \f$i < max(|x|)\f$.
  */
  GECODE_STRING_EXPORT void
  substring(Home home,const IntArgs& x, IntVar y, int i);
  
 /** \brief Post propagator for \f$x[i:i+|y|-1] = y\f$
  *
  * If \f$ |y| = 0 \f$, then the propagator only ensures 
  * that \f$i < max(|x|)\f$.
  */
  GECODE_STRING_EXPORT void
  substring(Home home, StringVar x, const IntArgs& y,int i);
  
  GECODE_STRING_EXPORT void
  extensional(Home home, StringVar x, DFA dfa);
  
  GECODE_STRING_EXPORT void
  extensional(Home home, IntArgs x, DFA dfa);

	//#ifdef GECODE_HAS_SET_VARS
 /// Post propagator for \a alpha is the alphabet of \a x.
  // GECODE_STRING_EXPORT void
  // alphabet(Home home, StringVar x, IntSet alpha);
	//#endif
  
 /** \brief Post propagator for [\a l,\a u] is the alphabet of \a x.
  *
  * If \a strict, then the interval is exactly the alphabet of \a x;
  * otherwise, only states that all characters in \a x take some symbol
  * from the interval.
  */
  GECODE_STRING_EXPORT void
  alphabet(Home home, StringVar x, int l, int u, bool strict=false);
  
 /** \brief Post propagator for \a d is the alphabet of \a x.
  *
  * If \a strict, then \a d is exactly the alphabet of \a x;
  * otherwise, only states that all characters in \a x take some symbol
  * from \a d.
  */
  GECODE_STRING_EXPORT void
  alphabet(Home home, StringVar x, const IntSet& d, bool strict=false);
}

namespace Gecode { namespace String {
  enum GECODE_STRING_EXPORT VarSelect        {VAR_NONE=0, 
                         VAR_WIDTH_MIN=2, VAR_WIDTH_MAX=3,
                       
                         VAR_CHARWIDTH_MIN=4, VAR_CHARWIDTH_MAX=5,
                       
                         VAR_LENGTH_MIN_MIN=8, VAR_LENGTH_MIN_MAX=9, 
                         VAR_LENGTH_MAX_MIN=10, VAR_LENGTH_MAX_MAX=11,
                        
                         VAR_LENGTH_WIDTH_MIN=12, VAR_LENGTH_WIDTH_MAX=13
                         };
  enum IndexSelect      {IND_NONE, IND_WIDTH_MIN, IND_WIDTH_MAX};
  enum CharValueSelect  {CHAR_VAL_MIN, CHAR_VAL_MAX, CHAR_VAL_SPLIT};
  enum LengthSelect     {LEN_MIN, LEN_MAX, LEN_MED};
}}

namespace Gecode {
  GECODE_STRING_EXPORT void 
  mandatory(Home home, StringVarArgs& x, String::VarSelect vs, String::IndexSelect is, String::CharValueSelect cvs, String::LengthSelect ls);
  
  GECODE_STRING_EXPORT void 
  mandatory(Home home, StringVarArray& x, String::VarSelect vs, String::IndexSelect is, String::CharValueSelect cvs, String::LengthSelect ls);
}

#endif