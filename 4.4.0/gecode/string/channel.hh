/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-22 03:57:09 +0200 (Fri, 22 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14979 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __GECODE_STRING_CHANNEL_HH__
#define __GECODE_STRING_CHANNEL_HH__

#include <gecode/string.hh>

namespace Gecode { namespace String { namespace Channel {
  template<class SView, class IView>
  class Base : public Propagator {
  protected:
    SView x; 
    IView i;
    unsigned int index;
    Council<Advisor> c;
  public:
    Base(Home home, SView x, int ind, IView i);
    Base(Home home, bool share, Base& p);
    virtual size_t dispose(Space& home);
    static ExecStatus post(Space& home, SView x, int ind, IView i);
    virtual ExecStatus propagate(Space& home, const ModEventDelta& med);
    virtual ExecStatus advise(Space&, Advisor&, const Delta& d);
    virtual Propagator* copy(Space& home, bool share){
      return new (home) Base(home,share, *this); 
    }
    virtual PropCost cost(const Space&, const ModEventDelta&) const{
      return PropCost::linear(PropCost::LO,i.size());
    }
  };
  
  template<class SView, class IView, class BView>
  class Reif : public Propagator {
  protected:
    SView x; 
    IView i;
    BView b;
    unsigned int index;
    Council<Advisor> c;
  public:
    Reif(Home home, SView x, int ind, IView i, BView b);
    Reif(Home home, bool share, Reif& p);
    virtual size_t dispose(Space& home);
    static ExecStatus post(Space& home, SView x, int ind, IView i, BView b);
    virtual ExecStatus propagate(Space& home, const ModEventDelta& med);
    virtual ExecStatus advise(Space&, Advisor&, const Delta& d);
    virtual Propagator* copy(Space& home, bool share){
      return new (home) Reif(home,share, *this); 
    }
    virtual PropCost cost(const Space&, const ModEventDelta&) const{
      return PropCost::linear(PropCost::LO,i.size());
    }
  };
}}}

#include <gecode/string/channel/base.hpp>
#include <gecode/string/channel/reif.hpp>
#endif