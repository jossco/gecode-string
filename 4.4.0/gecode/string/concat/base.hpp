/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Concat {

  template<class V0, class V1, class V2>
  Base<V0,V1,V2>::Base(Home home, V0 _X, V1 _Y, V2 _Z)
  : Propagator(home), X(_X), Y(_Y), Z(_Z) {
    X.subscribe(home, *this, PC_STRING_ANY);
    Y.subscribe(home, *this, PC_STRING_ANY);
    Z.subscribe(home, *this, PC_STRING_ANY);
  }
  
  template<class V0, class V1, class V2>
  Base<V0,V1,V2>::Base(Home home, bool share, Base<V0,V1,V2>& p)
  : Propagator(home, share, p) {
    X.update(home, share, p.X);
    Y.update(home, share, p.Y);
    Z.update(home, share, p.Z);
  }
  
  template<class V0, class V1, class V2>
  size_t 
  Base<V0,V1,V2>::dispose(Space& home) {
    X.cancel(home, *this, PC_STRING_ANY);
    Y.cancel(home, *this, PC_STRING_ANY);
    Z.cancel(home, *this, PC_STRING_ANY);
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }
  
  template<class V0, class V1, class V2>
  ExecStatus 
  Base<V0,V1,V2>::post(Space& home, V0 X, V1 Y, V2 Z) {
    GECODE_ME_CHECK(Z.llq(home,X.lengthmax()+Y.lengthmax()));
    GECODE_ME_CHECK(Z.lgq(home,X.lengthmin()+Y.lengthmin()));
    
  	GECODE_ME_CHECK(X.llq(home,Z.lengthmax()-Y.lengthmin()));
  	GECODE_ME_CHECK(X.lgq(home,Z.lengthmin()-Y.lengthmax()));
    
  	GECODE_ME_CHECK(Y.llq(home,Z.lengthmax()-X.lengthmin()));
  	GECODE_ME_CHECK(Y.lgq(home,Z.lengthmin()-X.lengthmax()));
    
    // X[0]..X[Xmin] = Z[0]..Z[Xmin]
    for (int i = 0; i < X.lengthmin(); i++) {
      GECODE_ME_CHECK(X.inter(home,i,Z.domain(i)));
      GECODE_ME_CHECK(Z.inter(home,i,X.domain(i)));
    }
    
  	(void) new (home) Base(home,X, Y, Z);
  	return ES_OK;
  }
  
  template<class V0, class V1, class V2>
  ExecStatus 
  Base<V0,V1,V2>::propagate(Space& home, const ModEventDelta&) {
    // basic length checks
    GECODE_ME_CHECK(Z.llq(home,X.lengthmax()+Y.lengthmax()));
    GECODE_ME_CHECK(Z.lgq(home,X.lengthmin()+Y.lengthmin()));
    
  	GECODE_ME_CHECK(X.llq(home,Z.lengthmax()-Y.lengthmin()));
  	GECODE_ME_CHECK(X.lgq(home,Z.lengthmin()-Y.lengthmax()));
    
  	GECODE_ME_CHECK(Y.llq(home,Z.lengthmax()-X.lengthmin()));
  	GECODE_ME_CHECK(Y.lgq(home,Z.lengthmin()-X.lengthmax()));
    
    // Y, feasible alignment:
    //   for i in 0..Ymin-1:
    //     Y[i] inter Z[i+Xmin] == {}--> |X| > Xmin
    bool realign = true;
    while (realign) {
      realign = false;
    for(int i = 0; i < Y.lengthmin(); i++) {
        if (interof(Y.domain(i),Z.domain(i+X.lengthmin())) == Empty) {
          GECODE_ME_CHECK_MODIFIED(realign,X.lgr(home,X.lengthmin()));
          break;
        }
      }
      if(realign) {
        GECODE_ME_CHECK(Z.lgq(home,X.lengthmin()+Y.lengthmin()));
        GECODE_ME_CHECK(Y.llq(home,Z.lengthmax()-X.lengthmin()));
      }
    }
    //     Y[i] inter Z[i+Xmax] == {}--> |X| < Xmax
    realign = true;
    while (realign) {
      realign = false;
      // FIXME: looping to lmax should be valid, BUT IT IS NOT
      //for(int i = 0; i < Y.lengthmax(); i++) {
      for(int i = 0; i < Y.lengthmin(); i++) {
        if (interof(Y.domain(i),Z.domain(i+X.lengthmax())) == Empty) {
          GECODE_ME_CHECK_MODIFIED(realign,X.lle(home,X.lengthmax()));
          break;
        }
      }
      if(realign) {
        GECODE_ME_CHECK(Z.llq(home,X.lengthmax()+Y.lengthmax()));
        GECODE_ME_CHECK(Y.lgq(home,Z.lengthmin()-X.lengthmax()));
      }
    }
    
  	assert(X.lengthmax() + Y.lengthmax() >= Z.lengthmax());
  	assert(X.lengthmin() + Y.lengthmin() <= Z.lengthmin());
    
    // X:
    //   X[i] in Z[i]
    for (int i = 0; i < X.lengthmax(); i++)
      GECODE_ME_CHECK(X.inter(home,i,Z.domain(i)));
    
    // Z, part 1:
    //   for i in 0..Xmin-1: Z[i] in X[i]
    for (int i = 0; i < X.lengthmin(); i++) {
      GECODE_ME_CHECK(Z.inter(home,i,X.domain(i)));
    }
    
    // Z, part 2:
    //  for i in Xmin..Zmax-1 : 
    //    Z[i] in (X[i] if i < Xmax) union 
    //      (union | j in i-Xmax..i-Xmin : Y[j])
    for (int i = X.lengthmin(); i < Z.lengthmax(); i++) {
      BitSet dom = Empty;
      if (i < X.lengthmax())
        dom = X.domain(i);
      
      int lb = std::max(i - X.lengthmax(), 0);
      int ub = std::min(i - X.lengthmin(),Y.lengthmax()-1);
      
      for (int j = lb; j <= ub; j++) {
        dom = unionof(dom,Y.domain(j));
      }
      GECODE_ME_CHECK(Z.inter(home,i,dom));
    }
    
    // Y, part 1:
    //  for i in 0..Ymax-1:
    //    Y[i] in (union | j in Xmin+i..Xmax+i : Z[Xmin+i])
    for(int i = 0; i < Y.lengthmax(); i++) {
      BitSet dom = Empty;
      int lb = X.lengthmin() + i;
      int ub = std::min(X.lengthmax() + i, Z.lengthmax() - 1);
      for (int j = lb; j <= ub; j++) {
        dom = unionof(dom,Z.domain(j));
      }
      GECODE_ME_CHECK(Y.inter(home,i,dom));
    } 
    
    return ES_NOFIX;
  }

}}}