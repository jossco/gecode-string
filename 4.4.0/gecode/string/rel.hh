/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 08:20:30 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14968 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __GECODE_STRING_REL_HH__
#define __GECODE_STRING_REL_HH__

#include <gecode/string.hh>

namespace Gecode { namespace String { namespace Rel {
  template<class V0, class V1>
  class Equal : 
    public MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY> {
  protected:
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x0;
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x1;
    
    /// Constructor for cloning \a p
    Equal(Home home, bool share, Equal<V0,V1>& p);
    
  public:
    /// Constructor for posting
    Equal(Home home, V0 x0, V1 x1);
    
    /// Post propagator \f$ x_0 = x_1\f$
    static ExecStatus post(Space& home, V0 x0, V1 x1);
    
    /// Perform propagation
    virtual ExecStatus propagate(Space& home, const ModEventDelta& med);

    /// Copy propagator during cloning
    virtual Actor* copy(Space& home, bool share);

    /// Cost function
    virtual PropCost cost(const Space& home, const ModEventDelta& med) const;
  };
  
  template<class V0, class V1>
  class Nequal : 
    public MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY> {
  protected:
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x0;
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x1;
    int i;
    
    /// Constructor for cloning \a p
    Nequal(Home home, bool share, Nequal<V0,V1>& p);
    
  public:
    /// Constructor for posting
    Nequal(Home home, V0 x0, V1 x1);
    
    /// Post propagator \f$ x_0 = x_1\f$
    static ExecStatus post(Space& home, V0 x0, V1 x1);
    
    /// Perform propagation
    virtual ExecStatus propagate(Space& home, const ModEventDelta& med);

    /// Copy propagator during cloning
    virtual Actor* copy(Space& home, bool share);

    /// Cost function
    virtual PropCost cost(const Space& home, const ModEventDelta& med) const;
  };
  
  template<class V0, class V1, class VB>
  class ReEqual : public Propagator {
  protected:
    V0 x; V1 y; VB b;
  public:
    ReEqual(Home home, V0 x, V1 y, VB b);
    ReEqual(Home home, bool share, ReEqual& p);
    virtual size_t dispose(Space& home);
    static ExecStatus post(Space& home, V0 x, V1 y, VB b);
    virtual ExecStatus propagate(Space& home, const Gecode::ModEventDelta& med);
    virtual Propagator* copy(Space& home, bool share){
      return new (home) ReEqual(home,share, *this); 
    }
    virtual PropCost cost(const Space&, const ModEventDelta&) const{
      return PropCost::binary(PropCost::LO);
    }
  };
}}}


#include <gecode/string/rel/eq.hpp>
#include <gecode/string/rel/nq.hpp>
#include <gecode/string/rel/reif.hpp>
#endif
