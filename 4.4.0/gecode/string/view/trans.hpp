/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-04 14:20:39 +0200 (Mon, 04 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14946 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String {
  
  forceinline ModEvent
  TransStrView::me_decode(ModEvent me) {
    
  }
  
  forceinline ModEvent
  TransStrView::me_encode(ModEvent me) {
    switch(me) {
      case ME_STR_CD:
      BitSetIter iter(encode)
    case ME_STR_FAILED:
    case ME_STR_NONE:
    case ME_STR_VAL:
    case ME_STR_CV:
    case ME_STR_CVLL:
      return me;
    default:
      return Gecode::Int::ME_INT_DOM;
    }
  }
  
  
  forceinline int 
  TransStrView::size(void) const {
    return x->size();
  }
  
  forceinline int 
  TransStrView::charwidth(int i) const {
    BitSetIter xit(x->domain(i));
    int count = 0;
    for(BitSetIter yit(encode_v(xit)); yit(); ++yit)
      count++;
    return count;
  }
  
  forceinline bool 
  TransStrView::mandatory(int l) const {
    return x->mandatory(l);
  }
  
  forceinline bool
  TransStrView::forbidden(int l) const {
    return x->forbidden(l);
  }
  
  forceinline int
  TransStrView::encode(int v) const {
    return map.left.at(v);
  }
  
  forceinline BitSet
  TransStrView::decode(int v) const {
    BitSet tdom = Empty;
    for(mtype::right_const_iterator iter = map.right.lowerbound(v), 
                                    end = map.right.upperbound(v);
        iter != end;
        ++iter) {
      tdom = unionof(tdom,singletonfor(iter->second));
    }
    return tdom;
  }
  
  // access operations
  forceinline BitSet 
  TransStrView::domain(int i) const {
    BitSetIter iter(x->domain(i))
    return encode(iter);
  }
  
  forceinline bool 
  TransStrView::assigned(void) const {
    if (!lengthfixed())
      return false;
    for(int i = 0; i < lmin; i++)
      
      if (!character.singleton(i))
        return false;
    return true;
  }
  
  forceinline bool 
  TransStrView::in(int i, int v) const {
    if (interof(decode(v), x->domain(i)) == Empty)
      return false;
    return true;
  }
  
  forceinline bool 
  TransStrView::charfixed(int i) const {
    if (i < lmin) {
      BitSetIter xit(x->domain(i));
      BitSetIter yit(decode_v(xit));
      if (yit()) {
        ++yit;
        return yit()?false:true;
      }
    }
    return false;
  }
  
  forceinline int 
  TransStrView::charval(int i) const {
    // TODO: this should fail if i not a singleton
    BitSetIter xit(x->domain(i));
    BitSetIter yit(decode_v(xit));
    return yit.val();
  }
  
  forceinline int 
  TransStrView::charmin(int i) const {
    BitSetIter iter(x->domain(i));
    BitSetIter dom(decode_v(iter));
    return dom.val();
  }
  
  forceinline bool 
  TransStrView::lengthfixed(void) const {
    return x->lengthfixed();
  }
  
  forceinline int 
  TransStrView::lengthmin(void) const {
    return x->lengthmin();
  }
  
  forceinline int 
  TransStrView::lengthmax(void) const {
    return x->lengthmax();
  }
  
  forceinline int 
  TransStrView::lengthmed(void) const {
    return x->lengthmed();
  }
  
  forceinline int 
  TransStrView::lengthval(void) const {
    return x->lengthval();
  }
  
  forceinline int 
  TransStrView::width(void) const {
    return x->width();
  }
  
  // length modification operations
  forceinline ModEvent 
  TransStrView::lle(Space& home, int n) {
    return x->lle(home,n);
  }
  
  forceinline ModEvent 
  TransStrView::llq(Space& home, int n) {
    return x->llq(home,n);
  }
  
  forceinline ModEvent 
  TransStrView::lgr(Space& home, int n) {
    return x->lgr(home,n);
  }
  
  forceinline ModEvent 
  TransStrView::lgq(Space& home, int n) {
    return x->lgq(home,n);
  }
    
  // character modification operations
  forceinline ModEvent 
  TransStrView::ceq(Space& home, int i, int n) {
    ModEvent me_len = x->lgr(home,i);
    if (me_len == ME_STR_FAILED) return me;
    
    ModEvent me_char = x->inter(home,i,encode(n));
    switch (me_char) {
      case ME_STR_NONE: return me_len;
      case ME_STR_CD:
      case ME_STR_CV:
      if (me_len == ME_STR_LL || me_len == ME_STR_CVLL)
        return ME_STR_CVLL;
      else
        return ME_STR_CLV;
      default: return me_char;
    }
    GECODE_NEVER; return ME_STR_NONE;
  }
  
  forceinline ModEvent 
  TransStrView::cnq(Space& home, int i, int n) {
    return x->minus(home,i,encode(n));
  }
  
  forceinline ModEvent 
  TransStrView::inter(Space& home, int index, BitSet b) {
    BitSetIter iter(b);
    return x->inter(home,index,encode_v(iter));
  }
  
  
  forceinline ModEvent 
  TransStrView::minus(Space& home, int index, BitSet b) {
    BitSetIter iter(b);
    return x->minus(home,index,encode_v(iter));
  }
  
    
  // delta information
  forceinline int 
  TransStrView::length_min(const Delta& d) const {
    return x->length_min(d);
  }
  forceinline int 
  TransStrView::length_max(const Delta& d) const {
    return x->length_max(d);
  }
  
  forceinline int 
  TransStrView::index(const Delta& d) const {
    return x->index(d);
  }
  
  forceinline bool 
  TransStrView::any(const Delta& d) const {
    return x->any(d);
  }
  
  forceinline bool 
  TransStrView::length(const Delta& d) const {
    return x->length(d);
  }
  
  forceinline bool 
  TransStrView::symbol(const Delta& d) const {
    return x->symbol(d);
  }


}}