/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 08:20:30 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14968 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String {
  
  forceinline int 
  ConstStringView::size(void) const {
    return sz;
  }
  
  forceinline int 
  ConstStringView::charwidth(int i) const {
    if (i < sz)
      return 1;
    return 0;
  }
  
  forceinline bool 
  ConstStringView::mandatory(int i) const {
    if (i < sz)
      return true;
    return false;
  }
  
  forceinline bool
  ConstStringView::forbidden(int i) const {
    return !mandatory(i);
  }

  
  // access operations
  forceinline BitSet 
  ConstStringView::domain(int i) const {
    assert(i >= 0);
    if (i >= sz)
      return Empty;
    return singletonfor(sym[i]);
  }
    
  forceinline bool 
  ConstStringView::assigned(void) const {
    return true;
  }
  
  forceinline bool 
  ConstStringView::in(int i, int n) const {
    if (i < sz) 
      return n == sym[i];
    return false;
  }
  
  forceinline bool 
  ConstStringView::charfixed(int) const {
    return true;
  }
  
  forceinline int 
  ConstStringView::charval(int i) const {
    assert(i < sz);
    return sym[i];
  }
  
  forceinline int 
  ConstStringView::charmin(int i) const {
    assert(i < sz);
    return sym[i];
  }
  
  forceinline bool 
  ConstStringView::lengthfixed(void) const {
    return true;
  }
  
  forceinline int 
  ConstStringView::lengthmin(void) const {
    return sz;
  }
  
  forceinline int 
  ConstStringView::lengthmax(void) const {
    return sz;
  }
  
  forceinline int 
  ConstStringView::lengthmed(void) const {
    return sz;
  }
  
  forceinline int 
  ConstStringView::lengthval(void) const {
    return sz;
  }
  
  forceinline int 
  ConstStringView::width(void) const {
    return Numbits;
  }
  
  // length modification operations
  forceinline ModEvent 
  ConstStringView::lle(Space&, int n) {
    return (n > sz) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstStringView::llq(Space&, int n) {
    return (n >= sz) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstStringView::lgr(Space&, int n) {
    return (n < sz) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstStringView::lgq(Space&, int n) {
    return (n <= sz) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
    
  // character modification operations
  forceinline ModEvent 
  ConstStringView::ceq(Space&, int i, int n) {
    if (i < sz) 
      return (n == sym[i])? ME_STRING_NONE : ME_STRING_FAILED;
    return ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstStringView::cnq(Space&, int i, int n) {
    if (i < sz) 
      return (n == sym[i])? ME_STRING_FAILED : ME_STRING_NONE;
    return ME_STRING_NONE;
  }
  
  forceinline ModEvent 
  ConstStringView::inter(Space&, int index, BitSet b) {
    if (index < sz) 
      return interof(singletonfor(sym[index]),b) != Empty;
    return ME_STRING_NONE;
  }
  
  
  forceinline ModEvent 
  ConstStringView::minus(Space&, int index, BitSet b) {
    if (index < sz) 
      return minusof(singletonfor(sym[index]),b) != Empty;
    return ME_STRING_NONE;
  }
  
  forceinline void 
  ConstStringView::update(Space& home, bool share, ConstStringView& y) {
    ConstView<StringView>::update(home,share,y);
    //if (sz > 0)
    //  home.free<int>(sym,sz);
    
    sz = y.sz;
    if (sz > 0) {
      sym = home.alloc<int>(sz);
      for(int i = sz; i--; ) {
        assert(i >= 0);
        assert(i < y.sz);
        sym[i] = y.sym[i];
      }
    } else {
      sym = NULL;
    }
  }
  
  // delta information
  forceinline ModEvent
  ConstStringView::modevent(const Delta&) {
    GECODE_NEVER; return 0;
  }
  
  forceinline int 
  ConstStringView::length_min(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  forceinline int 
  ConstStringView::length_max(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline int 
  ConstStringView::index(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline bool 
  ConstStringView::any(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline bool 
  ConstStringView::length(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline bool 
  ConstStringView::symbol(const Delta&) const {
    GECODE_NEVER; return 0;
  }

  // view tests
  forceinline bool 
  same(const ConstStringView& x, const ConstStringView& y) {
    return x.charmin(0) == y.charmin(0);
  }
  forceinline bool 
  before(const ConstStringView& x, const ConstStringView& y) {
    return x.charmin(0) < y.charmin(0);
  }
  
  template<class I>
  forceinline ModEvent 
  ConstStringView::inter_v(Space&, int index, I& i, bool) {
    if (index >= sz) return ME_STRING_NONE;
    while(i()) {
      if (i.val() == sym[index])
        return ME_STRING_NONE;
      if (i.val() > sym[index])
        return ME_STRING_FAILED;
      ++i;
    }
    return ME_STRING_FAILED;
  }
}}