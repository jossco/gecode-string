/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String {
  
  forceinline PropCond
  CharacterView::pc_strtoint(PropCond pc) {
    switch(pc) {
    case PC_STRING_VAL:
    case PC_STRING_CVAL:
    case PC_STRING_CLVAL:
      return Gecode::Int::PC_INT_VAL;
    case PC_STRING_CDOM:
    case PC_STRING_ANY:
      return Gecode::Int::PC_INT_DOM;
    default:
      return Gecode::Int::PC_INT_NONE;
    }
  }

  forceinline ModEvent
  CharacterView::me_inttostr(ModEvent me) {
    switch(me) {
    case Gecode::Int::ME_INT_FAILED:
      return ME_STRING_FAILED;
    case Gecode::Int::ME_INT_NONE:
      return ME_STRING_NONE;
    case Gecode::Int::ME_INT_VAL:
      return ME_STRING_VAL;
    case Gecode::Int::ME_INT_DOM:
    default:
      return ME_STRING_CD;
    }
  }

  forceinline ModEvent
  CharacterView::me_strtoint(ModEvent me) {
    switch(me) {
    case ME_STRING_FAILED:
      return Gecode::Int::ME_INT_FAILED;
    case ME_STRING_NONE:
      return Gecode::Int::ME_INT_NONE;
    case ME_STRING_VAL:
    case ME_STRING_CV:
    case ME_STRING_CVLL:
      return Gecode::Int::ME_INT_VAL;
    default:
      return Gecode::Int::ME_INT_DOM;
    }
  }
  
  forceinline int 
  CharacterView::size(void) const {
    return 1;
  }
  
  forceinline int 
  CharacterView::charwidth(int i) const {
    if (i == 0)
      return x.width();
    return 0;
  }
  
  forceinline bool 
  CharacterView::mandatory(int i) const {
    if (i == 0)
      return true;
    return false;
  }
  
  forceinline bool
  CharacterView::forbidden(int i) const {
    if (i == 0)
      return false;
    return true;
  }

  
  // access operations
  forceinline BitSet 
  CharacterView::domain(int i) const {
    if (i > 0)
      return Empty;
    Int::ViewValues<Int::IntView> iter(x);
    assert(iter.val() > 0);
    BitSet bs = Empty;
    while(iter() && iter.val() < static_cast<int>(Numbits)) {
      bs = unionof(bs,singletonfor(iter.val()));
      ++iter;
    }
    return bs;
  }
    
  forceinline bool 
  CharacterView::assigned(void) const {
    return x.assigned();
  }
  
  forceinline bool 
  CharacterView::in(int i, int n) const {
    if (i != 0) return false;
    return x.in(n);
  }
  
  forceinline bool 
  CharacterView::charfixed(int i) const {
    if (i != 0) return true;
    return x.assigned();
  }
  
  forceinline int 
  CharacterView::charval(int i) const {
    assert(i == 0);
    return x.val();
  }
  
  forceinline int 
  CharacterView::charmin(int i) const {
    assert(i == 0);
    return x.min();
  }
  
  forceinline bool 
  CharacterView::lengthfixed(void) const {
    return true;
  }
  
  forceinline int 
  CharacterView::lengthmin(void) const {
    return 1;
  }
  
  forceinline int 
  CharacterView::lengthmax(void) const {
    return 1;
  }
  
  forceinline int 
  CharacterView::lengthmed(void) const {
    return 1;
  }
  
  forceinline int 
  CharacterView::lengthval(void) const {
    return 1;
  }
  
  forceinline int 
  CharacterView::width(void) const {
    return Numbits;
  }
  
  // length modification operations
  forceinline ModEvent 
  CharacterView::lle(Space&, int n) {
    return (n > 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  CharacterView::llq(Space&, int n) {
    return (n >= 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  CharacterView::lgr(Space&, int n) {
    return (n < 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  CharacterView::lgq(Space&, int n) {
    return (n <= 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
    
  // character modification operations
  forceinline ModEvent 
  CharacterView::ceq(Space& home, int i, int n) {
    if (i != 0) return ME_STRING_FAILED;
    return me_inttostr(x.eq(home,n));
  }
  
  forceinline ModEvent 
  CharacterView::cnq(Space& home, int i, int n) {
    if (i != 0) return ME_STRING_NONE;
    return me_inttostr(x.nq(home,n));
  }
  
  forceinline ModEvent 
  CharacterView::inter(Space& home, int index, BitSet b) {
    if (index != 0) return ME_STRING_NONE;
    BitSetRangeIter iter(b);
    return me_inttostr(x.inter_r(home,iter));
  }
  
  
  forceinline ModEvent 
  CharacterView::minus(Space& home, int index, BitSet b) {
    if (index != 0) return ME_STRING_NONE;
    BitSetRangeIter iter(b);
    return me_inttostr(x.minus_r(home,iter));
  }
  
    
    
  forceinline void
  CharacterView::subscribe(Space& home, Propagator& p, PropCond pc,
                           bool schedule) {
    x.subscribe(home,p,pc_strtoint(pc),schedule);
  }
  forceinline void
  CharacterView::cancel(Space& home, Propagator& p, PropCond pc) {
    x.cancel(home,p,pc_strtoint(pc));
  }

  forceinline void
  CharacterView::subscribe(Space& home, Advisor& a) {
    x.subscribe(home,a);
  }
  forceinline void
  CharacterView::cancel(Space& home, Advisor& a) {
    x.cancel(home,a);
  }


  forceinline void
  CharacterView::schedule(Space& home, Propagator& p, ModEvent me) {
    return Int::IntView::schedule(home,p,me_strtoint(me));
  }
  forceinline ModEvent
  CharacterView::me(const ModEventDelta& med) {
    return me_inttostr(Int::IntView::me(med));
  }
  forceinline ModEventDelta
  CharacterView::med(ModEvent me) {
    return StringView::med(me_strtoint(me));
  }
  
  // delta information
  forceinline ModEvent
  CharacterView::modevent(const Delta& d) {
    return me_inttostr(Int::IntView::modevent(d));
  }
  
  forceinline int 
  CharacterView::length_min(const Delta&) const {
    return 1;
  }
  forceinline int 
  CharacterView::length_max(const Delta&) const {
    return 0;
  }
  
  forceinline int 
  CharacterView::index(const Delta&) const {
    return 0;
  }
  
  forceinline bool 
  CharacterView::any(const Delta&) const {
    return false;
  }
  
  forceinline bool 
  CharacterView::length(const Delta&) const {
    return false;
  }
  
  forceinline bool 
  CharacterView::symbol(const Delta&) const {
    return true;
  }

  template<class I>
  forceinline ModEvent 
  CharacterView::inter_v(Space& home, int index, I& i, bool depends) {
    return me_inttostr(x.inter_v(home,index,i,depends));
  }
}}
