/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 13:19:27 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14970 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String {
  
  forceinline int 
  StringView::size(void) const {
    return x->size();
  }
  
  forceinline int 
  StringView::charwidth(int i) const {
    return x->charwidth(i);
  }
  
  forceinline bool 
  StringView::mandatory(int l) const {
    return x->mandatory(l);
  }
  
  forceinline bool
  StringView::forbidden(int l) const {
    return x->forbidden(l);
  }

  
  // access operations
  forceinline BitSet 
  StringView::domain(int i) const {
    return x->domain(i);
  }
    
  forceinline bool 
  StringView::assigned(void) const {
    return x->assigned();
  }
  
  forceinline bool 
  StringView::in(int i, int n) const {
    return x->in(i,n);
  }
  
  forceinline bool 
  StringView::charfixed(int i) const {
    return x->charfixed(i);
  }
  
  forceinline int 
  StringView::charval(int i) const {
    return x->charval(i);
  }
  
  forceinline int 
  StringView::charmin(int i) const {
    return x->charmin(i);
  }
  
  forceinline bool 
  StringView::lengthfixed(void) const {
    return x->lengthfixed();
  }
  
  forceinline int 
  StringView::lengthmin(void) const {
    return x->lengthmin();
  }
  
  forceinline int 
  StringView::lengthmax(void) const {
    return x->lengthmax();
  }
  
  forceinline int 
  StringView::lengthmed(void) const {
    return x->lengthmed();
  }
  
  forceinline int 
  StringView::lengthval(void) const {
    return x->lengthval();
  }
  
  forceinline int 
  StringView::width(void) const {
    return x->width();
  }
  
  // length modification operations
  forceinline ModEvent 
  StringView::lle(Space& home, int n) {
    return x->lle(home,n);
  }
  
  forceinline ModEvent 
  StringView::llq(Space& home, int n) {
    return x->llq(home,n);
  }
  
  forceinline ModEvent 
  StringView::lgr(Space& home, int n) {
    return x->lgr(home,n);
  }
  
  forceinline ModEvent 
  StringView::lgq(Space& home, int n) {
    return x->lgq(home,n);
  }
    
  // character modification operations
  forceinline ModEvent 
  StringView::ceq(Space& home, int i, int n) {
    return x->ceq(home,i,n);
  }
  
  forceinline ModEvent 
  StringView::cnq(Space& home, int i, int n) {
    return x->cnq(home,i,n);
  }
  
  forceinline ModEvent 
  StringView::inter(Space& home, int index, BitSet b) {
    return x->inter(home,index,b);
  }
  
  
  forceinline ModEvent 
  StringView::minus(Space& home, int index, BitSet b) {
    return x->minus(home,index,b);
  }
  
    
  // delta information
  forceinline int 
  StringView::length_min(const Delta& d) const {
    return x->length_min(d);
  }
  forceinline int 
  StringView::length_max(const Delta& d) const {
    return x->length_max(d);
  }
  
  forceinline int 
  StringView::index(const Delta& d) const {
    return x->index(d);
  }
  
  forceinline bool 
  StringView::any(const Delta& d) const {
    return x->any(d);
  }
  
  forceinline bool 
  StringView::length(const Delta& d) const {
    return x->length(d);
  }
  
  forceinline bool 
  StringView::symbol(const Delta& d) const {
    return x->symbol(d);
  }

  template<class I>
  ModEvent 
  StringView::inter_v(Space& home, int index, I& i, bool depends) {
    return x->inter_v(home,index,i,depends);
  }
}}