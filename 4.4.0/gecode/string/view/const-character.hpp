/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String {
  
  forceinline int 
  ConstCharacterView::size(void) const {
    return 1;
  }
  
  forceinline int 
  ConstCharacterView::charwidth(int i) const {
    if (i == 0)
      return 1;
    return 0;
  }
  
  forceinline bool 
  ConstCharacterView::mandatory(int i) const {
    if (i == 0)
      return true;
    return false;
  }
  
  forceinline bool
  ConstCharacterView::forbidden(int i) const {
    if (i == 0)
      return false;
    return true;
  }

  
  // access operations
  forceinline BitSet 
  ConstCharacterView::domain(int i) const {
    assert(i >= 0);
    if (i > 0)
      return Empty;
    return singletonfor(value);
  }
    
  forceinline bool 
  ConstCharacterView::assigned(void) const {
    return true;
  }
  
  forceinline bool 
  ConstCharacterView::in(int i, int n) const {
    if (i != 0) return false;
    return n == value;
  }
  
  forceinline bool 
  ConstCharacterView::charfixed(int) const {
    return true;
  }
  
  forceinline int 
  ConstCharacterView::charval(int i) const {
    assert(i == 0);
    return value;
  }
  
  forceinline int 
  ConstCharacterView::charmin(int i) const {
    assert(i == 0);
    return value;
  }
  
  forceinline bool 
  ConstCharacterView::lengthfixed(void) const {
    return true;
  }
  
  forceinline int 
  ConstCharacterView::lengthmin(void) const {
    return 1;
  }
  
  forceinline int 
  ConstCharacterView::lengthmax(void) const {
    return 1;
  }
  
  forceinline int 
  ConstCharacterView::lengthmed(void) const {
    return 1;
  }
  
  forceinline int 
  ConstCharacterView::lengthval(void) const {
    return 1;
  }
  
  forceinline int 
  ConstCharacterView::width(void) const {
    return Numbits;
  }
  
  // length modification operations
  forceinline ModEvent 
  ConstCharacterView::lle(Space&, int n) {
    return (n > 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstCharacterView::llq(Space&, int n) {
    return (n >= 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstCharacterView::lgr(Space&, int n) {
    return (n < 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstCharacterView::lgq(Space&, int n) {
    return (n <= 1) ? ME_STRING_NONE : ME_STRING_FAILED;
  }
    
  // character modification operations
  forceinline ModEvent 
  ConstCharacterView::ceq(Space&, int i, int n) {
    if (i != 0) return ME_STRING_FAILED;
    return (n == value)? ME_STRING_NONE : ME_STRING_FAILED;
  }
  
  forceinline ModEvent 
  ConstCharacterView::cnq(Space&, int i, int n) {
    if (i != 0) return ME_STRING_NONE;
    return (n == value)? ME_STRING_FAILED : ME_STRING_NONE;
  }
  
  forceinline ModEvent 
  ConstCharacterView::inter(Space&, int index, BitSet b) {
    if (index != 0) return ME_STRING_NONE;
    return interof(singletonfor(value),b) != Empty;
  }
  
  
  forceinline ModEvent 
  ConstCharacterView::minus(Space&, int index, BitSet b) {
    if (index != 0) return ME_STRING_NONE;
    return minusof(singletonfor(value),b) != Empty;
  }
  
  forceinline void 
  ConstCharacterView::update(Space& home, bool share, ConstCharacterView& y) {
    ConstView<StringView>::update(home,share,y);
    value = y.value; 
  }
  
  // delta information
  forceinline ModEvent
  ConstCharacterView::modevent(const Delta&) {
    GECODE_NEVER; return 0;
  }
  
  forceinline int 
  ConstCharacterView::length_min(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  forceinline int 
  ConstCharacterView::length_max(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline int 
  ConstCharacterView::index(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline bool 
  ConstCharacterView::any(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline bool 
  ConstCharacterView::length(const Delta&) const {
    GECODE_NEVER; return 0;
  }
  
  forceinline bool 
  ConstCharacterView::symbol(const Delta&) const {
    GECODE_NEVER; return 0;
  }

  // view tests
  forceinline bool 
  same(const ConstCharacterView& x, const ConstCharacterView& y) {
    return x.charmin(0) == y.charmin(0);
  }
  forceinline bool 
  before(const ConstCharacterView& x, const ConstCharacterView& y) {
    return x.charmin(0) < y.charmin(0);
  }
  
  template<class I>
  forceinline ModEvent 
  ConstCharacterView::inter_v(Space& home, int index, I& i, bool depends) {
    if (index != 0) return ME_STRING_NONE;
    while(i()) {
      if (i.val() == value)
        return ME_STRING_NONE;
      if (i.val() > value)
        return ME_STRING_FAILED;
      ++i;
    }
    return ME_STRING_FAILED;
  }
}}