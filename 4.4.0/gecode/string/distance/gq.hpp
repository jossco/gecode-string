/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Distance {
  template<class V0, class V1>
  forceinline
  Gq<V0,V1>::Gq(Home home, V0 _x, V1 _y, int _d)
  : MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>(home,_x,_y),
    d(_d) {}
  
  template<class V0, class V1>
  ExecStatus 
  Gq<V0,V1>::post(Space& home, V0 x0, V1 x1, int d) {
    (void) new (home) Gq(home,x0,x1,d);
    return ES_OK;
  }
  
  template<class V0, class V1>
  forceinline
  Gq<V0,V1>::Gq(Home home, bool share, Gq<V0,V1>& p)
  : MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>(home, share, p),
    d(p.d) {}
  
  template<class V0, class V1>
  Actor* 
  Gq<V0,V1>::copy(Space& home, bool share){
    return new (home) Gq<V0,V1>(home,share, *this); 
  }
  
  template<class V0, class V1>
  ExecStatus 
  Gq<V0,V1>::propagate(Space& home, const ModEventDelta&) {
    int lmin = std::min(x0.lengthmin(),x1.lengthmin());
    int lmax = std::min(x0.lengthmax(),x1.lengthmax());
    int same = 0; int diff = 0;
    for (int i = 0; i < lmin; i++) {
      if (x0.charfixed(i) && x1.charfixed(i) && x0.charval(i) == x1.charval(i)) {
        same++;
      } else {
        BitSetIter domain(interof(x0.domain(i),x1.domain(i)));
        if (!domain()) {
          diff++;
        }
      }
      
    }
    if (diff >= d)
      return home.ES_SUBSUMED(*this);
    
    if (lmax - same > d) {
      return ES_FIX;
    } 
    if (lmax - same == d) {
      for (int i = 0; i < lmin; i++) {
        if (x0.charfixed(i) && x1.charfixed(i) && x0.charval(i) == x1.charval(i)) {
          same--;
        } else {
          if (x0.charfixed(i)) {
            GECODE_ME_CHECK(x1.cnq(home, i,x0.charval(i)));
          }
          if (x1.charfixed(i)) {
            GECODE_ME_CHECK(x0.cnq(home, i,x1.charval(i)));
          }
        }
      }
    } else {
      return ES_FAILED;
    }
    
    return ES_NOFIX;
  }
  
  template<class V0, class V1>
  forceinline PropCost 
  Gq<V0,V1>::cost(const Space&, const ModEventDelta&) const{
    return PropCost::linear(PropCost::LO,x1.lengthmax());
  }
}}}