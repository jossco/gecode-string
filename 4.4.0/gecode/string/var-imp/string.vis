#
#  Main authors:
#     Christian Schulte <schulte@gecode.org>
#     Joseph Scott <josephdscott@gmail.com>
#
#  Copyright:
#     Christian Schulte, 2002
#     Joseph Scott, 2016
#
#  Last modified:
#     $Date: 2016-04-04 14:20:39 +0200 (Mon, 04 Apr 2016) $ by $Author: jossco $
#     $Revision: 14946 $
#
#  This file is part of Gecode, the generic constraint
#  development environment:
#     http://www.gecode.org
#
#  Permission is hereby granted, free of charge, to any person obtaining
#  a copy of this software and associated documentation files (the
#  "Software"), to deal in the Software without restriction, including
#  without limitation the rights to use, copy, modify, merge, publish,
#  distribute, sublicense, and/or sell copies of the Software, and to
#  permit persons to whom the Software is furnished to do so, subject to
#  the following conditions:
#
#  The above copyright notice and this permission notice shall be
#  included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
#  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
#  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
#  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

[General]
Name:           String
Namespace:      Gecode::String
Ifdef:          GECODE_HAS_STRING_VARS
Bits:           0
Dispose:        true
[ModEventHeader]
  /**
   * \defgroup TaskActorStringMEPC String modification events and propagation conditions
   * \ingroup TaskActorString
   */
  //@{
[ModEvent]
Name:           FAILED=FAILED
#        <text to precede definition, optional>
[ModEvent]
Name:           NONE=NONE
#        <text to precede definition, optional>
[ModEvent]
Name:           VAL=ASSIGNED
// length assigned; all characters assigned
Combine:        VAL=VAL, CDLL=VAL , CVLL=VAL , CVLU=VAL , CVLB=VAL , CLV=VAL , CV=VAL , LV=VAL , LB=VAL , LU=VAL , LL=VAL , CD=VAL 
[ModEvent]
Name:           CDLL
// length LB changed; some characters pruned (at least for i > length)
Combine:        VAL=VAL, CDLL=CDLL , CVLL=CVLL , CVLU=CVLB , CVLB=CVLB , CLV=CLV , CV=CVLL , LV=CDLV , LB=CDLB , LU=CDLB , LL=CDLL , CD=CDLL 
[ModEvent]
Name:           CVLL
// length lower bound tightened; some characters assigned (at least for i > length)
Combine:        VAL=VAL, CDLL=CVLL , CVLL=CVLL , CVLU=CVLB , CVLB=CVLB , CLV=CLV , CV=CVLL , LV=CLV , LB=CVLB , LU=CVLB , LL=CVLL , CD=CVLL 
[ModEvent]
Name:           CVLU
// length upper bound tightened; some characters assigned
Combine:        VAL=VAL, CDLL=CVLB , CVLL=CVLB , CVLU=CVLU , CVLB=CVLB , CLV=CLV , CV=CVLU , LV=CVLV , LB=CVLB , LU=CVLU , LL=CVLB , CD=CVLU 
[ModEvent]
Name:           CVLB
// length both bound tightened; some characters assigned (at least for i > max(length))
Combine:        VAL=VAL, CDLL=CVLB , CVLL=CVLB , CVLU=CVLB , CVLB=CVLB , CLV=CLV , CV=CVLB , LV=CLV , LB=CVLB , LU=CVLB , LL=CVLB , CD=CVLB 
[ModEvent]
Name:           CLV
// length assigned; some characters assigned
Combine:        VAL=VAL, CDLL=CLV , CVLL=CLV , CVLU=CLV , CVLB=CLV , CLV=CLV , CV=CLV , LV=CLV , LB=CLV , LU=CLV , LL=CLV , CD=CLV 
[ModEvent]
Name:           CV
// length unchanged; some characters assigned
Combine:        VAL=VAL, CDLL=CVLL , CVLL=CVLL , CVLU=CVLU , CVLB=CVLB , CLV=CLV , CV=CV , LV=CLV , LB=CVLB , LU=CVLU , LL=CVLL , CD=CV 
[ModEvent]
Name:           LV
// length assigned; no characters assigned
Combine:        VAL=VAL, CDLL=LV , CVLL=CLV , CVLU=CLV , CVLB=CLV , CLV=CLV , CV=CLV , LV=LV , LB=LV , LU=LV , LL=LV , CD=LV 
[ModEvent]
Name:           LB=SUBSCRIBE
// length both bounds changed; no characters assigned
Combine:        VAL=VAL, CDLL=LB , CVLL=CVLB , CVLU=CVLB , CVLB=CVLB , CLV=CLV , CV=CVLB , LV=LV , LB=LB , LU=LB , LL=LB , CD=LB 
[ModEvent]
Name:           LU
// length UB changed; no characters assigned
Combine:        VAL=VAL, CDLL=LB , CVLL=CVLB , CVLU=CVLU , CVLB=CVLB , CLV=CLV , CV=CVLU , LV=LV , LB=LB , LU=LU , LL=LB , CD=LU 
[ModEvent]
Name:           LL
// length LB changed; some characters pruned
Combine:        VAL=VAL, CDLL=CDLL , CVLL=CVLL , CVLU=CVLB , CVLB=CVLB , CLV=CLV , CV=CVLL , LV=LV , LB=LV , LU=LB , LL=LL , CD=CDLL 
[ModEvent]
Name:           CD
// length unchanged; some characters pruned (but none assigned)
Combine:        VAL=VAL, CDLL=CDLL , CVLL=CVLL , CVLU=CVLU , CVLB=CVLB , CLV=CLV , CV=CV , LV=LV , LB=LB , LU=LU , LL=CDLL , CD=CD 
[ModEventFooter]
[PropCondHeader]
[PropCond]
Name:           NONE=NONE
[PropCond]
Name:           VAL=ASSIGNED
ScheduledBy:    VAL
[PropCond]
Name:           CDOM
ScheduledBy:    VAL, CDLL , CVLL , CVLU , CVLB , CLV , CV , LV , LB , LU , CD
[PropCond]
Name:           CVAL
ScheduledBy:    VAL, CVLL , CVLU , CVLB , CLV , CV
[PropCond]
Name:           LUB
ScheduledBy:    VAL, CVLU , CVLB , CLV , LV , LB , LU
[PropCond]
Name:           LLB
ScheduledBy:    VAL, CDLL , CVLL , CVLB , CLV , LV , LB , LL
[PropCond]
Name:           LBND
ScheduledBy:    VAL, CDLL , CVLL , CVLU , CVLB , CLV , LV , LB , LU , LL
[PropCond]
Name:           LVAL
ScheduledBy:    VAL, CLV , LV
[PropCond]
Name:           ANY
ScheduledBy:    VAL, CDLL , CVLL , CVLU , CVLB , CLV , CV , LV , LB , LU , LL , CD
[PropCond]
Name:           CLVAL
ScheduledBy:    VAL, CVLL , CVLU , CVLB , CLV , CV , LV
[PropCondFooter]
  //@}
[End]