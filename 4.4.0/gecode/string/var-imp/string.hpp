/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 13:19:27 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14970 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String {
    
    forceinline
    StringVarImp::StringVarImp(Space& home, 
                               int mi, 
                               int ma, 
                               int w, 
                               BitSet mask, 
                               int blocksize)
    : StringVarImpBase(home), 
      lmin(mi), 
      lmax(ma),
      alphabet(interof(mask,((1UL << w)-1))), 
#if (defined GECODE_STRING_BS_DYNAMIC && defined GECODE_STRING_BS_HEAP)
      character(Gecode::heap,lmin,lmax,blocksize,alphabet)
#elif (defined GECODE_STRING_BS_DYNAMIC && defined GECODE_STRING_BS_SPACE)
      character(home,lmin,lmax,blocksize,alphabet)
#elif defined GECODE_STRING_BS_HEAP
      character(Gecode::heap,lmax,lmax,blocksize,alphabet)
#else
      character(home,lmax,lmax,blocksize,alphabet)
#endif
      {
        assert(lmin <= lmax);
        assert(w > 0);
        assert(static_cast<unsigned int>(w) <= Numbits);
        BitSetIter iter(alphabet);
        wid = 0;
        while (iter()) {
          wid++; ++iter;
        }
      }
      
    // copying
      forceinline StringVarImp*
      StringVarImp::copy(Space& home, bool share) {
      if (copied()) 
        return static_cast<StringVarImp*>(forward());
      else
        return new (home) StringVarImp(home,share,*this);
    }
  
    forceinline
    StringVarImp::StringVarImp(Space& home, bool share, StringVarImp& y)
    : StringVarImpBase(home,share,y), 
      lmin(y.lmin), 
      lmax(y.lmax),
      alphabet(y.alphabet),
      wid(y.wid),
#if defined GECODE_STRING_BS_HEAP
      character(Gecode::heap,y.character,y.lmax) 
#else
      character(home,y.character,y.lmax)
#endif
      {}
        
    forceinline size_t
    StringVarImp::dispose(Space&) {
      character.~BSList();
      return sizeof(*this);
    }
    
    forceinline bool
    StringVarImp::mandatory(int ind) const{
      return ind < lmin;
    }
  
    forceinline bool
    StringVarImp::forbidden(int ind) const{
      return ind >= lmax;
    }
    
    // private methods for setting length bounds
    forceinline void 
    StringVarImp::exclude_index(int i, StringDelta& sd) {
      if (i < lmax) {
        sd.length_interval_removed(i+1,lmax);
        // These get thrown away eventually, so just ignore them:
        // for (int _i = i; _i < lmax; _i++) {
        //   character.removeall(_i);
        // }
        lmax = i;
  #ifdef GECODE_STRING_BS_DYNAMIC
        character.shrink(lmax);
  #endif
      }
    }
    
    forceinline void 
    StringVarImp::include_index(int i, StringDelta& sd) {
      if (lmin <= i) {
        sd.length_interval_removed(lmin,i);
        lmin = i + 1;
  #ifdef GECODE_STRING_BS_DYNAMIC
        character.grow(lmin,lmax,alphabet);
  #endif
      }
    }
    
    // domain tests
    forceinline bool
    StringVarImp::assigned(void) const {
      if (!lengthfixed())
        return false;
      for(int i = 0; i < lmin; i++)
        if (!character.singleton(i))
          return false;
      return true;
    }
    
    // size of the bitset list (may be longer than lmax)
    forceinline unsigned int 
    StringVarImp::size(void) const {
      return character.length();
    }
    // test whether \a n is in the domain of the character at \a i
    forceinline bool
    StringVarImp::in(int i, int v) const {
      if (forbidden(i) || (static_cast<unsigned int>(v) > Numbits))
        return false;
      if (i < character.length())
        return character.in(i,v);
      return interof(alphabet,singletonfor(v)) != Empty;
    }
    
    // size of domain for character \a i
    forceinline int
    StringVarImp::charwidth(int i) const {
      if (forbidden(i)) return 0;
      if (i < character.length())
        return static_cast<int>(character.width(i));
      return static_cast<int>(wid);
    }
    
    // test whether character \a i is assigned
    forceinline bool
    StringVarImp::charfixed(int i) const {
      if (mandatory(i)) {
        return character.width(i) == 1;
      }
      return false;
    }
    
    // get first domain value for character \a i
    forceinline unsigned int
    StringVarImp::charmin(int i) const {
      assert(i < lmax);
      BitSetIter iter((i < character.length())?character[i]:alphabet);
      return iter.val();
    }
    
    // get last domain value for character \a i
    forceinline unsigned int
    StringVarImp::charmax(int i) const {
      assert(i < lmax);
      BitSetIter iter((i < character.length())?character[i]:alphabet);
      unsigned int val;
      while(iter()) {
        val = iter.val(); ++iter;
      }
      return val;
    }
    
    forceinline unsigned int
    StringVarImp::next(int i, int v) const {
      assert(i < lmax);
      BitSetIter iter((i < character.length())?character[i]:alphabet, v);
      return iter.val();
    }
    
    forceinline unsigned int
    StringVarImp::charval(int i) const {
      assert(charfixed(i));
      BitSetIter iter((i < character.length())?character[i]:alphabet);
      return iter.val();
    }
    // test whether length is fixed to a value
    forceinline bool
    StringVarImp::lengthfixed(void) const {
      return lmin == lmax;
    }
    //
    forceinline int
    StringVarImp::lengthmin(void) const {
      return lmin;
    }
    //
    forceinline int
    StringVarImp::lengthmax(void) const {
      return lmax;
    }
    
    forceinline int
    StringVarImp::lengthmed(void) const {
      return (lmin+lmax)/2; // always false: - ((lmin+lmax)%2 < 0 ? 1 : 0);
    }
    //
    forceinline int
    StringVarImp::lengthval(void) const {
      assert(lengthfixed());
      return lmin;
    }
    
    forceinline unsigned int
    StringVarImp::width(void) const {
      return wid;
    }
    
    forceinline BitSet
    StringVarImp::domain(int i) {
      assert(i >= 0);
      if (i < lmax)
        return (i < character.length())?character[i]:alphabet;
      return Empty;
    }
    // length modification operations
    
    // 4 required modification operations:
    forceinline ModEvent 
    StringVarImp::llq(Space& home, int l) {
      if (l >= lmax) return ME_STRING_NONE;
      if (l < lmin) return ME_STRING_FAILED;
      StringDelta sd;
      exclude_index(l,sd);
      if(assigned())
        return notify(home, ME_STRING_VAL, sd);
      return notify(home, lmax == lmin ? ME_STRING_LV : ME_STRING_LU, sd);
    }
   
    forceinline ModEvent 
    StringVarImp::lgq(Space& home, int l) {
      if (l <= lmin) return ME_STRING_NONE;
      if (l > lmax) return ME_STRING_FAILED;
      StringDelta sd;
      bool cval = false;
      for (int _l = lmin; _l < l; _l++) {
        if(charwidth(_l) == 1) {
          cval = true;
        }
      }
      include_index(l-1,sd);
      if(assigned())
        return notify(home, ME_STRING_VAL, sd);
      if(cval)
        return notify(home, ME_STRING_CVLL, sd);
      return notify(home, lmin == lmax ? ME_STRING_LV : ME_STRING_LL, sd);
    }
  
    forceinline ModEvent 
    StringVarImp::ceq(Space& home, int i, int v) {
      assert(v > 0); // valid range: 1..(numbits)
      if ((forbidden(i)) || (!character.in(i,v)))
        return ME_STRING_FAILED; // v notin c[i]
      //if (character.singleton(i))
      //  return ME_STRING_NONE;   // c[i] == {v}
      StringDelta sd(i);
      include_index(i,sd);
      character.fix(i,v);
      if(assigned())
        return notify(home, ME_STRING_VAL, sd);
      return notify (home, lmin == lmax ? ME_STRING_CLV : ME_STRING_CV, sd);
    }
  
    forceinline ModEvent
    StringVarImp::inter(Space& home, int i, BitSet b) {
      BitSet dom = (i < character.length()) ? character.get(i) : alphabet;
      BitSet ib = interof(dom,b);
      if (forbidden(i) || (dom == ib))
        return ME_STRING_NONE;
      StringDelta sd;
      if (ib == Empty) {
        if (mandatory(i)) {
          return ME_STRING_FAILED;
        }
        exclude_index(i,sd);
        return notify(home,assigned() ? ME_STRING_VAL : ME_STRING_LU, sd);
      }
  #ifdef GECODE_STRING_BS_DYNAMIC
      character.grow(i+1,lmax,alphabet);
  #endif
      character.narrow(i,ib);
      sd.index_changed(i);
      if(mandatory(i) && character.singleton(i)){
        return notify(home, assigned() ? ME_STRING_VAL : ME_STRING_CV, sd);
      }
      return notify(home, ME_STRING_CD, sd);
    }
  
    //
    // other modification ops for convenience
    forceinline ModEvent 
    StringVarImp::lle(Space& home, int n) {
      return llq(home, n-1);
    }
   
    forceinline ModEvent 
    StringVarImp::lgr(Space& home, int n) {
      return lgq(home, n+1);
    }
  
    forceinline ModEvent 
    StringVarImp::cnq(Space& home, int i, int n) {
      BitSet b = singletonfor(n);
      return inter(home, i, negationof(b));
    }
  
    forceinline ModEvent 
    StringVarImp::minus(Space& home, int i, BitSet b) {
      return inter(home, i, negationof(b));
    }
    
    /**
    * \name Domain update by bitset
    *
    * Character domains can be updated by value bitsets.
    *
    * The argument \a depends must be true, if the iterator
    * passed as argument depends on the variable implementation
    * on which the operation is invoked. In this case, the variable
    * implementation is only updated after the iterator has been
    * consumed. Otherwise, the domain might be updated concurrently
    * while following the iterator.
    *
    */
    /// Replace domain by values described by \a b
    //ModEvent narrow_b(Space& home, unsigned int index, const BitSetDomain& bsd);
    
    //template<class I>
    //ModEvent narrow_v(Space& home, unsigned int index, I& i, bool depends=true);
    
    template<class I>
    ModEvent 
    StringVarImp::inter_v(Space& home, int index, I& i, bool) {
      return inter(home,index,vals2bitset(i));
    }
    
    
    //template<class I>
    //ModEvent minus_v(Space& home, unsigned int index, I& i, bool depends=true);
      
    // delta information
    forceinline int 
    StringVarImp::length_min(const Delta& d) {
      return static_cast<const StringDelta&>(d).length_min();
    }
    forceinline int 
    StringVarImp::length_max(const Delta& d) {
      return static_cast<const StringDelta&>(d).length_max();
    }
    forceinline int 
    StringVarImp::index(const Delta& d) {
      return static_cast<const StringDelta&>(d).index();
    }
    forceinline bool 
    StringVarImp::any(const Delta& d) {
      return static_cast<const StringDelta&>(d).any();
    }
    forceinline bool 
    StringVarImp::length(const Delta& d) {
      return static_cast<const StringDelta&>(d).length();
    }
    forceinline bool 
    StringVarImp::symbol(const Delta& d) {
      return static_cast<const StringDelta&>(d).symbol();
    }
      
    // subscriptions
    forceinline void
    StringVarImp::subscribe(Space& home, 
                              Propagator& p, 
                              PropCond pc, 
                              bool schedule) {
      StringVarImpBase::subscribe(home,p,pc,assigned(),schedule);
    }
    forceinline void
    StringVarImp::subscribe(Space& home, Advisor& a) {
      StringVarImpBase::subscribe(home,a,assigned());
    }
    forceinline void
    StringVarImp::cancel(Space& home, Propagator& p, PropCond pc) {
      StringVarImpBase::cancel(home,p,pc,assigned());
    }
    forceinline void
    StringVarImp::cancel(Space& home, Advisor& a) {
      StringVarImpBase::cancel(home,a,assigned());
    }
}}