/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-04 14:20:39 +0200 (Mon, 04 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14946 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String {
  forceinline
  BitSetIter::BitSetIter(BitSet b)
  :bs(b), cur(0), limit(Numbits+1) {
    move();
  }

  forceinline
  BitSetIter::BitSetIter(BitSet b, Value n, Value m)
  :bs(b), cur(n), limit(std::min(Numbits+1, m+1)) {
    move();
  }

  forceinline void
  BitSetIter::move(void) {
#if defined(GECODE_SUPPORT_MSVC_32)
    assert(Numbits == 32 - 1);
    unsigned long int p;
    _BitScanForward(&p,bs >> cur);
    cur += static_cast<Value>(p+1);
#elif defined(GECODE_SUPPORT_MSVC_64)
    assert(Numbits == 64 - 1);
    unsigned long int p;
    _BitScanForward64(&p,bs >> cur);
    cur += static_cast<Value>(p+1);
#elif defined(GECODE_HAS_BUILTIN_FFSL)
    if ((Numbits == 32 - 1) || (Numbits == 64 - 1)) {
      int p = __builtin_ffsl(bs >> cur);
      assert(p > 0);
      cur += static_cast<Value>(p);
    }
#else
    while ((bs & (static_cast<BitSet>(1UL) << cur++)) == static_cast<BitSet>(0UL)) {};
#endif
  }


  forceinline bool
  BitSetIter::operator()(void) const {
    return cur < limit;
  }


  forceinline void
  BitSetIter::operator++(void) {
    move();
  }


  forceinline Value
  BitSetIter::val(void) {
    return cur;
  }
  
  forceinline
  BitSetRangeIter::BitSetRangeIter(BitSet b)
  :bs(b), _min(0), _max(0), _lim(Numbits + 1) {
    next();
  }

  forceinline
  BitSetRangeIter::BitSetRangeIter(BitSet b, int n, int m)
  :bs(b), _min(n), _max(n), _lim(std::min(static_cast<int>(Numbits + 1), m+1)) {
    next();
  }


  forceinline void
  BitSetRangeIter::next(void) {
    _min = _max;
#if defined(GECODE_SUPPORT_MSVC_32)
    //FIXME: intrinsics untested
    assert(Numbits == 32 - 1);
    BitSet temp = bs >> _min;
    unsigned long int p;
    _BitScanForward(&p,temp);
    temp = ~(temp >> (p+1));
    _min += static_cast<Value>(p+1);
    _BitScanForward(&p,temp);
    _max = _min + static_cast<Value>(p);
#elif defined(GECODE_SUPPORT_MSVC_64)
    //FIXME: intrinsics untested
    assert(Numbits == 64 - 1);
    BitSet temp = bs >> _min;
    unsigned long int p+1;
    _BitScanForward64(&p,temp);
    temp = ~(temp >> (p+1));
    _min += static_cast<Value>(p+1);
    _BitScanForward64(&p,temp);
    _max = _min + static_cast<Value>(p);
#elif defined(GECODE_HAS_BUILTIN_FFSL)
    if ((Numbits == 32 - 1) || (Numbits == 64 - 1)) {
      BitSet temp = bs >> _min;
      int p = __builtin_ffsl(temp);
      assert(p > 0);
      temp = ~(temp >> (p-1));
      _min += static_cast<Value>(p);
      if (_min < _lim) {
        p = __builtin_ffsl(temp);
        _max = _min + static_cast<Value>(p - 1);
        if (_max > _lim) _max = _lim;
      } else {
        _max = _min;
      }
    }
#else
    while ((bs & (static_cast<BitSet>(1UL) << _min++)) == static_cast<BitSet>(0UL)) {};
    _max = _min;
    if (_max < _lim)
      while ((bs & (static_cast<BitSet>(1UL) << _max++)) != static_cast<BitSet>(0UL)) {};
#endif
  }


  forceinline bool
  BitSetRangeIter::operator()(void) const {
    return _min < _lim;
  }


  forceinline void
  BitSetRangeIter::operator++(void) {
    next();
  }
  
  forceinline int  
  BitSetRangeIter::min(void) const {
    return _min;
  }
  
  forceinline int  
  BitSetRangeIter::max(void) const {
    return _max - 1;
  }
  
  forceinline int
  BitSetRangeIter::width(void) const {
    return _max - _min;
  }
}}

namespace Gecode { namespace String {
  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const String::BitSetIter& _iter) {
    std::basic_ostringstream<Char,Traits> s;
    s.copyfmt(os); s.width(0);
  
    String::BitSetIter iter = _iter;
    s << "{";
    unsigned int minrange = iter.val();
    while (iter()) {
      unsigned int cur = iter.val();
      ++iter;
      if (!(iter()) || (iter.val() > cur + 1)) {
        s << minrange;
        if (minrange < cur)
          s << ":" << cur;
        minrange=iter.val();
        if (iter())
          s << ",";
      }
    }
    s << "}" ;
    return os << s.str();
  }

  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const String::BitSetRangeIter& _iter) {
    std::basic_ostringstream<Char,Traits> s;
    s.copyfmt(os); s.width(0);
    String::BitSetRangeIter iter = _iter;
    s << "{";
    while (iter()) {
      if (iter.min() == iter.max())
        s << iter.min();
      else
        s << iter.min() << ':' << iter.max();
      ++iter;
      if (iter())
        s << ",";
    }
    s << "}" ;
    return os << s.str();
  }
}}