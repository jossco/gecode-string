/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-22 03:57:09 +0200 (Fri, 22 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14979 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { 
  
  template<class A>
  forceinline void
  BitSetArray::initialize(A& a, size_t s, BitSet mask) {
    sz = s;
    bitset = a.template alloc<BitSet>(sz);
    for(size_t i = 0; i < sz; i++)
      bitset[i] = mask;
  }
  
  template<class A>
  forceinline void
  BitSetArray::initialize(A& a, BitSetArray *other, size_t s) {
    sz = (s==0) ? other->sz : std::min(other->sz,s);
    bitset = a.template alloc<BitSet>(sz);
    for(int i=sz; i--; )
      bitset[i] = other->bitset[i];
  }
  
  template<class A>
  forceinline
  BitSetArray::BitSetArray(A& a, size_t s, BitSet mask)
  : sz(s), bitset(a.template alloc<BitSet>(sz)) {
    for(size_t i = 0; i < sz; i++)
      bitset[i] = mask;
  }
  
  template<class A>
  forceinline void
  BitSetArray::realloc(A& a, size_t s, BitSet mask) {
    bitset = a.template realloc<BitSet>(bitset,sz,s);
    for(int i=sz; i < s; i++)
      bitset[i] = mask;
    sz = s;
  }
  
  // copy constructor: must always specify number of cells to copy
  template<class A>
  forceinline
  BitSetArray::BitSetArray(A& a, BitSetArray *other, size_t s)
  : sz(s), bitset(a.template alloc<BitSet>(s)) {
    assert(s <= other->length());
    for(size_t i=sz; i--; )
      bitset[i] = other->bitset[i];
  }
  
  template<class A>
  forceinline size_t
  BitSetArray::dispose(A& a) {
    a.template free<BitSet>(bitset, sz);
    size_t freed = sizeof(BitSet) * sz;
    bitset = NULL; sz = 0;
    return freed;
  }
  
  forceinline BitSet 
  BitSetArray::operator[](size_t ind) const {
    assert(ind < sz);
    return bitset[ind];
  }
  
  forceinline BitSet& 
  BitSetArray::operator[](size_t ind) {
    assert(ind < sz);
    return bitset[ind];
  }
  
  forceinline bool 
  BitSetArray::in(size_t ind, Value val) const {
    return (bitset[ind] & singletonfor(val,false)) != 0UL;
  }
  
  forceinline bool 
  BitSetArray::singleton(size_t ind) const {
    return width(ind) == 1;
  }
    
  forceinline void
  BitSetArray::remove(size_t ind, Value val) {
    bitset[ind] ^= singletonfor(val,false);
  }
  
  forceinline void
  BitSetArray::removeall(size_t ind) {
    bitset[ind] = Empty;
  }
  
  forceinline size_t 
  BitSetArray::width(size_t ind) const {
#if defined(GECODE_HAS_BUILTIN_FFSL)
    // FIXME: assuming that presence of ffsl builtin
    //        implies presence of popcountl builtin
    int c = __builtin_popcountl(bitset[ind]);
    return static_cast<size_t>(c - 1); // correcting for sentinel bit
#else
    unsigned int pos = next_value(ind, 0);
    unsigned int count = 0;
    while (pos < Numbits + 1) {
      count++;
      pos = next_value(ind,pos);
    }
    return count;
#endif
  }
  
  forceinline void
  BitSetArray::fix(size_t ind, Value val) {
    BitSet b = singletonfor(val);
    assert((bitset[ind] & b) != 0UL);
    inter(ind, b);
  }
    
  forceinline Value
  BitSetArray::next_value(size_t ind, Value cur) const {
#if defined(GECODE_SUPPORT_MSVC_32)
    assert(Numbits == 32 - 1);
    unsigned long int p;
    _BitScanForward(&p,bitset[ind] >> cur);
    return static_cast<Value>(p+1)+cur;
#elif defined(GECODE_SUPPORT_MSVC_64)
    assert(Numbits == 64 - 1);
    unsigned long int p;
    _BitScanForward64(&p,bitset[ind] >> cur);
    return static_cast<Value>(p+1)+cur;
#elif defined(GECODE_HAS_BUILTIN_FFSL)
    if ((Numbits == 32 - 1) || (Numbits == 64 - 1)) {
      int p = __builtin_ffsl(bitset[ind] >> cur);
      assert(p > 0);
      return static_cast<Value>(p)+cur;
    }
#else
    while ((bitset[ind] & (static_cast<BitSet>(1UL) << cur++)) == 0UL) {};
    return cur;
#endif
  }
  
  // Value
  // BitSetArray::prev(size_t ind, Value cur) const {
  //   while ((cur > 0) && ((bits[ind] & (static_cast<BitSet>(1UL) << --cur)) == 0UL)) {};
  //   return --cur;
  // }

  
  forceinline void
  BitSetArray::inter(size_t ind, BitSet bs) {
    bitset[ind] &= bs;
    bitset[ind] |= Empty;
  }
  
  forceinline void
  BitSetArray::minus(size_t ind, BitSet bs) {
    bitset[ind] &= ~bs;
    bitset[ind] |= Empty;
  }
  
  forceinline void
  BitSetArray::narrow(size_t ind, BitSet bs) {
    bitset[ind] = bs;
    bitset[ind] |= Empty;
  }
  
  
#ifdef GECODE_STRING_BS_ARRAY
  /*
   * Fixed size array of BitSets
   * (just a wrapper for BitSetArray)
   *
   */
  template<class A>
  forceinline 
  BitSetListBase::BitSetListBase(A& a, size_t minsize, size_t maxsize, size_t bsize, BitSet mask)
  : size(minsize), list(a.template alloc<BitSetArray>(1)) {
    list->initialize(a,minsize,mask);
  }
      
  template<class A>
  forceinline size_t
  BitSetListBase::dispose(A& a) {
    size_t freed = list->dispose(a);
    a.template free<BitSetArray>(list,1);
    return freed + sizeof(BitSetArray*);
  }
  
  // copy constructor: copy BitSets from \a other BitSetArrayList
  // optional parameter \a s says only allocate s BitSets and copy the first s from other
  template<class A>
  forceinline 
  BitSetListBase::BitSetListBase(A& a, const BitSetListBase& other, size_t maxsize)
    : size(std::min(other.size,maxsize)), list(a.template alloc<BitSetArray>(1)) {
      list->initialize(a,other.list,size);
    }
  
  // resize \a list so that it holds at most
  // enough nodes to accomodate \a s bitsets.
  template <class A>
  forceinline void
  BitSetListBase::shrink(A& a, size_t maxsize) {
    if(maxsize < size) {
      list->realloc(a,maxsize);
      size = maxsize;
    }
  }
  
  // resize \a list so that it holds at least
  // \a s bitsets.
  template <class A>
  forceinline void 
  BitSetListBase::grow(A& a, size_t minsize, size_t maxsize, BitSet mask) {
    if(minsize > size) {
      list->realloc(a,minsize,mask);
      size = minsize;
    }
  }
  
  
  forceinline int 
  BitSetListBase::length(void) const {return static_cast<int>(size);}
  
  // access a bitset
  forceinline BitSet& 
  BitSetListBase::operator[](size_t i) {return (*list)[i];}
  
  forceinline BitSet 
  BitSetListBase::operator[](size_t i) const {return (*list)[i];}
  
  forceinline BitSet 
  BitSetListBase::get(size_t i) const {return (*list)[i];};
  
  forceinline bool 
  BitSetListBase::in(size_t ind, Value val) const {
    return list->in(ind, val);
  }
  
  forceinline bool 
  BitSetListBase::singleton(size_t ind) const {
    return list->singleton(ind);
  }
  
  forceinline void
  BitSetListBase::remove(size_t ind, Value val) {
    list->remove(ind, val);
  }
  
  forceinline void
  BitSetListBase::removeall(size_t ind) {
    list->removeall(ind);
  }
  
  forceinline size_t 
  BitSetListBase::width(size_t ind) const {
    return list->width(ind);
  }
  
  forceinline Value 
  BitSetListBase::next(size_t ind, Value cur) const {
    return list->next_value(ind, cur);
  }
  
  // Value prev(size_t ind, Value cur) const {
//       return bits.prev_value(ind, cur);
//     }
  
  forceinline void
  BitSetListBase::inter(size_t ind, BitSet bs) {
    list->inter(ind, bs);
  }

  forceinline void
  BitSetListBase::minus(size_t ind, BitSet bs) {
    list->minus(ind, bs);
  }

  forceinline void
  BitSetListBase::narrow(size_t ind, BitSet bs) {
    list->narrow(ind, bs);
  }
  
  forceinline void
  BitSetListBase::fix(size_t ind, Value val) {
    list->fix(ind, val);
  }
  
  forceinline Value 
  BitSetListBase::val(size_t ind) const {
    assert(singleton(ind));
    return next(ind, 0);
  }
  
#elif defined GECODE_STRING_BS_LIST
  forceinline size_t 
  BitSetListBase::nodesfor(size_t sz) const {
    return (sz>0) ? ((sz-1) / blocksize) + 1 : 0;
  }
    
  template<class A>
  forceinline 
  BitSetListBase::BitSetListBase(A& a, size_t minsize, size_t maxsize, size_t bsize, BitSet mask)
  : size(minsize), blocksize(bsize), list(NULL) {
    Node *cur = NULL;
    if (nodesfor(size) > 0) {
      // allocate last node
      cur = a.template alloc<Node>(1);
      // size limited by maxsize
      if (maxsize < (nodesfor(size) * blocksize))
        cur->initialize(a, maxsize % blocksize, mask);
      else
        cur->initialize(a, blocksize, mask);
      cur->next = NULL;
      // allocate the rest of the nodes
      for (int i = nodesfor(size) - 1; i--;) {
        Node *prev = a.template alloc<Node>(1);
        prev->initialize(a,blocksize,mask);
        prev->next = cur;
        cur = prev;
      }
      list = cur;
    }
  }
      
  template<class A>
  forceinline size_t
  BitSetListBase::dispose(A& a) {
    size_t freed = 0;
    Node *cur = list; 
    while (cur != NULL) {
      freed += cur->dispose(a) + sizeof(Node);
      Node *next = cur->next;
      a.template free<Node>(cur,1);
      cur = next;
    }
    list = NULL; size = 0;
    return freed;
  }
  
  // copy constructor: copy BitSets from \a other BitSetArrayList
  // Allocates (at most) s BitSets and copy the first s from other
  // Will never allocate more than other.sz, though.
  template<class A>
  forceinline 
  BitSetListBase::BitSetListBase(A& a, const BitSetListBase& other, size_t maxsize)
  : size(std::min(other.size, maxsize)), blocksize(other.blocksize), list(NULL) {
    if (other.list == NULL) return;
    list = a.template alloc<Node>(1);
    list->initialize(a,other.list);
    Node *cur = list, *old = other.list;
    for (int i = 1; i < nodesfor(size); i++) {
      cur->next = a.template alloc<Node>(1);
      cur = cur->next; old = old->next;
      assert(old != NULL);
      cur->initialize(a,old);
    }
  }

  // resize \a list so that it holds at most
  // enough nodes to accomodate \a s bitsets.
  template <class A>
  forceinline void
  BitSetListBase::shrink(A& a, size_t maxsize) {
    size_t target = nodesfor(maxsize);
    if (target < nodesfor(size)) {
      int pos = 0;
      Node *cur = list;
      Node *last = NULL;
      while(pos++ < target) {
        assert(cur->next != NULL);
        last = cur;
        cur = cur->next;
      }
      if (last == NULL) {
        assert(target == 0);
        list = NULL;
      } else {
       last->next = NULL;
     }
      
      while (cur != NULL) {
        last = cur;
        cur = cur->next;
        (void) last->dispose(a);
        a.template free<Node>(last,1);
        size--;
      }
    }
    size = std::min(size,maxsize);
  }
  
  // resize \a list so that it holds at least
  // enough nodes to accomodate \a s bitsets.
  template <class A>
  forceinline void
  BitSetListBase::grow(A& a, size_t minsize, size_t maxsize, BitSet mask) {
    assert(minsize <= maxsize);
    size_t target = nodesfor(minsize);
    if (target > nodesfor(size)) {
      int pos = 0;
      if (nodesfor(size) == 0) {
        list = a.template alloc<Node>(1);
        list->initialize(a,blocksize,mask);
        list->next = NULL;
      }
      Node *cur = list;
      Node *last = NULL;
      while (cur != NULL) {
        last = cur;
        cur = cur->next;
        pos++;
      }
      assert(last != NULL);
      cur = last;
      assert(cur != NULL);
      assert(cur->next == NULL);

      while (pos++ < target) {
        assert(cur->next == NULL);
        cur->next = a.template alloc<Node>(1);
        if (pos == target && target * blocksize > maxsize)
          cur->next->initialize(a, maxsize % blocksize, mask);
        else
          cur->next->initialize(a, blocksize, mask);
        cur = cur->next;
      }
      assert(pos == target + 1);
    }
    size = std::max(size,minsize);
  }
  
  forceinline int 
  BitSetListBase::length(void) const {return static_cast<int>(size);}
  
  forceinline BitSetListBase::Node* 
  BitSetListBase::nodefor(int index) const {
    Node* cur = list;
    int pos = blocksize - 1;
    while (pos < index) {
      if (cur == NULL) return cur;
      cur = cur->next;
      pos += blocksize;
    }
    return cur;
  }
  
  forceinline int 
  BitSetListBase::indexof(int index) const {
    return index % blocksize;
  }
  // access a bitset
  forceinline BitSet& 
  BitSetListBase::operator[](size_t i) {
    Node *n = nodefor(i);
    assert(n != NULL);
    return (*n)[indexof(i)];
  }
  
  forceinline BitSet 
  BitSetListBase::operator[](size_t i) const {
    Node *n = nodefor(i);
    assert(n != NULL);
    return (*n)[indexof(i)];
  }
  
  forceinline BitSet 
  BitSetListBase::get(size_t i) const {
    Node *n = nodefor(i);
    assert(n != NULL);
    return (*n)[indexof(i)];
  }
  
  forceinline bool 
  BitSetListBase::in(size_t ind, Value val) const {
    Node *n = nodefor(ind);
    assert(n != NULL);
    return n->in(indexof(ind), val);
  }
  
  forceinline bool 
  BitSetListBase::singleton(size_t ind) const {
    Node *n = nodefor(ind);
    assert(n != NULL);
    return n->singleton(indexof(ind));
  }
  
  forceinline void
  BitSetListBase::remove(size_t ind, Value val) {
    Node *n = nodefor(ind);
    assert(n != NULL);
    n->remove(indexof(ind), val);
  }
  
  forceinline void
  BitSetListBase::removeall(size_t ind) {
    Node *n = nodefor(ind);
    assert(n != NULL);
    n->removeall(indexof(ind));
  }
  
  forceinline size_t 
  BitSetListBase::width(size_t ind) const {
    Node *n = nodefor(ind);
    assert(n != NULL);
    return n->width(indexof(ind));
  }
  
  forceinline Value 
  BitSetListBase::next(size_t ind, Value cur) const {
    Node *n = nodefor(ind);
    assert(n != NULL);
    return n->next_value(indexof(ind), cur);
  }
  
  // Value prev(size_t ind, Value cur) const {
//       return bits.prev(ind, cur);
//     }
  
  forceinline void 
  BitSetListBase::inter(size_t ind, BitSet bs) {
    Node *n = nodefor(ind);
    assert(n != NULL);
    n->inter(indexof(ind), bs);
  }

  forceinline void 
  BitSetListBase::minus(size_t ind, BitSet bs) {
    Node *n = nodefor(ind);
    assert(n != NULL);
    n->minus(indexof(ind), bs);
  }

  forceinline void 
  BitSetListBase::narrow(size_t ind, BitSet bs) {
    Node *n = nodefor(ind);
    assert(n != NULL);
    n->narrow(indexof(ind), bs);
  }
  
  forceinline void 
  BitSetListBase::fix(size_t ind, Value val) {
    Node *n = nodefor(ind);
    assert(n != NULL);
    n->fix(indexof(ind), val);
  }
  
  forceinline Value 
  BitSetListBase::val(size_t ind) const {
    return next(indexof(ind), 0);
  }
#elif defined GECODE_STRING_BS_ARRAYLIST
  forceinline size_t 
  BitSetListBase::nodesfor(size_t sz) const {
    return (sz>0) ? ((sz-1) / blocksize) + 1 : 0;
  }
  
  template<class A>
  forceinline 
  BitSetListBase::BitSetListBase(A& a, size_t minsize, size_t maxsize, size_t bsize, BitSet mask)
  : size(minsize),
    blocksize(bsize),
    nodecount(nodesfor(maxsize)),
    list(NULL) 
  {
    if (nodecount > 0) {
      list = a.template alloc<BitSetArray>(nodecount);
      size_t active = nodesfor(minsize);
      //initialize final chunk
      if (active == nodecount && maxsize < active * blocksize)
        list[--active].initialize(a, maxsize % blocksize, mask);
      else
        list[--active].initialize(a, blocksize, mask);
      // initialize all other chunks
      while(active > 0)
        list[--active].initialize(a,blocksize,mask);
    } else {
      list = NULL;
    }
  }
      
  template<class A>
  forceinline size_t
  BitSetListBase::dispose(A& a) {
    size_t freed = 0;
    for(size_t i=nodecount; i--; )
      if (list[i].initialized())
        freed += list[i].dispose(a);
    a.template free<BitSetArray>(list,nodecount);
    return freed;
  }
  
  // copy constructor: copy BitSets from \a other BitSetArrayList
  // Allocates (at most) s BitSets and copy the first s from other
  // Will never allocate more than other.size, though.
  template<class A>
  BitSetListBase::BitSetListBase(A& a, const BitSetListBase& other, size_t maxsize)
  : size(std::min(other.size, maxsize)), 
    blocksize(other.blocksize),
    nodecount(std::min(other.nodecount,nodesfor(maxsize))),
    list(NULL) 
  {
    if (other.list == NULL) return;
    if (nodecount > 0) {
      list = a.template alloc<BitSetArray>(nodecount);
      size_t active = nodesfor(size);
      //initialize final chunk
      if (active == nodecount && maxsize < active * blocksize) {
        active--;
        BitSetArray *o = &(other.list[active]);
        list[active].initialize(a, o, maxsize % blocksize);
      }
      else {
        active--;
        BitSetArray *o = &(other.list[active]);
        list[active].initialize(a, o);
      }
      // initialize all other chunks
      while(active > 0) {
        active--;
        BitSetArray *o = &(other.list[active]);
        list[active].initialize(a,o);
      }
    }
  }
  
  // resize \a list so that it holds at most
  // enough nodes to accomodate \a s bitsets.
  template <class A>
  forceinline void
  BitSetListBase::shrink(A& a, size_t maxsize) {
    size_t target = nodesfor(maxsize);
    for (size_t i = target; i < nodecount; i++) {
      if (list[i].initialized())
        (void) list[i].dispose(a);
    }
    size = maxsize;
  }
  
  // resize \a list so that it holds at least
  // enough nodes to accomodate \a s bitsets.
  template <class A>
  forceinline void
  BitSetListBase::grow(A& a, size_t minsize, size_t maxsize, BitSet mask) {
    assert(minsize <= maxsize);
    size_t target = nodesfor(minsize);
    size_t lastnode = nodesfor(maxsize);
    assert(target <= nodecount);
    if (list[target-1].initialized()) return; // nothing left to do
    if (target == lastnode && maxsize < target * blocksize)
      list[--target].initialize(a, maxsize % blocksize, mask);
    else
      list[--target].initialize(a, blocksize, mask);
    for (size_t i = target - 1; i--; ) {
      if (list[i].initialized()) break;
      assert((i+1)*blocksize > size);
      list[i].initialize(a,blocksize, mask);
    }
  }
    
  
  forceinline int 
  BitSetListBase::length(void) const {
    return static_cast<int>(size);
  }
  
  forceinline int 
  BitSetListBase::nodefor(int index) const {
    return index / blocksize;
  }
  
  forceinline int 
  BitSetListBase::indexof(int index) const {
    return index % blocksize;
  }
  // access a bitset
  forceinline BitSet& 
  BitSetListBase::operator[](size_t i) {
    return list[nodefor(i)][indexof(i)];
  }
  
  forceinline BitSet 
  BitSetListBase::operator[](size_t i) const {
    return list[nodefor(i)][indexof(i)];
  }
  
  forceinline BitSet 
  BitSetListBase::get(size_t i) const {
    return list[nodefor(i)][indexof(i)];
  }
  
  forceinline bool 
  BitSetListBase::in(size_t ind, Value val) const {
    return list[nodefor(ind)].in(indexof(ind), val);
  }
  
  forceinline bool 
  BitSetListBase::singleton(size_t ind) const {
    return list[nodefor(ind)].singleton(indexof(ind));
  }
  
  forceinline void
  BitSetListBase::remove(size_t ind, Value val) {
    list[nodefor(ind)].remove(indexof(ind), val);
  }
  
  forceinline void
  BitSetListBase::removeall(size_t ind) {
    list[nodefor(ind)].removeall(indexof(ind));
  }
  
  forceinline size_t 
  BitSetListBase::width(size_t ind) const {
    return list[nodefor(ind)].width(indexof(ind));
  }
  
  forceinline Value 
  BitSetListBase::next(size_t ind, Value cur) const {
    return list[nodefor(ind)].next_value(indexof(ind), cur);
  }
  
  // Value prev(size_t ind, Value cur) const {
//       return bits.prev(ind, cur);
//     }
  
  forceinline void
  BitSetListBase::inter(size_t ind, BitSet bs) {
    list[nodefor(ind)].inter(indexof(ind), bs);
  }

  forceinline void
  BitSetListBase::minus(size_t ind, BitSet bs) {
    list[nodefor(ind)].minus(indexof(ind), bs);
  }

  forceinline void
  BitSetListBase::narrow(size_t ind, BitSet bs) {
    list[nodefor(ind)].narrow(indexof(ind), bs);
  }
  
  forceinline void
  BitSetListBase::fix(size_t ind, Value val) {
    list[nodefor(ind)].fix(indexof(ind), val);
  }
  
  forceinline Value 
  BitSetListBase::val(size_t ind) const {
    assert(list[nodefor(ind)].singleton(indexof(ind)));
    return list[nodefor(ind)].next_value(indexof(ind),0);
  }
#endif
  
  template<class A>
  forceinline 
  BitSetList<A>::BitSetList(A& a0, size_t minsize, size_t maxsize, size_t bsize, BitSet mask)
    : BitSetListBase(a0,minsize,maxsize,bsize,mask), a(a0) {}

  template<class A>
  forceinline 
  BitSetList<A>::BitSetList(A& a0, const BitSetList<A>& other, size_t maxsize)
    : BitSetListBase(a0,other,maxsize?maxsize:other.size), a(a0) {}

  template<class A>
  forceinline
  BitSetList<A>::~BitSetList(void) {
    dispose(a);
  }
  
  template<class A>
  forceinline void
  BitSetList<A>::grow(size_t minsize, size_t maxsize, BitSet mask) {
    BitSetListBase::grow(a,minsize,maxsize,mask);
  }
  
  template<class A>
  forceinline void
  BitSetList<A>::shrink(size_t maxsize) {
    BitSetListBase::shrink(a,maxsize);
  }

}}