/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 08:20:30 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14968 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <gecode/string/branch.hh>

namespace Gecode {
  void 
  mandatory(Home home, StringVarArgs& x, String::VarSelect vs, String::IndexSelect is, String::CharValueSelect cvs, String::LengthSelect ls) {
    if (home.failed()) return;
    ViewArray<String::StringView> y(home,x);
    String::Branch::Mandatory::post(home,y,vs,is,cvs,ls);
  }
  
  void 
  mandatory(Home home, StringVarArray& x, String::VarSelect vs, String::IndexSelect is, String::CharValueSelect cvs, String::LengthSelect ls) {
    if (home.failed()) return;
    StringVarArgs y(x.size());
    for(int i = 0; i < x.size(); i++)
      y[i] = x[i];
    ViewArray<String::StringView> _y(home,y);
    String::Branch::Mandatory::post(home,_y,vs,is,cvs,ls);
  }
}