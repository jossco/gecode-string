/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-05 10:24:04 +0200 (Tue, 05 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14947 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <gecode/string/length.hh>

namespace Gecode {
  
  void
  length(Home home, StringVar _x, IntVar _l) {
    if (home.failed()) return;
    String::StringView x(_x);
    Int::IntView l(_l);
    GECODE_ES_FAIL((String::Length::Base< String::StringView, Int::IntView >::post(home,x,l)));
  }
  
  
  void
  length(Home home, StringVar _x, int l) {
    if (home.failed()) return;
    String::StringView x(_x);
    GECODE_ME_FAIL(x.llq(home,l));
    GECODE_ME_FAIL(x.lgq(home,l));
  }
  
  void
  length(Home home, const StringVarArgs& _x, const IntVarArgs& _l) {
    if (home.failed()) return;
    if (_x.size() != _l.size())
      throw String::ArgumentSizeMismatch("String::length");
    for(int i=0;i<_x.size();i++) {
      String::StringView x(_x[i]);
      Int::IntView l(_l[i]);
      GECODE_ES_FAIL((String::Length::Base< String::StringView, Int::IntView >::post(home,x,l)));
    }
  }
}