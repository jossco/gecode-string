
#ifdef GECODE_HAS_UNORDERED_MAP
#include <unordered_map>
#endif
#include <map>

namespace Gecode { namespace String { namespace Morph{
  class SharedBiMap : public SharedHandle {
  public:
  #ifdef GECODE_HAS_UNORDERED_MAP
    typedef std::unordered_map<int,int> Encoder;
  #else
    typedef std::map<int,int> Encoder;
  #endif
    typedef std::multimap<int,int> Decoder;
  protected:
    class BMO : public SharedHandle::Object {
    public:
      Encoder encoder;
      Decoder decoder;
    
      template<class MapType>
      BMO(const MapType& m) {
        typedef typename MapType::const_iterator const_iterator;
        for(const_iterator iter = m.begin(), iend = m.end(); iter != iend; ++iter ) {
          encoder[iter->first] = iter->second;
          decoder.insert(std::make_pair(iter->second,iter->first));
        }
      }
    
      BMO(const IntArgs& from, const IntArgs& to) {
        for(int i = 0; i < from.size(); i++) {
          encoder[from[i]] = to[i];
          decoder.insert(std::make_pair(to[i],from[i]));
        }
      }
      BMO(const BMO& BMO) 
      : encoder(BMO.encoder), decoder(BMO.decoder) {}
    
      virtual Object* copy(void) const {
        return new BMO(*this); 
      }
    
      virtual ~BMO(void) {}
    };
    // Translate an integer value in the source alphabet
    // into a (singleton) BitSet with value in the target alphabet.
    BitSet _encode(int v) {
      if (static_cast<BMO*>(object())->encoder.count(v))
        return singletonfor(static_cast<BMO*>(object())->encoder[v]);
      return singletonfor(v);
    }
    
    // Translate an integer value in the target alphabet
    // into a BitSet of values in the source alphabet.
    BitSet _decode(int v) {
      BitSet tdom = Empty;
      if (static_cast<BMO*>(object())->decoder.count(v)) {
        typedef typename Decoder::const_iterator iterator;
        for(iterator iter = static_cast<BMO*>(object())->decoder.lower_bound(v), 
                     end = static_cast<BMO*>(object())->decoder.upper_bound(v);
            iter != end;
            ++iter) {
          tdom = unionof(tdom,singletonfor(iter->second));
        }
      } else {
        tdom = singletonfor(v);
      }
      return tdom;
    }
  public:
    
    template<class MapType>
    SharedBiMap(const MapType& m)
      : SharedHandle(new BMO(m)) {}
  
    SharedBiMap(const SharedBiMap& tm)
      : SharedHandle(tm) {}
    
    SharedBiMap(const IntArgs& from, const IntArgs& to)
      : SharedHandle(new BMO(from,to)) {}
  
    SharedBiMap& operator =(const SharedBiMap& tm) {
      return static_cast<SharedBiMap&>(SharedHandle::operator =(tm));
    }
    
    // Translate a value iterator in the source alphabet
    // into a BitSet of values in the target alphabet.
    template<class I> 
    BitSet encode_v(I& i) {
      BitSet tdom = Empty;
      while(i()) {
        tdom = unionof(tdom,_encode(i.val()));
        ++i;
      }
      return tdom;
    }

    BitSet encode(BitSet b) {
      BitSetIter bi(b);
      return encode_v(bi);
    }
    
    // Translate a value iterator in the target alphabet,
    // into a BitSet of values from the source alphabet.
    template<class I> 
    BitSet decode_v(I& i) {
      BitSet tdom = Empty;
      while(i()) {
        tdom = unionof(tdom,_decode(i.val()));
        ++i;
      }
      return tdom;
    }

    BitSet decode(BitSet b) {
      BitSetIter bi(b);
      return decode_v(bi);
    }
  };
}}}