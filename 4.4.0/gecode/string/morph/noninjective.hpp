
namespace Gecode { namespace String { namespace Morph {
  template<class V0, class V1>
  NonInjective<V0,V1>::NonInjective(Home home, V0 _x, V1 _y, const IntArgs& from, const IntArgs& to)
  : Propagator(home), X(_x), Y(_y), map(from,to) {
    home.notice(*this,AP_DISPOSE);
    X.subscribe(home, *this, PC_STRING_ANY);
    Y.subscribe(home, *this, PC_STRING_ANY);
  }

  template<class V0, class V1>
  NonInjective<V0,V1>::NonInjective(Home home, bool share, NonInjective& p)
  : Propagator(home, share, p), map(p.map) {
    X.update(home, share, p.X);
    Y.update(home, share, p.Y);
  }

  template<class V0, class V1>
  size_t 
  NonInjective<V0,V1>::dispose(Space& home) {
    home.ignore(*this,AP_DISPOSE);
    X.cancel(home, *this, PC_STRING_ANY);
    Y.cancel(home, *this, PC_STRING_ANY);
    map.~SharedBiMap();
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }

  template<class V0, class V1>
  ExecStatus 
  NonInjective<V0,V1>::post(Space& home, 
                            V0 x, V1 y, 
                            const IntArgs& from, 
                            const IntArgs& to) {
    GECODE_ME_CHECK(x.llq(home,y.lengthmax()));
    GECODE_ME_CHECK(x.lgq(home,y.lengthmin()));
    GECODE_ME_CHECK(y.llq(home,x.lengthmax()));
    GECODE_ME_CHECK(y.lgq(home,x.lengthmin()));
    (void) new (home) NonInjective(home,x,y,from,to);
    return ES_OK;
  }

  template<class V0, class V1>
  ExecStatus 
  NonInjective<V0,V1>::propagate(Space& home, const Gecode::ModEventDelta&) {
    GECODE_ME_CHECK(X.llq(home,Y.lengthmax()));
    GECODE_ME_CHECK(X.lgq(home,Y.lengthmin()));
    GECODE_ME_CHECK(Y.llq(home,X.lengthmax()));
    GECODE_ME_CHECK(Y.lgq(home,X.lengthmin()));
    for (int i = X.lengthmax(); i--; ) {
      GECODE_ME_CHECK(X.inter(home,i,map.decode(Y.domain(i))));
      GECODE_ME_CHECK(Y.inter(home,i,map.encode(X.domain(i))));
    }
    if(Y.assigned())
      return home.ES_SUBSUMED(*this);
    return ES_FIX;
  }

}}}