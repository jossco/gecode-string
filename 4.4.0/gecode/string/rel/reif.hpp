/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


namespace Gecode { namespace String { namespace Rel {
  
  template<class V0, class V1, class VB>
  ReEqual<V0,V1,VB>::ReEqual(Home home, V0 _x, V1 _y, VB _b)
  : Propagator(home), x(_x), y(_y), b(_b) {
    x.subscribe(home, *this, PC_STRING_ANY);
    y.subscribe(home, *this, PC_STRING_ANY);
    b.subscribe(home, *this, Int::PC_BOOL_VAL);
  }
  
  template<class V0, class V1, class VB>
  ReEqual<V0,V1,VB>::ReEqual(Home home, bool share, ReEqual& p)
  : Propagator(home, share, p) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
    b.update(home, share, p.b);
  }
  
  template<class V0, class V1, class VB>
  size_t 
  ReEqual<V0,V1,VB>::dispose(Space& home) {
    x.cancel(home, *this, PC_STRING_ANY);
    y.cancel(home, *this, PC_STRING_ANY);
    b.cancel(home, *this, Int::PC_BOOL_VAL);
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }
  
  template<class V0, class V1, class VB>
  ExecStatus 
  ReEqual<V0,V1,VB>::post(Space& home, V0 x, V1 y, VB b) {
    (void) new (home) ReEqual(home,x,y,b);
    return ES_OK;
  }
  
  template<class V0, class V1, class VB>
  ExecStatus 
  ReEqual<V0,V1,VB>::propagate(Space& home, const Gecode::ModEventDelta&) {
    if (b.one())
      GECODE_REWRITE(*this,(Equal<V0, V1>::post(home(*this),x,y)));
    if (b.zero())
      GECODE_REWRITE(*this,(Nequal<V0, V1>::post(home(*this),x,y)));
    int same = 0;
    int len = std::min(x.lengthmin(),y.lengthmin());
    for (int i = 0; i < len; i++) {
      if (Empty == unionof(x.domain(i), y.domain(i))) {
        GECODE_ME_CHECK(b.zero(home));
      }
      if (x.charfixed(i) && y.charfixed(i) && x.charval(i) == y.charval(i)) {
        same++;
      }
    }
    if (same == len && x.lengthfixed() && y.lengthfixed()) {
      GECODE_ME_CHECK(b.one(home));
      return home.ES_SUBSUMED(*this);
    } 
    return ES_FIX;
  }
}}}