/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 08:20:30 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14968 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Rel {
  template<class V0, class V1>
  forceinline
  Nequal<V0,V1>::Nequal(Home home, V0 _x, V1 _y)
  : MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>(home,_x,_y),
    i(0) {}
  
  template<class V0, class V1>
  ExecStatus 
  Nequal<V0,V1>::post(Space& home, V0 x0, V1 x1) {
    if ((x0.lengthmin() > x1.lengthmax()) || (x0.lengthmax() < x1.lengthmin()))
      return ES_OK;
    if ((x0.lengthmax() == 0) && (x1.lengthmax() == 0))
      return ES_FAILED;
    (void) new (home) Nequal(home,x0,x1);
    return ES_OK;
  }
  
  template<class V0, class V1>
  forceinline
  Nequal<V0,V1>::Nequal(Home home, bool share, Nequal<V0,V1>& p)
  : MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>(home, share, p), 
    i(p.i) {}
  
  template<class V0, class V1>
  Actor* 
  Nequal<V0,V1>::copy(Space& home, bool share){
    return new (home) Nequal<V0,V1>(home,share, *this); 
  }
  
  template<class V0, class V1>
  ExecStatus 
  Nequal<V0,V1>::propagate(Space& home, const ModEventDelta&) {
    int len = std::min(x0.lengthmin(),x1.lengthmin());
    while (i < len) {
      if (Empty == unionof(x0.domain(i), x1.domain(i))) {
        return home.ES_SUBSUMED(*this);
      }
      if (x0.charfixed(i) && x1.charfixed(i) && x0.charval(i) == x1.charval(i)) {
        i++;
      } else {
        break;
      }
    }
    if (i == len) {
      // all mandatory characters in shorter string are equal to larger string
      if (x0.lengthfixed() && x1.lengthfixed()) {
        return ES_FAILED;
      }
    }
    assert( i <= len );
    return ES_FIX;
  }
  
  template<class V0, class V1>
  forceinline PropCost 
  Nequal<V0,V1>::cost(const Space&, const ModEventDelta&) const{
    return PropCost::linear(PropCost::LO,x1.lengthmax());
  }
}}}