/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 08:20:30 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14968 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Rel {
  template<class V0, class V1>
  forceinline
  Equal<V0,V1>::Equal(Home home, V0 _x, V1 _y)
  : MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>(home,_x,_y) {}
  
  template<class V0, class V1>
  ExecStatus 
  Equal<V0,V1>::post(Space& home, V0 x0, V1 x1) {
    GECODE_ME_CHECK(x0.llq(home,x1.lengthmax()));
    GECODE_ME_CHECK(x0.lgq(home,x1.lengthmin()));
    GECODE_ME_CHECK(x1.llq(home,x0.lengthmax()));
    GECODE_ME_CHECK(x1.lgq(home,x0.lengthmin()));
    for (int i = x0.lengthmax(); i--; ) {
      GECODE_ME_CHECK(x0.inter(home,i,x1.domain(i)));
      GECODE_ME_CHECK(x1.inter(home,i,x0.domain(i)));
    }
    if (!x0.assigned())
      (void) new (home) Equal(home,x0,x1);
    return ES_OK;
  }
  
  template<class V0, class V1>
  forceinline
  Equal<V0,V1>::Equal(Home home, bool share, Equal<V0,V1>& p)
  : MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>(home, share, p) {}
  
  template<class V0, class V1>
  Actor* 
  Equal<V0,V1>::copy(Space& home, bool share){
    return new (home) Equal<V0,V1>(home,share, *this); 
  }
  
  template<class V0, class V1>
  ExecStatus 
  Equal<V0,V1>::propagate(Space& home, const ModEventDelta&) {
    GECODE_ME_CHECK(x0.llq(home,x1.lengthmax()));
    GECODE_ME_CHECK(x0.lgq(home,x1.lengthmin()));
    GECODE_ME_CHECK(x1.llq(home,x0.lengthmax()));
    GECODE_ME_CHECK(x1.lgq(home,x0.lengthmin()));
    for (int i = x0.lengthmax(); i--; ) {
      GECODE_ME_CHECK(x0.inter(home,i,x1.domain(i)));
      GECODE_ME_CHECK(x1.inter(home,i,x0.domain(i)));
    }
    if(x0.assigned())
      return home.ES_SUBSUMED(*this);
    return ES_FIX;
  }
  
  template<class V0, class V1>
  forceinline PropCost 
  Equal<V0,V1>::cost(const Space&, const ModEventDelta&) const{
    return PropCost::linear(PropCost::LO,x1.lengthmax());
  }
}}}