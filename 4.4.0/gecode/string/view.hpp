/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 13:19:27 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14970 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <iostream>

namespace Gecode { namespace String {

  class StringView : public VarImpView<StringVar> {
  protected:
    using VarImpView<StringVar>::x;
  public:
    StringView(void) {}
    StringView(const StringVar& y)
      : VarImpView<StringVar>(y.varimp()) {}
    StringView(StringVarImp* y)
      : VarImpView<StringVar>(y) {}
    
    // access operations
    BitSet domain(int i) const;
    
    bool assigned(void) const;
    int size(void) const;
    bool in(int i, int n) const;
    int charwidth(int i) const;
    bool charfixed(int i) const;
    int charval(int i) const;
    int charmin(int i) const;
    int charmax(int i) const;
    bool lengthfixed(void) const;
    int lengthmin(void) const;
    int lengthmax(void) const;
    int lengthmed(void) const;
    int lengthval(void) const;
    int width(void) const;
    bool mandatory(int l) const;
    bool forbidden(int l) const;
    int next(int i, int v) const;

    // length modification operations
    ModEvent lle(Space& home, int n);
    ModEvent llq(Space& home, int n);
    ModEvent lgr(Space& home, int n);
    ModEvent lgq(Space& home, int n);
      
    // character modification operations
    ModEvent ceq(Space& home, int i, int n);
    ModEvent cnq(Space& home, int i, int n);
    ModEvent inter(Space& home, int index, BitSet b);
    ModEvent minus(Space& home, int index, BitSet b);
    
    template<class I>
    ModEvent inter_v(Space& home, int index, I& i, bool depends=true);
      
    // delta information
    int length_min(const Delta& d) const;
    int length_max(const Delta& d) const;
    int index(const Delta& d) const;
    bool any(const Delta& d) const;
    bool length(const Delta& d) const;
    bool symbol(const Delta& d) const;
  };
  
  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const StringView& x);

  class CharacterView : public DerivedView<Int::IntView> {
  protected:
    using DerivedView<Int::IntView>::x;
    /// Convert string variable PropCond \a pc to a PropCond for integer variables
    static PropCond pc_strtoint(PropCond pc);
    /// Convert integer variable ModEvent \a me to a ModEvent for str variables
    static ModEvent me_inttostr(ModEvent me);
    /// Convert str variable ModEvent \a me to a ModEvent for integer variables
    static ModEvent me_strtoint(ModEvent me);
  public:
    /// Default constructor
    CharacterView(void) {}
    /// Initialize with integer view \a y
    explicit CharacterView(Int::IntView& y)
      : DerivedView<Int::IntView>(y) {}
    
    // access operations
    BitSet domain(int i) const;
    
    bool assigned(void) const;
    int size(void) const;
    bool in(int i, int n) const;
    int charwidth(int i) const;
    bool charfixed(int i) const;
    int charval(int i) const;
    int charmin(int i) const;
    int charmax(int i) const;
    bool lengthfixed(void) const;
    int lengthmin(void) const;
    int lengthmax(void) const;
    int lengthmed(void) const;
    int lengthval(void) const;
    int width(void) const;
    bool mandatory(int l) const;
    bool forbidden(int l) const;
    int next(int i, int v) const;

    // length modification operations
    ModEvent lle(Space& home, int n);
    ModEvent llq(Space& home, int n);
    ModEvent lgr(Space& home, int n);
    ModEvent lgq(Space& home, int n);
      
    // character modification operations
    ModEvent ceq(Space& home, int i, int n);
    ModEvent cnq(Space& home, int i, int n);
    ModEvent inter(Space& home, int index, BitSet b);
    ModEvent minus(Space& home, int index, BitSet b);
    
    template<class I>
    ModEvent inter_v(Space& home, int index, I& i, bool depends=true);
      
    /// \name View-dependent propagator support
    //@{
    /// Schedule propagator \a p with modification event \a me
    static void schedule(Space& home, Propagator& p, ModEvent me);
    /// Return modification event for view type in \a med
    static ModEvent me(const ModEventDelta& med);
    /// Translate modification event \a me to modification event delta for view
    static ModEventDelta med(ModEvent);
    //@}

    /// \name Dependencies
    //@{
    /**
     * \brief Subscribe propagator \a p with propagation condition \a pc to view
     *
     * In case \a schedule is false, the propagator is just subscribed but
     * not scheduled for execution (this must be used when creating
     * subscriptions during propagation).
     */
    void subscribe(Space& home, Propagator& p, PropCond pc, bool schedule=true);
    /// Cancel subscription of propagator \a p with propagation condition \a pc to view
    void cancel(Space& home, Propagator& p, PropCond pc);
    /// Subscribe advisor \a a to view
    void subscribe(Space& home, Advisor& a);
    /// Cancel subscription of advisor \a a
    void cancel(Space& home, Advisor& a);
    //@}

    /// \name Delta information for advisors
    //@{
    /// Return modification event
    static ModEvent modevent(const Delta& d);
    int length_min(const Delta& d) const;
    int length_max(const Delta& d) const;
    int index(const Delta& d) const;
    bool any(const Delta& d) const;
    bool length(const Delta& d) const;
    bool symbol(const Delta& d) const;
  };

  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const CharacterView& x);
  
  class ConstCharacterView : public ConstView<StringView> {
  protected:
    int value;
  public:
    ConstCharacterView(void) : value(0) {}
    /// Initialize with integer \a i
    explicit ConstCharacterView(int i) : value(i) {}
    
    // access operations
    BitSet domain(int i) const;
    
    bool assigned(void) const;
    int size(void) const;
    bool in(int i, int n) const;
    int charwidth(int i) const;
    bool charfixed(int i) const;
    int charval(int i) const;
    int charmin(int i) const;
    int charmax(int i) const;
    bool lengthfixed(void) const;
    int lengthmin(void) const;
    int lengthmax(void) const;
    int lengthmed(void) const;
    int lengthval(void) const;
    int width(void) const;
    bool mandatory(int l) const;
    bool forbidden(int l) const;
    int next(int i, int v) const;

    // length modification operations
    ModEvent lle(Space& home, int n);
    ModEvent llq(Space& home, int n);
    ModEvent lgr(Space& home, int n);
    ModEvent lgq(Space& home, int n);
      
    // character modification operations
    ModEvent ceq(Space& home, int i, int n);
    ModEvent cnq(Space& home, int i, int n);
    ModEvent inter(Space& home, int index, BitSet b);
    ModEvent minus(Space& home, int index, BitSet b);
    
    template<class I>
    ModEvent inter_v(Space& home, int index, I& i, bool depends=true);

    /// \name Delta information for advisors
    //@{
    /// Return modification event
    static ModEvent modevent(const Delta& d);
    int length_min(const Delta& d) const;
    int length_max(const Delta& d) const;
    int index(const Delta& d) const;
    bool any(const Delta& d) const;
    bool length(const Delta& d) const;
    bool symbol(const Delta& d) const;
    
    void update(Space& home, bool share, ConstCharacterView& y);
  };
  
  // view tests
  bool same(const ConstCharacterView& x, const ConstCharacterView& y);
  bool before(const ConstCharacterView& x, const ConstCharacterView& y);
  
  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const ConstCharacterView& x);
  
  class ConstStringView : public ConstView<StringView> {
  protected:
    int *sym;
    int sz;
  public:
    ConstStringView(void) : sym(NULL), sz(0) {}
    
    template <class T>
    explicit ConstStringView(Space& home, const T& _sym) {
      sz = 0;
      if (_sym.size() > 0) {
        sym = home.alloc<int>(_sym.size());
        for(typename T::const_iterator i = _sym.begin(); i != _sym.end(); ++i)
          sym[sz++] = *i;
      } else {
        sym = NULL;
      }
      assert(sz == _sym.size());
    }
    
    // access operations
    BitSet domain(int i) const;
    
    bool assigned(void) const;
    int size(void) const;
    bool in(int i, int n) const;
    int charwidth(int i) const;
    bool charfixed(int i) const;
    int charval(int i) const;
    int charmin(int i) const;
    int charmax(int i) const;
    bool lengthfixed(void) const;
    int lengthmin(void) const;
    int lengthmax(void) const;
    int lengthmed(void) const;
    int lengthval(void) const;
    int width(void) const;
    bool mandatory(int l) const;
    bool forbidden(int l) const;
    int next(int i, int v) const;

    // length modification operations
    ModEvent lle(Space& home, int n);
    ModEvent llq(Space& home, int n);
    ModEvent lgr(Space& home, int n);
    ModEvent lgq(Space& home, int n);
      
    // character modification operations
    ModEvent ceq(Space& home, int i, int n);
    ModEvent cnq(Space& home, int i, int n);
    ModEvent inter(Space& home, int index, BitSet b);
    ModEvent minus(Space& home, int index, BitSet b);
    
    template<class I>
    ModEvent inter_v(Space& home, int index, I& i, bool depends=true);

    /// \name Delta information for advisors
    //@{
    /// Return modification event
    static ModEvent modevent(const Delta& d);
    int length_min(const Delta& d) const;
    int length_max(const Delta& d) const;
    int index(const Delta& d) const;
    bool any(const Delta& d) const;
    bool length(const Delta& d) const;
    bool symbol(const Delta& d) const;
    void update(Space& home, bool share, ConstStringView& y);
  };
  
  // view tests
  bool same(const ConstStringView& x, const ConstStringView& y);
  bool before(const ConstStringView& x, const ConstStringView& y);
  
  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const ConstStringView& x);
}}

#include <gecode/string/var/string.hpp>
#include <gecode/string/view/string.hpp>
#include <gecode/string/view/character.hpp>
#include <gecode/string/view/const.hpp>
#include <gecode/string/view/const-character.hpp>
#include <gecode/string/view/print.hpp>
#include <gecode/string/var/print.hpp>