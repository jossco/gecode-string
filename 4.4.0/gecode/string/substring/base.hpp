/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Substring {

  template<class SView1, class SView2, class IView>
  Base<SView1,SView2,IView>::Base(Home home, SView1 _x, SView2 _y, IView _i)
  : Propagator(home), x(_x), y(_y), i(_i), c(home) {
    i.subscribe(home, *this, Int::PC_INT_DOM);
    x.subscribe(home, *new (home) Advisor(home,*this,c));
    y.subscribe(home, *this, PC_STRING_ANY);
  }
  
  template<class SView1, class SView2, class IView>
  ExecStatus
  Base<SView1,SView2,IView>::advise(Space&, Advisor&, const Delta& d) {
    if (x.any(d)) return ES_NOFIX;
    if (x.symbol(d)) {
      if ((x.index(d) >= i.min()) && (x.index(d) <= i.max() + y.lengthmax()))
        return ES_NOFIX;
    }
    if (x.length(d)) {
      if ((x.length_min(d) <= i.max() + y.lengthmax()) || (x.length_max(d) <= i.min())) {
        // at least some range has been removed
        if ((x.length_min(d) <= i.min()) && (x.length_max(d) <= i.max() + y.lengthmax())) {
          return ES_FAILED;
        }
        return ES_NOFIX;
      }
    }
    return ES_FIX;
  }
  
  template<class SView1, class SView2, class IView>
  Base<SView1,SView2,IView>::Base(Home home, bool share, Base& p)
  : Propagator(home, share, p) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
    i.update(home, share, p.i);
    c.update(home, share, p.c);
  }
  
  template<class SView1, class SView2, class IView>
  size_t 
  Base<SView1,SView2,IView>::dispose(Space& home) {
    i.cancel(home, *this, Int::PC_INT_DOM);
    x.cancel(home,Advisors<Advisor>(c).advisor());
    c.dispose(home);
    y.cancel(home,*this,PC_STRING_ANY);
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }
  
  template<class SView1, class SView2, class IView>
  ExecStatus 
  Base<SView1,SView2,IView>::post(Space& home, SView1 x, SView2 y, IView i) {
    GECODE_ME_CHECK(x.lgq(home,y.lengthmin() + i.min()));
    GECODE_ME_CHECK(y.llq(home,x.lengthmax() - i.min()));
    GECODE_ME_CHECK(i.lq(home,x.lengthmax() - y.lengthmin()));
    (void) new (home) Base(home,x,y,i);
    return ES_OK;
  }
  
  template<class SView1, class SView2, class IView>
  ExecStatus 
  Base<SView1,SView2,IView>::propagate(Space& home, const Gecode::ModEventDelta&) {
    bool nafp = false;
    GECODE_ME_CHECK(x.lgq(home,y.lengthmin() + i.min()));
    GECODE_ME_CHECK(y.llq(home,x.lengthmax() - i.min()));
    GECODE_ME_CHECK(i.lq(home,x.lengthmax() - y.lengthmin()));
    for (int yind = y.lengthmax() - 1; yind >= y.lengthmin(); yind--) {
      // pruning in this region may result in changes to length of y
      BitSet xs = Empty;
      for (int xind = i.min(); xind <= i.max(); xind++) {
        xs = unionof(xs,x.domain(xind));
      }
      GECODE_ME_CHECK_MODIFIED(nafp,y.inter(home,yind,xs));
    }
    for (int yind = y.lengthmin(); yind--; ) {
      BitSet xs = Empty;
      for (int xind = i.min(); xind <= i.max(); xind++) {
        xs = unionof(xs,x.domain(xind));
      }
      GECODE_ME_CHECK(y.inter(home,yind,xs));
    }
    for (int xind = i.max(); xind < i.min() + y.lengthmin(); xind++) {
      bool nafp2 = false;
      BitSet ys = Empty;
      for (int yind = xind - i.max(); yind <= xind - i.min(); yind++) {
        ys = unionof(ys,y.domain(yind));
      }
      GECODE_ME_CHECK_MODIFIED(nafp2, x.inter(home,xind,ys));
      nafp |= (nafp2 && (xind >= x.lengthmin()));
    }
  
    if (y.lengthfixed() && y.lengthval() == 0)
      return home.ES_SUBSUMED(*this);
    
    if (i.assigned() && y.assigned()) {
      if (!x.assigned()) {
        for (int xind = i.val(); xind <= i.val() + y.lengthval(); xind++)
          GECODE_ME_CHECK(x.ceq(home,xind,y.charval(xind - i.val())));
      }
      return home.ES_SUBSUMED(*this);
    }   
    return nafp ? ES_NOFIX : ES_FIX;
  }
}}}