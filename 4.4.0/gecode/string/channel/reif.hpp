/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 16:45:08 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14962 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


namespace Gecode { namespace String { namespace Channel {
  
  template<class SView, class IView, class BView>
  Reif<SView,IView,BView>::Reif(Home home, SView _x, int ind, IView _i, BView _b)
  : Propagator(home), x(_x), i(_i), b(_b), index(static_cast<unsigned int>(ind)), c(home) {
    i.subscribe(home, *this, Int::PC_INT_DOM);
    x.subscribe(home, *new (home) Advisor(home,*this,c));
    b.subscribe(home, *this, Int::PC_BOOL_VAL);
  }
  
  template<class SView, class IView, class BView>
  ExecStatus
  Reif<SView,IView,BView>::advise(Space&, Advisor&, const Delta& d) {
    if (x.any(d) || (x.index(d) == static_cast<int>(index)))
      return ES_NOFIX;
    if (x.length(d))
      if ((x.length_min(d) <= index) && (x.length_max(d) >= index))
        return ES_NOFIX;
    return ES_FIX;
  }
  
  template<class SView, class IView, class BView>
  Reif<SView,IView,BView>::Reif(Home home, bool share, Reif& p)
  : Propagator(home, share, p), index(p.index) {
    x.update(home, share, p.x);
    i.update(home, share, p.i);
    c.update(home, share, p.c);
    b.update(home, share, p.b);
  }
  
  template<class SView, class IView, class BView>
  size_t 
  Reif<SView,IView,BView>::dispose(Space& home) {
    i.cancel(home, *this, Int::PC_INT_DOM);
    b.cancel(home, *this, Int::PC_BOOL_VAL);
    x.cancel(home,Advisors<Advisor>(c).advisor());
    c.dispose(home);
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }
  
  template<class SView, class IView, class BView>
  ExecStatus 
  Reif<SView,IView,BView>::post(Space& home, SView x, int ind, IView i, BView b) {
    (void) new (home) Reif(home,x,ind,i,b);
    return ES_OK;
  }
  
  template<class SView, class IView, class BView>
  ExecStatus 
  Reif<SView,IView,BView>::propagate(Space& home, const Gecode::ModEventDelta&) {
    if (b.one())
      GECODE_REWRITE(*this,(Base<SView,IView>::post(home(*this),x,index,i)));
    if (b.zero()) {
      GECODE_ME_CHECK(x.lle(home,index));
      return home.ES_SUBSUMED(*this);
    }
    
    if (index >= x.lengthmax()) {
      GECODE_ME_CHECK(b.zero(home));
      return home.ES_SUBSUMED(*this);
    }
    if (index < x.lengthmin()) {
      GECODE_ME_CHECK(b.one(home));
      GECODE_REWRITE(*this,(Base<SView,IView>::post(home(*this),x,index,i)));
    }
    BitSetIter x_delta(x.domain(index));
    Int::ViewValues<IView> i_delta(i);
    Iter::Values::Inter< BitSetIter, Int::ViewValues<IView> > overlap(x_delta,i_delta);
    if (!overlap()) {
      GECODE_ME_CHECK(b.zero(home));
      GECODE_ME_CHECK(x.lle(home,index));
      return home.ES_SUBSUMED(*this);
    }
    return ES_FIX;
  }
}}}