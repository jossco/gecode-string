/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-22 03:57:09 +0200 (Fri, 22 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14979 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


namespace Gecode { namespace String { namespace Channel {
  
  template<class SView, class IView>
  Base<SView,IView>::Base(Home home, SView _x, int ind, IView _i)
  : Propagator(home), x(_x), i(_i), index(static_cast<unsigned int>(ind)), c(home) {
    i.subscribe(home, *this, Int::PC_INT_DOM);
    x.subscribe(home, *new (home) Advisor(home,*this,c));
  }
  
  template<class SView, class IView>
  ExecStatus
  Base<SView,IView>::advise(Space&, Advisor&, const Delta& d) {
    if (x.any(d) || (x.index(d) == static_cast<int>(index)))
      return ES_NOFIX;
    if (x.length(d))
      if ((x.length_min(d) <= index) && (x.length_max(d) >= index))
        return ES_FAILED;
    return ES_FIX;
  }
  
  template<class SView, class IView>
  Base<SView,IView>::Base(Home home, bool share, Base& p)
  : Propagator(home, share, p), index(p.index) {
    x.update(home, share, p.x);
    i.update(home, share, p.i);
    c.update(home, share, p.c);
  }
  
  template<class SView, class IView>
  size_t 
  Base<SView,IView>::dispose(Space& home) {
    i.cancel(home, *this, Int::PC_INT_DOM);
    x.cancel(home,Advisors<Advisor>(c).advisor());
    c.dispose(home);
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }
  
  template<class SView, class IView>
  ExecStatus 
  Base<SView,IView>::post(Space& home, SView x, int ind, IView i) {
    GECODE_ME_CHECK(x.lgq(home,ind));
    (void) new (home) Base(home,x,ind,i);
    return ES_OK;
  }
  
  template<class SView, class IView>
  ExecStatus 
  Base<SView,IView>::propagate(Space& home, const Gecode::ModEventDelta&) {
    Int::ViewValues<IView> idom(i);
    GECODE_ME_CHECK(x.inter_v(home,index,idom,false));
    BitSetIter xdom(x.domain(index));
    GECODE_ME_CHECK(i.narrow_v(home,xdom,false));
    if (x.charfixed(index) && i.assigned())
      return home.ES_SUBSUMED(*this);
    else
      return ES_FIX;
  }
}}}