/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-05 10:24:04 +0200 (Tue, 05 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14947 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <gecode/string/character.hh>

namespace Gecode {
  
  void
  character(Space& home, StringVar _x, IntVar _c, IntVar _i) {
    if(home.failed()) return;
    String::StringView x(_x);
    Int::IntView c0(_c);
    String::CharacterView c(c0);
    Int::IntView i(_i);
    GECODE_ES_FAIL((String::Substring::Base< String::StringView, String::CharacterView, Int::IntView >::post(home,x,c,i)));
  }
  
  void
  character(Space& home, StringVar _x, int _c, IntVar _i) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::ConstCharacterView c(_c);
    Int::IntView i(_i);
    GECODE_ES_FAIL((String::Substring::Base< String::StringView, String::ConstCharacterView, Int::IntView >::post(home,x,c,i)));
  }
  
  void
  character(Space& home, StringVar _x, IntVar _c, int _i) {
    if(home.failed()) return;
    String::StringView x(_x);
    Int::IntView c0(_c); String::CharacterView c(c0);
    Int::ConstIntView i(_i);
    GECODE_ES_FAIL((String::Substring::Base< String::StringView, String::CharacterView, Int::ConstIntView >::post(home,x,c,i)));
  }
  
  void
  character(Space& home, StringVar _x, int _c, int _i) {
    if (home.failed()) return; 
    String::StringView x(_x);
    String::ConstCharacterView c(_c);
    Int::ConstIntView i(_i);
    GECODE_ES_FAIL((String::Substring::Base< String::StringView, String::ConstCharacterView, Int::ConstIntView >::post(home,x,c,i)));
  }
  
  void
  character(Space& home, IntArgs& _x, IntVar _c, IntVar _i) {
    if(home.failed()) return;
    String::ConstStringView x(home,_x);
    Int::IntView c0(_c);
    String::CharacterView c(c0);
    Int::IntView i(_i);
    GECODE_ES_FAIL((String::Substring::Base< String::ConstStringView, String::CharacterView, Int::IntView >::post(home,x,c,i)));
  }
  
  void
  character(Space& home, IntArgs& _x, IntVar _c, int _i) {
    if(home.failed()) return;
    String::ConstStringView x(home,_x);
    Int::IntView c0(_c); String::CharacterView c(c0);
    Int::ConstIntView i(_i);
    GECODE_ES_FAIL((String::Substring::Base< String::ConstStringView, String::CharacterView, Int::ConstIntView >::post(home,x,c,i)));
  }
  
  void
  character(Space& home, IntArgs& _x, int _c, IntVar _i) {
    if(home.failed()) return;
    String::ConstStringView x(home,_x);
    String::ConstCharacterView c(_c);
    Int::IntView i(_i);
    GECODE_ES_FAIL((String::Substring::Base< String::ConstStringView, String::ConstCharacterView, Int::IntView >::post(home,x,c,i)));
  }

}