/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-05 10:24:04 +0200 (Tue, 05 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14947 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __GECODE_STRING_EXTENSIONAL_HH__
#define __GECODE_STRING_EXTENSIONAL_HH__

#include <gecode/string.hh>

namespace Gecode { namespace String { namespace Extensional {
     /**
      * \brief Domain consistent layered graph (regular) propagator
      *
      * The algorithm for the regular propagator is based on:
      *   Gilles Pesant, A Regular Language Membership Constraint
      *   for Finite Sequences of Variables, CP 2004.
      *   Pages 482-495, LNCS 3258, Springer-Verlag, 2004.
      *
      * The propagator is not capable of dealing with multiple occurences
      * of the same view.
      *
      * Requires \code #include <gecode/string/extensional.hh> \endcode
      * \ingroup FuncIntProp
      */
     template<class View, class Val, class Degree, class StateIdx>
     class LayeredGraph : public Propagator {
     protected:
       /// States are described by number of incoming and outgoing edges
       class State {
       public:
         Degree i_deg; ///< The in-degree (number of incoming edges)
         Degree o_deg; ///< The out-degree (number of outgoing edges)
         /// Initialize with zeroes
         void init(void);
       };
       /// %Edge defined by in-state and out-state
       class Edge {
       public:
         StateIdx i_state; ///< Number of in-state
         StateIdx o_state; ///< Number of out-state
       };
       /// %Support information for a value
       class Support {
       public:
         Val val; ///< Supported value
         Degree n_edges; ///< Number of supporting edges
         Edge* edges; ///< Supporting edges in layered graph
       };
       /// Type for support size
       typedef typename Gecode::Support::IntTypeTraits<Val>::utype ValSize;
       /// %Layer for a view in the layered graph
       class Layer {
       public:
         StateIdx n_states; ///< Number of states used by outgoing edges
         ValSize size; ///< Number of supported values
         State* states; ///< States used by outgoing edges
         Support* support; ///< Supported values
       };
       /// Iterator for telling variable domains by scanning support
       class LayerValues {
       private:
         const Support* s1; ///< Current support
         const Support* s2; ///< End of support
       public:
         /// Default constructor
         LayerValues(void);
         /// Initialize for support of layer \a l
         LayerValues(const Layer& l);
         /// Initialize for support of layer \a l
         void init(const Layer& l);
         /// Test whether more values supported
         bool operator ()(void) const;
         /// Move to next supported value
         void operator ++(void);
         /// Return supported value
         int val(void) const;
       };
       /// Range approximation of which positions have changed
       class IndexRange {
       private:
         int _fst; ///< First index
         int _lst; ///< Last index
       public:
         /// Initialize range as empty
         IndexRange(void);
         /// Reset range to be empty
         void reset(void);
         /// Add index \a i to range
         void add(int i);
         /// Add index range \a ir to range
         void add(const IndexRange& ir);
         /// Shift index range by \a n elements to the left
         void lshift(int n);
         /// Test whether range is empty
         bool empty(void) const;
         /// Return first position
         int fst(void) const;
         /// Return last position
         int lst(void) const;
       };
       /// The bounded-length string
       View x;
       /// The advisor council
       Council<Advisor> c;
       /// The original DFA
       DFA dfa;
       /// Number of layers (and views)
       int n;
       /// The distance from node i to any final state in the dfa
       IntSharedArray distance;
       /// min distance from last layer in lgp, to any dfa-final state
       int mindist;
       /// map from last layer nodes to dfa states
       int* dfa_map;
       /// The layers of the graph
       Layer* layers;
       /// Maximal number of states per layer
       StateIdx max_states;
       /// Total number of states
       unsigned int n_states;
       /// Total number of edges
       unsigned int n_edges;
       /// Index range with in-degree modifications
       IndexRange i_ch;
       /// Index range with out-degree modifications
       IndexRange o_ch;
       /// Index range for any change (for compression)
       IndexRange a_ch;
       /// Return in state for layer \a i and state index \a is
       State& i_state(int i, StateIdx is);
       /// Return in state for layer \a i and in state of edge \a e
       State& i_state(int i, const Edge& e);
       /// Decrement out degree for in state of edge \a e for layer \a i
       bool i_dec(int i, const Edge& e);
       /// Return out state for layer \a i and state index \a os
       State& o_state(int i, StateIdx os);
       /// Return state for layer \a i and out state of edge \a e
       State& o_state(int i, const Edge& e);
       /// Decrement in degree for out state of edge \a e for layer \a i
       bool o_dec(int i, const Edge& e);
       /// Perform consistency check on data structures
       void audit(void);
       /// Determine 'finality' of states in layers[i]
       bool finalize(int i);
       bool finalize(int i, int num_states);
       /// Add new layers to layered graph
       ExecStatus extend(Space& home);
       /// Initialize layered graph
       ExecStatus initialize(Space& home);
       /// Constructor for cloning \a p
       LayeredGraph(Space& home, bool share,
                    LayeredGraph<View,Val,Degree,StateIdx>& p);
     public:
       /// Constructor for posting
       LayeredGraph(Home home, View x, const DFA& dfa, const IntArgs& dist);
       /// Copy propagator during cloning
       virtual Actor* copy(Space& home, bool share);
       /// Cost function (defined as high linear)
       virtual PropCost cost(const Space& home, const ModEventDelta& med) const;
       /// Give advice to propagator
       virtual ExecStatus advise(Space& home, Advisor& a, const Delta& d);
       /// Perform propagation
       virtual ExecStatus propagate(Space& home, const ModEventDelta& med);
       /// Delete propagator and return its size
       virtual size_t dispose(Space& home);
       /// Post propagator on views \a x and DFA \a dfa
       static ExecStatus post(Home home, View x, const DFA& dfa);
     };
     
     /// Select small types for the layered graph propagator
     template<class View>
     ExecStatus post_lgp(Home home, View x, const DFA& dfa);
}}}

#include <gecode/string/extensional/layered-graph.hpp>

#endif