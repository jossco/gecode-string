/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */


// namespace Gecode { namespace String { namespace Alphabet {
//
//   template<class StringV, class SetV>
//   Strict<StringV,SetV>::Strict(Home home, StringV _x, SetV _alpha)
//   : Propagator(home), x(_x), alpha(_alpha) {
//     alpha.subscribe(home, *this, Set::PC_SET_ANY);
//     x.subscribe(home, *this, PC_STRING_ANY);
//   }
//
//   template<class StringV, class SetV>
//   Strict<StringV,SetV>::Strict(Home home, bool share, Strict& p)
//   : Propagator(home, share, p) {
//     x.update(home, share, p.x);
//     alpha.update(home, share, p.alpha);
//   }
//
//   template<class StringV, class SetV>
//   size_t
//   Strict<StringV,SetV>::dispose(Space& home) {
//     alpha.cancel(home, *this, Set::PC_SET_ANY);
//     x.cancel(home,*this,PC_STRING_ANY);
//     (void) Propagator::dispose(home);
//     return sizeof(*this);
//   }
//
//   template<class StringV, class SetV>
//   ExecStatus
//   Strict<StringV,SetV>::post(Space& home, StringV x, SetV alpha) {
//     // x must be long enough to handle all unique symbols in alpha
//     GECODE_ME_CHECK(x.lgq(home,alpha.cardMin()));
//     (void) new (home) Strict(home,x,alpha);
//     return ES_OK;
//   }
//
//   template<class StringV, class SetV>
//   ExecStatus
//   Strict<StringV,SetV>::propagate(Space& home, const Gecode::ModEventDelta&) {
//     GECODE_ME_CHECK(x.lgq(home,alpha.cardMin()));
//     BitSet xlub = Empty;
//     BitSet xglb = Empty;
//     for (int i = 0; i < x.lengthmax(); i++) {
//       Set::LubRanges<SetV> alubr(alpha);
//       Iter::Ranges::ToValues< Set::LubRanges<SetV> > alub(alubr);
//       GECODE_ME_CHECK(x.inter_v(home,i,alub));
//
//       if (x.charfixed(i)) {
//         GECODE_ME_CHECK(alpha.include(home,x.charval(i)));
//         xglb = unionof(xglb,x.domain(i));
//       }
//       xlub = unionof(xlub,x.domain(i));
//     }
//     BitSetRangeIter xlubr(xlub);
//     GECODE_ME_CHECK(alpha.intersectI(home,xlubr));
//
//     if (alpha.assigned()) {
//       Set::GlbRanges<SetV> aglbr(alpha); BitSetRangeIter xglbr(xglb);
//       Iter::Ranges::Diff< Set::GlbRanges<SetV>, BitSetRangeIter > diff(aglbr,xglbr);
//       if (!diff()) {
//         // each symbol in alpha occurs in at least one assigned character
//         return home.ES_SUBSUMED(*this);
//       }
//     }
//
//     return ES_FIX;
//   }
// }}}