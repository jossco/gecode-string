/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-04 14:20:39 +0200 (Mon, 04 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14946 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Extensional {

  /*
   * States
   */
  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::State::init(void) {
    i_deg=o_deg=0; 
  }

  template<class View, class Val, class Degree, class StateIdx>
  forceinline typename LayeredGraph<View,Val,Degree,StateIdx>::State& 
  LayeredGraph<View,Val,Degree,StateIdx>::i_state(int i, StateIdx is) {
    return layers[i].states[is];
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline typename LayeredGraph<View,Val,Degree,StateIdx>::State& 
  LayeredGraph<View,Val,Degree,StateIdx>::i_state
  (int i, const typename LayeredGraph<View,Val,Degree,StateIdx>::Edge& e) {
    return i_state(i,e.i_state);
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline bool 
  LayeredGraph<View,Val,Degree,StateIdx>::i_dec
  (int i, const typename LayeredGraph<View,Val,Degree,StateIdx>::Edge& e) {
    return --i_state(i,e).o_deg == 0;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline typename LayeredGraph<View,Val,Degree,StateIdx>::State& 
  LayeredGraph<View,Val,Degree,StateIdx>::o_state(int i, StateIdx os) {
    return layers[i+1].states[os];
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline typename LayeredGraph<View,Val,Degree,StateIdx>::State& 
  LayeredGraph<View,Val,Degree,StateIdx>::o_state
  (int i, const typename LayeredGraph<View,Val,Degree,StateIdx>::Edge& e) {
    return o_state(i,e.o_state);
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline bool 
  LayeredGraph<View,Val,Degree,StateIdx>::o_dec
  (int i, const typename LayeredGraph<View,Val,Degree,StateIdx>::Edge& e) {
    return --o_state(i,e).i_deg == 0;
  }


  /*
   * Value iterator
   */
  template<class View, class Val, class Degree, class StateIdx>
  forceinline
  LayeredGraph<View,Val,Degree,StateIdx>::LayerValues::LayerValues(void) {}
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline
  LayeredGraph<View,Val,Degree,StateIdx>::LayerValues
  ::LayerValues(const Layer& l)
    : s1(l.support), s2(l.support+l.size) {}
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::LayerValues::init(const Layer& l) {
    s1=l.support; s2=l.support+l.size;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline bool
  LayeredGraph<View,Val,Degree,StateIdx>::LayerValues
  ::operator ()(void) const {
    return s1<s2;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::LayerValues::operator ++(void) {
    s1++;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline int
  LayeredGraph<View,Val,Degree,StateIdx>::LayerValues::val(void) const {
    return s1->val;
  }

  /*
   * Index ranges
   *
   */
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::IndexRange(void)
    : _fst(INT_MAX), _lst(INT_MIN) {}
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::reset(void) {
    _fst=INT_MAX; _lst=INT_MIN;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::add(int i) {
    _fst=std::min(_fst,i); _lst=std::max(_lst,i);
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::add
  (const typename LayeredGraph<View,Val,Degree,StateIdx>::IndexRange& ir) {
    _fst=std::min(_fst,ir._fst); _lst=std::max(_lst,ir._lst);
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline bool
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::empty(void) const {
    return _fst>_lst;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::lshift(int n) {
    if (empty())
      return;
    if (n > _lst) {
      reset();
    } else {
      _fst = std::max(0,_fst-n);
      _lst -= n;
    }
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline int
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::fst(void) const {
    return _fst;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline int
  LayeredGraph<View,Val,Degree,StateIdx>::IndexRange::lst(void) const {
    return _lst;
  }



  /*
   * The layered graph
   *
   */

  template<class View, class Val, class Degree, class StateIdx>
  forceinline
  LayeredGraph<View,Val,Degree,StateIdx>::LayeredGraph(Home home,
                                                       View _x, 
                                                       const DFA& dfa,
                                                       const IntArgs& dist)
  : Propagator(home), 
    x(_x),
    c(home), 
    dfa(dfa),
    n(0),
    distance(dist),
    max_states(static_cast<StateIdx>(dfa.n_states())),
    n_states(0),
    n_edges(0) 
  {
    home.notice(*this,AP_DISPOSE);
  }

  template<class View, class Val, class Degree, class StateIdx>
  forceinline void
  LayeredGraph<View,Val,Degree,StateIdx>::audit(void) {
#ifdef GECODE_AUDIT
    // Check states and edge information to be consistent
    unsigned int n_e = 0; // Number of edges
    unsigned int n_s = 0; // Number of states
    StateIdx m_s = 0; // Maximal number of states per layer
    for (int i=n; i--; ) {
      n_s += layers[i].n_states;
      m_s = std::max(m_s,layers[i].n_states);
      for (ValSize j=layers[i].size; j--; )
        n_e += layers[i].support[j].n_edges;
    }
    n_s += layers[n].n_states;
    m_s = std::max(m_s,layers[n].n_states);
    assert(n_e == n_edges);
    assert(n_s <= n_states);
    assert(m_s <= max_states);
#endif
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline bool
  LayeredGraph<View,Val,Degree,StateIdx>::finalize(int i, int num_states) {
    mindist = x.lengthmax() + 1;
    bool changed = false;
    
    for (int s=num_states; s--; ) {
      if ((o_state(i-1,static_cast<StateIdx>(s)).i_deg != 0)
          && (distance[dfa_map[s]] + i <= x.lengthmax())) {
            o_state(i-1,static_cast<StateIdx>(s)).o_deg = 1;
            mindist = std::min(mindist,distance[dfa_map[s]]);
      } else {
        if (o_state(i-1,static_cast<StateIdx>(s)).o_deg != 0) {
          changed = true;
          o_ch.add(i-1);
        }
        o_state(i-1,static_cast<StateIdx>(s)).o_deg = 0;
      }
    }
    return changed;
  }

  template<class View, class Val, class Degree, class StateIdx>
  forceinline bool
  LayeredGraph<View,Val,Degree,StateIdx>::finalize(int i) {
    return finalize(i, layers[i].n_states);
  }
    
  template<class View, class Val, class Degree, class StateIdx>
  forceinline ExecStatus
  LayeredGraph<View,Val,Degree,StateIdx>::extend(Space& home) {
    assert(n < x.lengthmin());
    Region r(home);
    
    // Allocate memory for all possible states in new layers
    State* states = r.alloc<State>(max_states*(x.lengthmin()-n));
    for (int i= static_cast<int>(max_states*(x.lengthmin()-n)); i--; )
      states[i].init();
    
    for (int i=x.lengthmin(); i > n; i--) {
      layers[i].states = states + ((i-n)-1)*max_states;
    }

    // undo outgoing edges of "final" states in previous last layer
    for (StateIdx j=0; j < layers[n].n_states; j++){
      layers[n].states[j].o_deg = 0;
    }
    
    // Allocate temporary memory for edges
    Edge* edges = r.alloc<Edge>(dfa.max_degree());

    
    
    int* layer_map = r.alloc<int>(max_states);
    for (int i = max_states; i--; )
      layer_map[i] = -1;
    for (int i = layers[n].n_states; i--; )
      layer_map[dfa_map[i]] = i;
    
    // BitSet alpha = Empty;
//     for (DFA::Symbols sym(dfa); sym(); ++sym) {
//       alpha = unionof(alpha,singletonfor(sym.val()));
//     }
    
    // Forward pass: add transitions
    for (int i=n; i<x.lengthmin(); i++) {
      // value iterator for set of symbols in dfa
      // DFA::Symbols sym(dfa);
 //      GECODE_ME_CHECK(x.inter_v(home,i,sym,false));
      
      // GECODE_ME_CHECK(x.inter(home,i,alpha));
      
      layers[i].support = home.alloc<Support>(x.charwidth(i));
      ValSize j=0;
      // Enter links leaving reachable states (indegree != 0)
      for (BitSetIter nx(x.domain(i)); nx(); ++nx) {
        Degree n_edges=0;
        if (i == n) {
          for (DFA::Transitions t(dfa,nx.val()); t(); ++t)
            if (layer_map[t.i_state()] >= 0 &&
          i_state(i,static_cast<StateIdx>(layer_map[t.i_state()])).i_deg != 0) {
            i_state(i,static_cast<StateIdx>(layer_map[t.i_state()])).o_deg++;
            o_state(i,static_cast<StateIdx>(t.o_state())).i_deg++;
            edges[n_edges].i_state = static_cast<StateIdx>(layer_map[t.i_state()]);
            edges[n_edges].o_state = static_cast<StateIdx>(t.o_state());
            n_edges++;
          }
        } else {
          for (DFA::Transitions t(dfa,nx.val()); t(); ++t)
          if (i_state(i,static_cast<StateIdx>(t.i_state())).i_deg != 0) {
            i_state(i,static_cast<StateIdx>(t.i_state())).o_deg++;
            o_state(i,static_cast<StateIdx>(t.o_state())).i_deg++;
            edges[n_edges].i_state = static_cast<StateIdx>(t.i_state());
            edges[n_edges].o_state = static_cast<StateIdx>(t.o_state());
            n_edges++;
          }
        }
        assert(n_edges <= dfa.max_degree());
        // Found support for value
        if (n_edges > 0) {
          Support& s = layers[i].support[j];
          s.val = static_cast<Val>(nx.val());
          s.n_edges = n_edges;
          s.edges = Gecode::Heap::copy(home.alloc<Edge>(n_edges),edges,n_edges);
          j++;
        }
      }
      if ((layers[i].size = j) == 0)
        return ES_FAILED;
    }
    // Mark states that are "close enough" to a dfa-final state
    for (int i = max_states; i--; )
      dfa_map[i] = i;
    (void) finalize(x.lengthmin(), max_states);
    
    // Backward pass: prune all transitions that do not lead to final state
    n_edges = 0;
    for (int i=x.lengthmin(); i--; ) {
      ValSize k=0;
      for (ValSize j=0; j<layers[i].size; j++) {
        Support& s = layers[i].support[j];
        for (Degree d=s.n_edges; d--; )
        if (o_state(i,s.edges[d]).o_deg == 0) {
          // Adapt states
          i_dec(i,s.edges[d]); o_dec(i,s.edges[d]);
          // Prune edge
          s.edges[d] = s.edges[--s.n_edges];
        }
        // Value has support, copy the support information
        if (s.n_edges > 0) {
          layers[i].support[k++]=s;
          n_edges += s.n_edges;
        }
      }
      if ((layers[i].size = k) == 0)
        return ES_FAILED;
      LayerValues lv(layers[i]);
      GECODE_ME_CHECK(x.inter_v(home,i,lv,false));
    }
  
    // Copy and compress states, setup other information
    {
      // State map for in-states
      StateIdx* i_map = r.alloc<StateIdx>(max_states);
      // State map for out-states
      StateIdx* o_map = r.alloc<StateIdx>(max_states);
      // Number of in-states
      StateIdx i_n = 0;
    
      // Initialize map for in-states
      for (StateIdx j=max_states; j--; )
        if ((layers[x.lengthmin()].states[j].o_deg != 0) ||
      (layers[x.lengthmin()].states[j].i_deg != 0)) {
        dfa_map[static_cast<int>(i_n)] = static_cast<int>(j);
        i_map[j]=i_n++;
      }
    
      layers[x.lengthmin()].n_states = i_n;
    
      // Total number of states
      long unsigned int n_s = i_n;

      // compress the new layers
      for (int i=x.lengthmin()-1; i>n; i--) {
        // In-states become out-states
        std::swap(o_map,i_map); i_n=0;
        // Initialize map for in-states
        for (StateIdx j=max_states; j--; )
          if ((layers[i].states[j].o_deg != 0) ||
            (layers[i].states[j].i_deg != 0))
              i_map[j]=i_n++;
        layers[i].n_states = i_n;
        n_s += i_n;

        // Update states in edges
        for (ValSize j=layers[i].size; j--; ) {
          Support& s = layers[i].support[j];
          //n_edges += s.n_edges;
          for (Degree d=s.n_edges; d--; ) {
            s.edges[d].i_state = i_map[s.edges[d].i_state];
            s.edges[d].o_state = o_map[s.edges[d].o_state];
          }
        }
      }
    
      // map the (already compressed) old final layer
      std::swap(o_map,i_map);
      // Initialize map for in-states
      for (StateIdx j=0; j<layers[n].n_states; j++)
        i_map[j]=j;

      // Update states in edges
      for (ValSize j=layers[n].size; j--; ) {
        Support& s = layers[n].support[j];
        //n_edges += s.n_edges;
        for (Degree d=s.n_edges; d--; ) {
          s.edges[d].i_state = i_map[s.edges[d].i_state];
          s.edges[d].o_state = o_map[s.edges[d].o_state];
        }
      }

      // Allocate and copy states
      State* a_states = home.alloc<State>(n_s);
      for (int i=x.lengthmin(); i > n; i--) {
        StateIdx k=0;
        for (StateIdx j=max_states; j--; )
          if ((layers[i].states[j].o_deg != 0) ||
            (layers[i].states[j].i_deg != 0))
              a_states[k++] = layers[i].states[j];
        assert(k == layers[i].n_states);
        layers[i].states = a_states;
        a_states += layers[i].n_states;
      }

      n_states += n_s;
    }
  
    n = x.lengthmin();
    audit();
    return ES_OK;
  }
  
  template<class View, class Val, class Degree, class StateIdx>
  forceinline ExecStatus
  LayeredGraph<View,Val,Degree,StateIdx>::initialize(Space& home) {
    dfa_map = home.alloc<int>(max_states);
    
    
    // Allocate memory for layers
    // Allocate enough for x.lengthmax() up front,
    // even though only x.lengthmin()+1 layers used now.
    layers = home.alloc<Layer>(x.lengthmax()+1);
    
    // Set up the inital state (layer 0)
    State* initial = home.alloc<State>(1);
    initial->init();
    layers[0].states = initial;
    layers[0].n_states = 1;
    n_states = 1;
    // Mark as being reachable,
    layers[0].states[0].i_deg = 1;
    // and as leading to a final state.
    layers[0].states[0].o_deg = 1;
    dfa_map[0]=0;
    
    // shortest accepted string gives a lower bound on length
    GECODE_ME_CHECK(x.lgq(home,distance[0]));
  

    while (n < x.lengthmin()){
      // Add new layers
      if (extend(home) == ES_FAILED)
        return ES_FAILED;
      // Increase min length if there are no dfa-final states in last layer
      GECODE_ME_CHECK(x.lgq(home,n+mindist));
    }
    
    x.subscribe(home,*new (home) Advisor(home,*this,c));
    
    // Schedule if subsumption is needed
    if (x.assigned())
        View::schedule(home,*this,ME_STRING_VAL);
    
    audit();
    return ES_OK;
  }

  template<class View, class Val, class Degree, class StateIdx>
  ExecStatus
  LayeredGraph<View,Val,Degree,StateIdx>::advise(Space& home,
                                                 Advisor&, const Delta& d) {
    // FIXME: Why was this here?
    //if(n_states == 0)
    //  return ES_NOFIX; // haven't even initialized the lgp yet
    
    // Check whether state information has already been created
    if (layers[0].states == NULL) {
      State* states = home.alloc<State>(n_states);
      for (unsigned int i=n_states; i--; )
        states[i].init();
      layers[n].states = states;
      states += layers[n].n_states;
      for (int i=n; i--; ) {
        layers[i].states = states;
        states += layers[i].n_states;
        for (ValSize j=layers[i].size; j--; ) {
          Support& s = layers[i].support[j];
          for (Degree d=s.n_edges; d--; ) {
            i_state(i,s.edges[d]).o_deg++;
            o_state(i,s.edges[d]).i_deg++;
          }
        }
      }
    }

    bool fix = true;
    
    if (x.symbol(d)) {
      int i = x.index(d);
      assert((i >= 0) && (i < x.lengthmax()));
      if (layers[i].size <= x.charwidth(i)) {
        // Propagator has already done everything
        return ES_FIX;
      }

      bool i_mod = false;
      bool o_mod = false;

      if (x.modevent(d) == ME_STRING_CV) {
        Val n = static_cast<Val>(x.charval(i));
        // Remove supported values less than n
        ValSize j=0;
        for (; layers[i].support[j].val < n; j++) {
          Support& s = layers[i].support[j];
          n_edges -= s.n_edges;
          for (Degree d=s.n_edges; d--; ) {
            // Adapt states
            o_mod |= i_dec(i,s.edges[d]);
            i_mod |= o_dec(i,s.edges[d]);
          }
        }
        // n becomes the first supported value
        assert(layers[i].support[j].val == n);
        layers[i].support[0] = layers[i].support[j++];
        // remove supported values greater than n
        ValSize s=layers[i].size;
        layers[i].size = 1;
        for (; j<s; j++) {
          Support& s = layers[i].support[j];
          n_edges -= s.n_edges;
          for (Degree d=s.n_edges; d--; ) {
            // Adapt states
            o_mod |= i_dec(i,s.edges[d]);
            i_mod |= o_dec(i,s.edges[d]);
          }
        }
      } else {
        ValSize j=0;
        ValSize k=0;
        ValSize s=layers[i].size;
        while (j<s) {
          Support& s = layers[i].support[j];
          if (x.in(i,s.val)) {
            layers[i].support[k++]=s;
          } else {
            // Supported value not any longer in view
            n_edges -= s.n_edges;
            for (Degree d=s.n_edges; d--; ) {
              // Adapt states
              o_mod |= i_dec(i,s.edges[d]);
              i_mod |= o_dec(i,s.edges[d]);
            }
          }
          ++j;
        }
      
        assert(k > 0);
        layers[i].size = k;
      }
      if (o_mod && (i > 0)) {
        o_ch.add(i-1); fix = false;
       }
      if (i_mod && (i+1 < n)) {
        i_ch.add(i+1); fix = false;
      }
    }

    // TODO: this test might yield a minor efficiency increase
//      if (x.length(d) || (x.symbol(d) && x.index(d) == n-1)) {
    fix &= !finalize(n);
//      }
      
    audit();

    if(n + mindist > x.lengthmax())
      return ES_FAILED;
    if (x.lengthmin() > n)
        return ES_NOFIX;

    return fix ? ES_FIX : ES_NOFIX;
  }


  template<class View, class Val, class Degree, class StateIdx>
  forceinline size_t
  LayeredGraph<View,Val,Degree,StateIdx>::dispose(Space& home) {
    home.ignore(*this,AP_DISPOSE);
    x.cancel(home,Advisors<Advisor>(c).advisor());
    c.dispose(home);
    distance.~IntSharedArray();
    dfa.~DFA();
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }

  template<class View, class Val, class Degree, class StateIdx>
  ExecStatus
  LayeredGraph<View,Val,Degree,StateIdx>::propagate(Space& home, const ModEventDelta&) {
    
    // Forward pass
    for (int i=i_ch.fst(); i<=i_ch.lst(); i++) {
      bool i_mod = false;
      bool o_mod = false;
      ValSize j=0;  // index for values with prior support
      ValSize k=0;  // index for values that still have support
      ValSize s=layers[i].size;
      do {
        Support& s=layers[i].support[j];
        n_edges -= s.n_edges;
        for (Degree d=s.n_edges; d--; )
          if (i_state(i,s.edges[d]).i_deg == 0) {
            // Adapt states
            o_mod |= i_dec(i,s.edges[d]);
            i_mod |= o_dec(i,s.edges[d]);
            // Remove edge
            s.edges[d] = s.edges[--s.n_edges];
          }
        n_edges += s.n_edges;
        // Check whether value is still supported
        if (s.n_edges == 0) {
          layers[i].size--;
          GECODE_ME_CHECK(x.cnq(home,i,s.val));
        } else {
          layers[i].support[k++]=s;
        }
      } while (++j<s);
      assert(k > 0);
      // Update modification information
      if (o_mod && (i > 0))
        o_ch.add(i-1);
      if (i_mod && (i+1 < n))
        i_ch.add(i+1);
    }
    
    /// Update lower bound of length if
    /// there is no dfa-final state in layer[n]:
    // bool o_mod = finalize(n);
    // if (o_mod)
    //   o_ch.add(n-1);
    
    bool repeat;
    do {
      repeat = false;
      // Increase min length if there are no dfa-final states in last layer
      GECODE_ME_CHECK(x.lgq(home, n + mindist));
    
      // Extend layered graph if min length has increased
      if (n < x.lengthmin()){
        repeat = true;
        if (extend(home) == ES_FAILED)
          return ES_FAILED;
        // already did full bw pass in extend()
        //a_ch.add(o_ch);
        //o_ch.reset();
      }
    } while (repeat);
    
    // Backward pass
    for (int i=o_ch.lst(); i>=o_ch.fst(); i--) {
      bool o_mod = false;
      ValSize j=0;
      ValSize k=0;
      ValSize s=layers[i].size;
      do {
        Support& s=layers[i].support[j];
        n_edges -= s.n_edges;
        for (Degree d=s.n_edges; d--; )
          if (o_state(i,s.edges[d]).o_deg == 0) {
            // Adapt states
            o_mod |= i_dec(i,s.edges[d]);
            (void)   o_dec(i,s.edges[d]);
            // Remove edge
            s.edges[d] = s.edges[--s.n_edges];
          }
        n_edges += s.n_edges;
        // Check whether value is still supported
        if (s.n_edges == 0) {
          layers[i].size--;
          GECODE_ME_CHECK(x.cnq(home,i,s.val));
        } else {
          layers[i].support[k++]=s;
        }
      } while (++j<s);
      assert(k > 0);
      // Update modification information
      if (o_mod && (i > 0))
        o_ch.add(i-1);
    }
    
    a_ch.add(i_ch); i_ch.reset();
    a_ch.add(o_ch); o_ch.reset();

    audit();

    // Check subsumption
    if (x.assigned())
      return home.ES_SUBSUMED(*this);
    
    return ES_FIX;
  }

  template<class View, class Val, class Degree, class StateIdx>
  ExecStatus
  LayeredGraph<View,Val,Degree,StateIdx>::post(Home home, 
                                               View x,
                                               const DFA& dfa) 
  {
    BitSet alpha = Empty;
    for (DFA::Symbols sym(dfa); sym(); ++sym) {
      alpha = unionof(alpha,singletonfor(sym.val()));
    }
    for(int i=x.lengthmin(); i--; )
      GECODE_ME_CHECK(x.inter(home,i,alpha));
    
    if (x.lengthmax() == 0) {
      // Check whether the start state 0 is also a final state
      if ((dfa.final_fst() <= 0) && (dfa.final_lst() >= 0))
        return ES_OK;
      return ES_FAILED;
    }
    
    // Check if there are any final states
    if (dfa.final_fst() == dfa.final_lst())
      return ES_FAILED;
    
    // Check if any final state is reachable
    // compute shortest distance to final state for all states
    // basically Bellman-Ford, with all final states starting at dist=0
    IntArgs dist = IntArgs::create(dfa.n_states(),dfa.n_states()+1,0);
    for (int s = static_cast<int>(dfa.final_fst()); 
         s < static_cast<int>(dfa.final_lst()); 
         s++) {
      dist[s] = 0;
    }
    for (int i = 0; i < static_cast<int>(dfa.n_states()); i++) {
      DFA::Transitions t(dfa);
      while(t()) {
        if (dist[t.i_state()] > dist[t.o_state()] + 1) {
          dist[t.i_state()] = dist[t.o_state()] + 1;
        }
        ++t;
      }
    }
    if (dist[0] > x.lengthmax())
      return ES_FAILED;
    
    LayeredGraph<View,Val,Degree,StateIdx>* p =
      new (home) LayeredGraph<View,Val,Degree,StateIdx>(home,x,dfa,dist);
    return p->initialize(home);
  }

  template<class View, class Val, class Degree, class StateIdx>
  forceinline
  LayeredGraph<View,Val,Degree,StateIdx>
  ::LayeredGraph(Space& home, bool share, LayeredGraph<View,Val,Degree,StateIdx>& p)
      : Propagator(home,share,p), 
        n(p.n), 
        mindist(p.mindist),
        layers(home.alloc<Layer>(p.x.lengthmax()+1)),
        max_states(p.max_states), 
        n_states(p.n_states), 
        n_edges(p.n_edges) 
  {
    x.update(home,share,p.x);
    c.update(home,share,p.c);
    dfa.update(home,share,p.dfa);
    distance.update(home, share, p.distance);
    
    // Do not allocate states, postpone to advise!
    layers[n].n_states = p.layers[n].n_states;
    layers[n].states = NULL;
    
    if (n_edges == 0) 
      assert(n == 0);
    else {
      // Allocate memory for edges
      Edge* edges = home.alloc<Edge>(n_edges);
      // Copy layers
      for (int i=n; i--; ) {
        //assert(x.charwidth(i) == p.layers[i].size);
        layers[i].size = p.layers[i].size;
        assert(layers[i].size == p.layers[i].size);
        layers[i].support = home.alloc<Support>(layers[i].size);
        for (ValSize j=layers[i].size; j--; ) {
          layers[i].support[j].val = p.layers[i].support[j].val;
          layers[i].support[j].n_edges = p.layers[i].support[j].n_edges;
          assert(layers[i].support[j].n_edges > 0);
          layers[i].support[j].edges = 
            Gecode::Heap::copy(edges,p.layers[i].support[j].edges,
                       layers[i].support[j].n_edges);
          edges += layers[i].support[j].n_edges;
        }
        layers[i].n_states = p.layers[i].n_states;
        layers[i].states = NULL;
      }
    }
    
    // copy final-layer-to-dfa map
    dfa_map = home.alloc<int>(max_states);
    for (int j=layers[n].n_states; j--; )
      dfa_map[j] = p.dfa_map[j];
    
    audit();
  }

  template<class View, class Val, class Degree, class StateIdx>
  PropCost
  LayeredGraph<View,Val,Degree,StateIdx>::cost(const Space&,
                                               const ModEventDelta&) const {
    return PropCost::linear(PropCost::HI,n);
  }

  template<class View, class Val, class Degree, class StateIdx>
  Actor*
  LayeredGraph<View,Val,Degree,StateIdx>::copy(Space& home, bool share) {
    /*
      TODO: restore elimination of assigned prefixes
    
    Eliminating an assigned prefix is still a valid idea. 
    BUT it changes the relationship between the layers of the graph and the length var.
    Would need to track the size of all eliminated prefixes, 
    and modify all references to x.lengthmin(), x.lengthmax() accordingly.
    */
    
    // // Eliminate an assigned prefix
    // {
    //   int k=0;
    //   while (layers[k].size == 1) {
    //     assert(layers[k].support[0].n_edges == 1);
    //     n_states -= layers[k].n_states;
    //     k++;
    //   }
    //   if (k > 0) {
    //     /*
    //      * The state information is always available: either the propagator
    //      * has been created (hence, also the state information has been
    //      * created), or the first variable become assigned and hence
    //      * an advisor must have been run (which then has created the state
    //      * information).
    //      */
    //     // Eliminate assigned layers
    //     n -= k; layers += k;
    //     // Eliminate edges
    //     n_edges -= static_cast<unsigned int>(k);
    //     // Update advisor indices
    //     for (Advisors<Index> as(c); as(); ++as)
    //       as.advisor().i -= k;
    //     // Update all change information
    //     a_ch.lshift(k);
    //   }
    // }
    // audit();
    
    // Compress states
    if (!a_ch.empty()) {
      int f = a_ch.fst();
      int l = a_ch.lst();
      assert((f >= 0) && (l <= n));
      Region r(home);
      // State map for in-states
      StateIdx* i_map = r.alloc<StateIdx>(max_states);
      // State map for out-states
      StateIdx* o_map = r.alloc<StateIdx>(max_states);
      // Number of in-states
      StateIdx i_n = 0;
    
      int* new_dfa_map = r.alloc<int>(dfa.n_states());
      
      n_states -= layers[l].n_states;
      // Initialize map for in-states and compress
      for (StateIdx j=0; j<layers[l].n_states; j++)
        if ((layers[l].states[j].i_deg != 0) ||
            (layers[l].states[j].o_deg != 0)) {
          layers[l].states[i_n]=layers[l].states[j];
          if (l == n) new_dfa_map[static_cast<int>(i_n)] = dfa_map[static_cast<int>(j)];
          i_map[j]=i_n++;
        }
      layers[l].n_states = i_n;
      n_states += layers[l].n_states;
       // FIXME: figure out why this can fail here, but not in the fixed length version
      assert(i_n > 0);
      
      // Update map to dfa if last layer changed
      if (l == n)
        for (StateIdx j=layers[l].n_states; j--; )
          dfa_map[j] = new_dfa_map[j];
      
      // Update in-states in edges for last layer, if any
      if (l < n)
        for (ValSize j=layers[l].size; j--; ) {
          Support& s = layers[l].support[j];
          for (Degree d=s.n_edges; d--; )
            s.edges[d].i_state = i_map[s.edges[d].i_state];
        }
    
      // Update all changed layers
      for (int i=l-1; i>=f; i--) {
        // In-states become out-states
        std::swap(o_map,i_map); i_n=0;
        // Initialize map for in-states and compress
        n_states -= layers[i].n_states;
        for (StateIdx j=0; j<layers[i].n_states; j++)
          if ((layers[i].states[j].o_deg != 0) ||
              (layers[i].states[j].i_deg != 0)) {
            layers[i].states[i_n]=layers[i].states[j];
            i_map[j]=i_n++;
          }
        layers[i].n_states = i_n;
        n_states += layers[i].n_states;
        assert(i_n > 0);
    
        // Update states in edges
        for (ValSize j=layers[i].size; j--; ) {
          Support& s = layers[i].support[j];
          for (Degree d=s.n_edges; d--; ) {
            s.edges[d].i_state = i_map[s.edges[d].i_state];
            s.edges[d].o_state = o_map[s.edges[d].o_state];
          }
        }
      }
    
      // Update out-states in edges for previous layer, if any
      if (f > 0)
        for (ValSize j=layers[f-1].size; j--; ) {
          Support& s = layers[f-1].support[j];
          for (Degree d=s.n_edges; d--; )
            s.edges[d].o_state = i_map[s.edges[d].o_state];
        }
    
      a_ch.reset();
    }
    audit();

    return new (home) LayeredGraph<View,Val,Degree,StateIdx>(home,share,*this);
  }

  /// Select small types for the layered graph propagator
  template<class View>
  forceinline ExecStatus
  post_lgp(Home home, View x, const DFA& dfa) {
    Gecode::Support::IntType t_state_idx =
      Gecode::Support::u_type(static_cast<unsigned int>(dfa.n_states()));
    Gecode::Support::IntType t_degree =
      Gecode::Support::u_type(dfa.max_degree());
    Gecode::Support::IntType t_val = 
      std::max(Support::s_type(dfa.symbol_min()),
               Support::s_type(dfa.symbol_max()));
    switch (t_val) {
    case Gecode::Support::IT_CHAR:
    case Gecode::Support::IT_SHRT:
      switch (t_state_idx) {
      case Gecode::Support::IT_CHAR:
        switch (t_degree) {
        case Gecode::Support::IT_CHAR:
          return Extensional::LayeredGraph
            <View,short int,unsigned char,unsigned char>
            ::post(home,x,dfa);
        case Gecode::Support::IT_SHRT:
          return Extensional::LayeredGraph
            <View,short int,unsigned short int,unsigned char>
            ::post(home,x,dfa);
        case Gecode::Support::IT_INT:
          return Extensional::LayeredGraph
            <View,short int,unsigned int,unsigned char>
            ::post(home,x,dfa);
        default: GECODE_NEVER;
        }
        break;
      case Gecode::Support::IT_SHRT:
        switch (t_degree) {
        case Gecode::Support::IT_CHAR:
          return Extensional::LayeredGraph
            <View,short int,unsigned char,unsigned short int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_SHRT:
          return Extensional::LayeredGraph
            <View,short int,unsigned short int,unsigned short int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_INT:
          return Extensional::LayeredGraph
            <View,short int,unsigned int,unsigned short int>
            ::post(home,x,dfa);
        default: GECODE_NEVER;
        }
        break;
      case Gecode::Support::IT_INT:
        switch (t_degree) {
        case Gecode::Support::IT_CHAR:
          return Extensional::LayeredGraph
            <View,short int,unsigned char,unsigned int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_SHRT:
          return Extensional::LayeredGraph
            <View,short int,unsigned short int,unsigned int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_INT:
          return Extensional::LayeredGraph
            <View,short int,unsigned int,unsigned int>
            ::post(home,x,dfa);
        default: GECODE_NEVER;
        }
        break;
      default: GECODE_NEVER;
      }

    case Gecode::Support::IT_INT:
      switch (t_state_idx) {
      case Gecode::Support::IT_CHAR:
        switch (t_degree) {
        case Gecode::Support::IT_CHAR:
          return Extensional::LayeredGraph
            <View,int,unsigned char,unsigned char>
            ::post(home,x,dfa);
        case Gecode::Support::IT_SHRT:
          return Extensional::LayeredGraph
            <View,int,unsigned short int,unsigned char>
            ::post(home,x,dfa);
        case Gecode::Support::IT_INT:
          return Extensional::LayeredGraph
            <View,int,unsigned int,unsigned char>
            ::post(home,x,dfa);
        default: GECODE_NEVER;
        }
        break;
      case Gecode::Support::IT_SHRT:
        switch (t_degree) {
        case Gecode::Support::IT_CHAR:
          return Extensional::LayeredGraph
            <View,int,unsigned char,unsigned short int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_SHRT:
          return Extensional::LayeredGraph
            <View,int,unsigned short int,unsigned short int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_INT:
          return Extensional::LayeredGraph
            <View,int,unsigned int,unsigned short int>
            ::post(home,x,dfa);
        default: GECODE_NEVER;
        }
        break;
      case Gecode::Support::IT_INT:
        switch (t_degree) {
        case Gecode::Support::IT_CHAR:
          return Extensional::LayeredGraph
            <View,int,unsigned char,unsigned int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_SHRT:
          return Extensional::LayeredGraph
            <View,int,unsigned short int,unsigned int>
            ::post(home,x,dfa);
        case Gecode::Support::IT_INT:
          return Extensional::LayeredGraph
            <View,int,unsigned int,unsigned int>
            ::post(home,x,dfa);
        default: GECODE_NEVER;
        }
        break;
      default: GECODE_NEVER;
      }

    default: GECODE_NEVER;
    }
    return ES_OK;
  }
}}}