/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 08:20:30 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14968 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <gecode/string/rel.hh>

namespace Gecode {
  
  void 
  equal(Home home, StringVar _x, StringVar _y) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::StringView y(_y);
    if (!same(x,y))
      GECODE_ES_FAIL((String::Rel::Equal< String::StringView, String::StringView >::post(home, x, y)));
  }
  
  void
  equal(Home home, StringVar _x, IntArgs _y) {
    if(home.failed()) return;
    String::StringView x(_x);
    GECODE_ME_FAIL(x.llq(home,_y.size()));
    GECODE_ME_FAIL(x.lgq(home,_y.size()));
    for (int i = 0; i < _y.size(); i++) {
      GECODE_ME_FAIL(x.ceq(home,i,_y[i]));
    }
  }
  
  void
  equal(Home home, IntArgs _x, StringVar _y) {
    if(home.failed()) return;
    String::StringView y(_y);
    GECODE_ME_FAIL(y.llq(home,_x.size()));
    GECODE_ME_FAIL(y.lgq(home,_x.size()));
    for (int i = 0; i < _x.size(); i++) {
      GECODE_ME_FAIL(y.ceq(home,i,_x[i]));
    }
  }
  
  void
  equal(Home home, StringVar _x, StringVar _y, BoolVar _b) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::StringView y(_y);
    Int::BoolView b(_b);
    if (same(x,y)) {
      GECODE_ME_FAIL(b.zero(home));
    }
    GECODE_ES_FAIL((String::Rel::ReEqual< String::StringView, String::StringView, Int::BoolView >::post(home,x,y,b)));
  }
  
  void
  equal(Home home, StringVar _x, const IntArgs& _y, BoolVar _b) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::ConstStringView y(home,_y);
    Int::BoolView b(_b);
    GECODE_ES_FAIL((String::Rel::ReEqual< String::StringView, String::ConstStringView, Int::BoolView >::post(home,x,y,b)));
  }
  
  void
  equal(Home home, const IntArgs& _x, StringVar _y, BoolVar _b) {
    if(home.failed()) return;
    String::StringView y(_y);
    String::ConstStringView x(home,_x);
    Int::BoolView b(_b);
    GECODE_ES_FAIL((String::Rel::ReEqual< String::ConstStringView, String::StringView, Int::BoolView >::post(home,x,y,b)));
  }
  
  void 
  nequal(Home home, StringVar _x, StringVar _y) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::StringView y(_y);
    if (same(x,y)) {
      home.fail();
    }
    GECODE_ES_FAIL((String::Rel::Nequal< String::StringView, String::StringView >::post(home, x, y)));
  }
  
  void
  nequal(Home home, StringVar _x, IntArgs _y) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::ConstStringView y(home,_y);
    GECODE_ES_FAIL((String::Rel::Nequal< String::StringView, String::ConstStringView >::post(home, x, y)));
  }
  
  void
  nequal(Home home, IntArgs _x, StringVar _y) {
    if(home.failed()) return;
    String::StringView y(_y);
    String::ConstStringView x(home,_x);
    GECODE_ES_FAIL((String::Rel::Nequal< String::ConstStringView, String::StringView >::post(home, x, y)));
  }
}