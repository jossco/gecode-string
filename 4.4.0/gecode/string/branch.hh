/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-05 10:24:04 +0200 (Tue, 05 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14947 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __GECODE_STRING_BRANCH_HH__
#define __GECODE_STRING_BRANCH_HH__

#include <gecode/string.hh>

/**
 * \namespace Gecode::String::Branch
 * \brief String branchers
 */

namespace Gecode { namespace String { namespace Branch {
  
  /*
  * Character/Length branching that reasons on mandatory characters only
  *
  * If there is a mandatory character that isn't fixed, 
  *   then try a value for it,
  *   else branch on the length.
  */

  class Mandatory : public Brancher {
  protected:
    ViewArray<StringView> x;
    mutable int start_var;
    VarSelect v_sel;
    IndexSelect i_sel;
    CharValueSelect cv_sel;
    LengthSelect l_sel;
    // choice definition
    class PosIndVal : public Choice {
    public:
      int pos; int ind; BitSet val; int len;
      // constructor for index/value
      PosIndVal(const Mandatory& b, int p, int i, BitSet v);
      //constructor for length choice
      PosIndVal(const Mandatory& b, int p, int l);
      virtual size_t size(void) const {
        return sizeof(*this);
      }
      virtual void archive(Archive& e) const;
    };
    int select_var(void) const;
    int select_index(int y) const;
    BitSet select_char(BitSet bs) const;
    int select_length(int y) const;
  public:
    Mandatory(Home home, 
              ViewArray<StringView>& x0, 
              VarSelect vs, 
              IndexSelect is, 
              CharValueSelect cvs, 
              LengthSelect ls);
  
    static void post(Home home, 
                     ViewArray<StringView>& x, 
                     VarSelect vs, 
                     IndexSelect is, 
                     CharValueSelect cvs, 
                     LengthSelect ls);
    
    virtual size_t dispose(Space& home);
    
    Mandatory(Space& home, bool share, Mandatory& b);
    
    virtual Brancher* copy(Space& home, bool share);
  
    // status
    virtual bool status(const Space& home) const;
  
    // choice
    virtual Choice* choice(Space& home);
  
    virtual Choice* choice(const Space&, Archive& e);
  
    // commit
    virtual ExecStatus commit(Space& home, 
                              const Choice& c,
                              unsigned int a);
  
    // print
    virtual void print(const Space& home, 
                       const Choice& c,
                       unsigned int a,
                       std::ostream& o) const;
  };
}}}

#include <gecode/string/branch/mandatory.hpp>

#endif