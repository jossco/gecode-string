/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-22 03:57:09 +0200 (Fri, 22 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14979 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <cstddef>
#include <algorithm>

#ifdef _MSC_VER

#include <intrin.h>

#if defined(_M_IX86)
#pragma intrinsic(_BitScanForward)
#define GECODE_SUPPORT_MSVC_32
#endif

#if defined(_M_X64) || defined(_M_IA64)
#pragma intrinsic(_BitScanForward64)
#define GECODE_SUPPORT_MSVC_64
#endif

#endif



namespace Gecode { namespace String {

  class StringVarImp;
  
  class StringDelta : public Delta {
  private:
    int lmin, lmax, idx;
  public:
    // create delta providing no information
    StringDelta();
    // create delta for removal of interval [min,max] from length
    StringDelta(int min, int max);
    // create delta for modification of ch domain at index
    StringDelta(int i);
  
    void index_changed(int i);
  
    void length_interval_removed(int min, int max);
  
    bool any(void) const;
    bool length(void) const;
    bool symbol(void) const;
    int length_min(void) const;
    int length_max(void) const;
    int index(void) const;
    
  };
}}

#include <gecode/string/var-imp/delta.hpp>
namespace Gecode { namespace String {
    
  class BitSetArray{
  protected:
    size_t sz;
    BitSet *bitset;
    
  public:
    BitSetArray(): sz(0), bitset(NULL) {}
    
    bool initialized(void) const {return (sz != 0) && (bitset != NULL);}
    
    template<class A>
    void initialize(A& a, size_t s, BitSet mask=Full);
    
    template<class A>
    void initialize(A& a, BitSetArray *other, size_t s=0);
    
    template<class A>
    BitSetArray(A& a, size_t s, BitSet mask=Full);
    
    template<class A>
    void realloc(A& a, size_t s, BitSet mask=Full);
    
    // copy constructor: must always specify number of cells to copy
    template<class A>
    BitSetArray(A& a, BitSetArray *other, size_t s);
    
    template<class A>
    size_t
      dispose(A& a);
    
    BitSet& operator[](size_t i);
    
    BitSet operator[](size_t i) const;
    
    size_t length(void) const {return sz;}
    
    bool in(size_t ind, Value val) const;
    
    bool singleton(size_t ind) const;
    
    void remove(size_t ind, Value val);
    
    void removeall(size_t ind);
    
    size_t width(size_t ind) const;
    
    Value next_value(size_t ind, Value cur) const;
    
    Value prev_value(size_t ind, Value cur) const;
    
    void inter(size_t ind, BitSet bs);
  
    void minus(size_t ind, BitSet bs);
  
    void narrow(size_t ind, BitSet bs);
    
    void fix(size_t ind, Value val);
  };
  
#ifdef GECODE_STRING_BS_ARRAY
  /*
   * Fixed size array of BitSets
   * (just a wrapper for BitSetArray)
   *
   */
  class BitSetListBase {
  protected:
    size_t size;
    BitSetArray *list;
    
  public:
    //BitSetListBase() : sz(0), bits(NULL) {}
    
    template<class A>
    BitSetListBase(A& a, size_t minsize, size_t maxsize, size_t bsize, BitSet mask=Full);
        
    template<class A>
    size_t dispose(A& a);
    
    // copy constructor: copy BitSets from \a other BitSetArrayList
    // optional parameter \a s says only allocate s BitSets and copy the first s from other
    template<class A>
    BitSetListBase(A& a, const BitSetListBase& other, size_t maxsize);
    
    // resize \a list so that it holds at most
    // enough nodes to accomodate \a s bitsets.
    template <class A>
    void shrink(A& a, size_t maxsize);
    
    // resize \a list so that it holds at least
    // \a s bitsets.
    template <class A>
    void grow(A& a, size_t minsize, size_t maxsize, BitSet mask=Full);
    
    ~BitSetListBase() {}
    
     int length(void) const;
    
    // access a bitset
    BitSet& operator[](size_t i);
    BitSet operator[](size_t i);
    BitSet get(size_t i) const;
    
    bool in(size_t ind, Value val) const;
    
    bool singleton(size_t ind) const;
    
    void remove(size_t ind, Value val);
    
    void removeall(size_t ind);
    
    size_t width(size_t ind) const;
    
    Value next(size_t ind, Value cur) const;
    
    // Value prev(size_t ind, Value cur) const;
    
    void inter(size_t ind, BitSet bs);
  
    void minus(size_t ind, BitSet bs);
  
    void narrow(size_t ind, BitSet bs);
    
    void fix(size_t ind, Value val);
    
    Value val(size_t ind) const;
  };
  
#elif defined GECODE_STRING_BS_LIST
  class BitSetListBase {
  protected:
    class Node : public BitSetArray {
    public:
      Node *next;
      Node(): BitSetArray(), next(NULL) {}
    };
    
    // logical size of the list
    // (there may be more bitsets actually allocated)
    size_t size;
    // size of a block
    size_t blocksize;
    // the blocks
    Node *list;
    
    size_t nodesfor(size_t sz) const;
  public:
    template<class A>
    BitSetListBase(A& a, size_t minsize, size_t maxsize, size_t bsize, BitSet mask=Full);;
        
    template<class A>
    size_t dispose(A& a);
    
    // copy constructor: copy BitSets from \a other BitSetArrayList
    // Allocates (at most) s BitSets and copy the first s from other
    // Will never allocate more than other.sz, though.
    template<class A>
    BitSetListBase(A& a, const BitSetListBase& other, size_t maxsize);
    
      // resize \a list so that it holds at most
      // enough nodes to accomodate \a s bitsets.
      template <class A>
      void shrink(A& a, size_t maxsize);
      
      // resize \a list so that it holds at least
      // enough nodes to accomodate \a s bitsets.
      template <class A>
      void grow(A& a, size_t minsize, size_t maxsize, BitSet mask=Full);
      
    ~BitSetListBase() {};
    
    int length(void) const;
    
    Node* nodefor(int index) const;
    
    int indexof(int index) const;
    // access a bitset
    BitSet& operator[](size_t i);
    BitSet operator[](size_t i) const;
    
    BitSet get(size_t i) const;
    
    bool in(size_t ind, Value val) const;
    
    bool singleton(size_t ind) const;
    
    void remove(size_t ind, Value val);
    
    void removeall(size_t ind);
    
    size_t width(size_t ind) const;
    
    Value next(size_t ind, Value cur) const;
    
    // Value prev(size_t ind, Value cur) const {
//       return bits.prev(ind, cur);
//     }
    
    void inter(size_t ind, BitSet bs);
  
    void minus(size_t ind, BitSet bs);
  
    void narrow(size_t ind, BitSet bs);
    
    void fix(size_t ind, Value val);
    
    Value val(size_t ind) const;
  };
#elif defined GECODE_STRING_BS_ARRAYLIST
  class BitSetListBase {
  protected:
    // logical size of the list
    size_t size;
    // size of a block
    size_t blocksize;
    // number of nodes (set at construction)
    size_t nodecount;
    // the blocks
    BitSetArray *list;
    
    size_t nodesfor(size_t sz) const;
  public:
    template<class A>
    BitSetListBase(A& a, size_t minsize, size_t maxsize, size_t bsize, BitSet mask=Full);
        
    template<class A>
    size_t dispose(A& a);
    
    // copy constructor: copy BitSets from \a other BitSetArrayList
    // Allocates (at most) s BitSets and copy the first s from other
    // Will never allocate more than other.size, though.
    template<class A>
    BitSetListBase(A& a, const BitSetListBase& other, size_t maxsize);
    
    // resize \a list so that it holds at most
    // enough nodes to accomodate \a s bitsets.
    template <class A>
    void shrink(A& a, size_t maxsize);
    
    // resize \a list so that it holds at least
    // enough nodes to accomodate \a s bitsets.
    template <class A>
    void grow(A& a, size_t minsize, size_t maxsize, BitSet mask=Full);
      
    ~BitSetListBase() {};
    
    int length(void) const;
    
    int nodefor(int index) const;
    
    int indexof(int index) const;
    // access a bitset
    BitSet& operator[](size_t i);
    
    BitSet operator[](size_t i) const;
    
    BitSet get(size_t i) const;
    
    bool in(size_t ind, Value val) const;
    
    bool singleton(size_t ind) const;
    
    void remove(size_t ind, Value val);
    
    void removeall(size_t ind);
    
    size_t width(size_t ind) const;
    
    Value next(size_t ind, Value cur) const;
    
    // Value prev(size_t ind, Value cur) const;
    
    void inter(size_t ind, BitSet bs);
  
    void minus(size_t ind, BitSet bs);
  
    void narrow(size_t ind, BitSet bs);
    
    void fix(size_t ind, Value val);
    
    Value val(size_t ind) const;
  };
#endif
  
  template <class A>
  class BitSetList : public BitSetListBase {
  protected:
    /// Allocator
    A& a;
  public:
    /// uninitialized list
    //BitSetList() : BitSetListBase() {}
    /// List with space for \a s bitsets
    BitSetList(A& a, size_t minsize, size_t maxsize, size_t bsize, BitSet mask=Full);
    /// Copy list \a other
    /// If size \a maxsize is 0 (the default),
    ///   then copy all bitsets,
    ///   else copy the first \a s bitsets
    BitSetList(A& a, const BitSetList& other,size_t maxsize=0);
    /// Destructor
    ~BitSetList(void);
    
    void grow(size_t minsize, size_t maxsize, BitSet mask=Full);
    void shrink(size_t maxsize);
  };
  
}}

#include <gecode/string/var-imp/bitset-list.hpp>

namespace Gecode { namespace String {
  class BitSetIter {
  
  protected:
    const BitSet bs;
    Value cur;
    Value limit;
    Value next(Value v);
    void move(void);
  
  public:
    BitSetIter(BitSet b);
    BitSetIter(BitSet b, Value n, Value m=Numbits);
    bool operator()(void) const;
    void operator++(void);
    Value val(void);
  };
  
  class BitSetRangeIter {
  
  protected:
    const BitSet bs;
    int _min, _max;
    int _lim;
    void next(void);
  
  public:
    BitSetRangeIter(BitSet b);
    BitSetRangeIter(BitSet b, int n, int m=Numbits);
    bool operator()(void) const;
    void operator++(void);
    int min(void) const;
    int max(void) const;
    int width(void) const;
  };
}}

namespace Gecode { namespace String {
  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, 
              const String::BitSetIter& _iter);

  template<class Char, class Traits>
  std::basic_ostream<Char,Traits>&
    operator <<(std::basic_ostream<Char,Traits>& os, 
                const String::BitSetRangeIter& _iter);
}}

#include <gecode/string/var-imp/bitset-iter.hpp>

namespace Gecode { namespace String {
  class StringVarImp: public StringVarImpBase {
  protected:
#ifdef GECODE_STRING_BS_HEAP
    typedef BitSetList<Gecode::Heap> BSList;
#else
    typedef BitSetList<Space> BSList;
#endif
    int lmin;
    int lmax;
    BitSet alphabet;
    unsigned int wid;
    BSList character;
    void exclude_index(int i, StringDelta& sd);
    void include_index(int i, StringDelta& sd);
    
  public:
    
    StringVarImp(Space& home, 
                 int mi, 
                 int ma, 
                 int w=Numbits, 
                 BitSet mask=Full, 
                 int blocksize=16);
    // copying
    StringVarImp* copy(Space& home, bool share);
  
    StringVarImp(Space& home, 
                 bool share, 
                 StringVarImp& y);
    
    size_t dispose(Space& home);
                       
    bool mandatory(int l) const;
    bool forbidden(int l) const;
    
    // domain tests
    bool assigned(void) const;
    
    // size of the bitset list (may be longer than lmax)
    unsigned int size(void) const;
    // test whether \a n is in the domain of the character at \a i
    bool in(int i, int v) const;
    
    // size of domain for character \a i
    int charwidth(int i) const;
    
    // test whether character \a i is assigned
    bool charfixed(int i) const;
    
    // get first domain value for character \a i
    unsigned int charmin(int i) const;
    
    // get last domain value for character \a i
    unsigned int charmax(int i) const;
    
    unsigned int next(int i, int v) const;
    
    unsigned int charval(int i) const;
    
    // test whether length is fixed to a value
    bool lengthfixed(void) const;
    
    //
    int lengthmin(void) const;
    
    //
    int lengthmax(void) const;
    
    
    int lengthmed(void) const;
    
    //
    int lengthval(void) const;
    
    unsigned int width(void) const;
    
    BitSet domain(int i);
    // length modification operations
    
    //
    ModEvent lle(Space& home, int n);
    //
    ModEvent llq(Space& home, int n);
    //
    ModEvent lgr(Space& home, int n);
    //
    ModEvent lgq(Space& home, int n);
      
    // character modification operations
      
    // assign value \a n to character \a i 
    // semantics: if dom[i] becomes assigned, then length >= i
    ModEvent ceq(Space& home, int i, int n);
    // remove value \a n from domain of character \a i 
    // semantics: if dom[i] becomes empty, then length <= i
    ModEvent cnq(Space& home, int i, int n);
    /**
    * \name Domain update by bitset
    *
    * Character domains can be updated by value bitsets.
    *
    * The argument \a depends must be true, if the iterator
    * passed as argument depends on the variable implementation
    * on which the operation is invoked. In this case, the variable
    * implementation is only updated after the iterator has been
    * consumed. Otherwise, the domain might be updated concurrently
    * while following the iterator.
    *
    */
    /// Replace domain by values described by \a b
    //ModEvent narrow_b(Space& home, unsigned int index, const BitSetDomain& bsd);
    
    //template<class I>
    //ModEvent narrow_v(Space& home, unsigned int index, I& i, bool depends=true);
    
    /// Intersect domain with values described by \a b
    ModEvent inter(Space& home, int i, BitSet b);
    
    template<class I>
    ModEvent inter_v(Space& home, int index, I& i, bool depends=true);
    
    /// Remove from domain the values described by \a b
    ModEvent minus(Space& home, int i, BitSet b);
    
    //template<class I>
    //ModEvent minus_v(Space& home, unsigned int index, I& i, bool depends=true);
      
    // delta information
    static int length_min(const Delta& d);
    static int length_max(const Delta& d);
    static int index(const Delta& d);
    static bool any(const Delta& d);
    static bool length(const Delta& d);
    static bool symbol(const Delta& d);
      
    // subscriptions
    void subscribe(Space& home, 
                   Propagator& p, 
                   PropCond pc, 
                   bool schedule=true);
    void subscribe(Space& home, 
                   Advisor& a);
    void cancel(Space& home, 
                Propagator& p, 
                PropCond pc);
    void cancel(Space& home, 
                Advisor& a);
  };
}}
#include <gecode/string/var-imp/string.hpp>