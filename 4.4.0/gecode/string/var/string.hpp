/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-05 10:24:04 +0200 (Tue, 05 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14947 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode {
  
  // access operations
  
  forceinline bool 
  StringVar::assigned(void) const {
    return x->assigned();
  }
  
  forceinline int 
  StringVar::size(void) const {
    return x->size();
  }
  
  forceinline bool 
  StringVar::in(int i, int n) const {
    return x->in(i,n);
  }
  
  forceinline int 
  StringVar::charwidth(int i) const {
    return x->charwidth(i);
  }
  
  forceinline bool 
  StringVar::charfixed(int i) const {
    return x->charfixed(i);
  }
  
  forceinline int 
  StringVar::charval(int i) const {
    return x->charval(i);
  }
  
  forceinline int 
  StringVar::charmin(int i) const {
    return x->charmin(i);
  }
  
  forceinline int 
  StringVar::charmax(int i) const {
    return x->charmax(i);
  }
  
  forceinline bool 
  StringVar::lengthfixed(void) const {
    return x->lengthfixed();
  }
  
  forceinline int 
  StringVar::lengthmin(void) const {
    return x->lengthmin();
  }
  
  forceinline int 
  StringVar::lengthmax(void) const {
    return x->lengthmax();
  }
  
  forceinline int 
  StringVar::lengthmed(void) const {
    return x->lengthmed();
  }
  
  forceinline int 
  StringVar::lengthval(void) const {
    return x->lengthval();
  }
  
  forceinline int 
  StringVar::width(void) const {
    return x->width();
  }
  
  forceinline bool 
  StringVar::mandatory(int l) const {
    return x->mandatory(l);
  }
  
  forceinline bool 
  StringVar::forbidden(int l) const {
    return x->forbidden(l);
  }
  
  forceinline int 
  StringVar::next(int i, int v) const {
    return x->next(i,v);
  }
  
  forceinline String::BitSet 
  StringVar::domain(int i) const {
    return x->domain(i);
  }
}
