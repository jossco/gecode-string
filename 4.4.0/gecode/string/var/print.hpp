/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-05 10:24:04 +0200 (Tue, 05 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14947 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode {

  template<class Char, class Traits>
  inline std::basic_ostream<Char,Traits>&
  operator <<(std::basic_ostream<Char,Traits>& os, const StringVar& x) {
    std::basic_ostringstream<Char,Traits> s;
    s.copyfmt(os); s.width(0);

    if (x.assigned()) {
      s << "[";
      for(int i = 0; i < x.lengthval(); i++)
        s << x.charval(i) << ((i<x.lengthval()-1)?", ":"");
      s << "]";
    }
    else {
      if (x.lengthfixed())
        s << x.lengthval();
      else 
        s << "{" << x.lengthmin() << ":" << x.lengthmax() << "}";
      s << ":[";
      for(int i = 0; i < x.size() ; i++) {
        if (x.mandatory(i))
          s << "+";
        else {
          if (x.forbidden(i))
            s << "-";
          else
            s << "?";
        }
        if (x.charfixed(i))
          s << x.charval(i);
        else {
          s << "{";
          String::BitSetIter iter(x.domain(i));
          unsigned int minrange = iter.val();
          while (iter()) {
            unsigned int cur = iter.val();
            ++iter;
            if (!(iter()) || (iter.val() > cur + 1)) {
              s << minrange;
              if (minrange < cur)
                s << ":" << cur;
              minrange=iter.val();
              if (iter())
                s << ",";
            }
          }
          s << "}" ;
        }
        if (i < x.size() - 1)
          s << ", ";
      }
      s << "]";
    }
    return os << s.str();
  }
}