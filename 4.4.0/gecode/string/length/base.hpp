/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Length {

  template<class SV, class IV>
  Base<SV,IV>::Base(Home home, SV _x, IV _l)
  : Propagator(home), x(_x), l(_l), c(home) {
    l.subscribe(home, *this, Int::PC_INT_BND);
    x.subscribe(home, *new (home) Advisor(home,*this,c));
  }
  
  template<class SV, class IV>
  ExecStatus
  Base<SV,IV>::advise(Space&, Advisor&, const Delta& d) {
    if (x.length(d)) {
      if ((x.length_min(d) > l.max()) || (x.length_max(d) < l.min()))
        return ES_FIX;
      else
        return ES_NOFIX;
    }
    return ES_FIX;
  }
  
  template<class SV, class IV>
  Base<SV,IV>::Base(Home home, bool share, Base& p)
  : Propagator(home, share, p) {
    x.update(home, share, p.x);
    l.update(home, share, p.l);
    c.update(home, share, p.c);
  }
  
  template<class SV, class IV>
  size_t 
  Base<SV,IV>::dispose(Space& home) {
    l.cancel(home, *this, Int::PC_INT_BND);
    x.cancel(home,Advisors<Advisor>(c).advisor());
    c.dispose(home);
    (void) Propagator::dispose(home);
    return sizeof(*this);
  }
  
  template<class SV, class IV>
  ExecStatus 
  Base<SV,IV>::post(Space& home, SV x, IV l) {
    (void) new (home) Base(home,x,l);
    return ES_OK;
  }
  
  template<class SV, class IV>
  ExecStatus 
  Base<SV,IV>::propagate(Space& home, const Gecode::ModEventDelta&) {
    // dom(l) might have holes, so check it first
    Gecode::Iter::Ranges::Singleton x_iter(x.lengthmin(), x.lengthmax());
    GECODE_ME_CHECK(l.inter_r(home,x_iter,false));
    //GECODE_ME_CHECK(l.lq(home,x.lengthmax()));
    //GECODE_ME_CHECK(l.gq(home,x.lengthmin()));
    // len(x) is always an interval
    GECODE_ME_CHECK(x.llq(home,l.max()));
    GECODE_ME_CHECK(x.lgq(home,l.min()));
    
    if (x.lengthfixed()) {
       assert(l.assigned());
       return home.ES_SUBSUMED(*this);
     }
    else
      return ES_FIX;
  }

}}}