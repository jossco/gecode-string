/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-22 03:57:09 +0200 (Fri, 22 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14979 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <gecode/string/channel.hh>

namespace Gecode {
	
  void
  channel(Home home, StringVar _x, int index, IntVar _i) {
    if (home.failed()) return;
    if ((index < 0) || (index >= _x.lengthmax()))
      throw String::IndexOutOfLimits("String::channel");
    String::StringView x(_x);
    Int::IntView i(_i);
    GECODE_ES_FAIL((String::Channel::Base< String::StringView, Int::IntView >::post(home,x,index,i)));
  }
  
  
	void
  channel(Home home, StringVar _x, int index, int i) {
    if (home.failed()) return;
    if ((index < 0) || (index >= _x.lengthmax()))
      throw String::IndexOutOfLimits("String::channel");
    String::StringView x(_x);
    GECODE_ME_FAIL(x.ceq(home,index,i));
    GECODE_ME_FAIL(x.lgq(home,index));
  }
  
  void
  channel(Home home, StringVar _x, int index, IntVar _i, BoolVar _b) {
    if (home.failed()) return;
    if ((index < 0))
      throw String::IndexOutOfLimits("String::channel");
    String::StringView x(_x);
    Int::IntView i(_i);
    Int::BoolView b(_b);
    GECODE_ES_FAIL((String::Channel::Reif< String::StringView, Int::IntView, Int::BoolView >::post(home,x,index,i,b)));
  }
  
  
	void
  channel(Home home, StringVar _x, int index, int _i, BoolVar _b) {
    if (home.failed()) return;
    if ((index < 0))
      throw String::IndexOutOfLimits("String::channel");
    Int::BoolView b(_b);
    if (index > _x.lengthmax()) {
      GECODE_ME_FAIL(b.zero(home));
    } else {
      String::StringView x(_x);
      Int::ConstIntView i(_i);
      if (index <= _x.lengthmin()) {
        GECODE_ME_FAIL(b.one(home));
        GECODE_ES_FAIL((String::Channel::Base< String::StringView, Int::ConstIntView >::post(home,x,index,i)));
      } else {
        GECODE_ES_FAIL((String::Channel::Reif< String::StringView, Int::ConstIntView, Int::BoolView >::post(home,x,index,i,b)));
      }
    }
  }
}