/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __GECODE_STRING_SUBSTRING_HH__
#define __GECODE_STRING_SUBSTRING_HH__

#include <gecode/string.hh>

namespace Gecode { namespace String { namespace Substring {
  template<class SView1, class SView2, class IView>
  class Base : public Propagator {
  protected:
    SView1 x;
    SView2 y; 
    IView i;
    Council<Advisor> c;
    
  public:
    Base(Home home, SView1 x, SView2 y, IView i);
    Base(Home home, bool share, Base& p);
    virtual size_t dispose(Space& home);
    static ExecStatus post(Space& home, SView1 x, SView2 y, IView i);
    virtual ExecStatus propagate(Space& home, const ModEventDelta& med);
    virtual ExecStatus advise(Space&, Advisor&, const Delta& d);
    virtual Propagator* copy(Space& home, bool share){
      return new (home) Base(home,share, *this); 
    }
    virtual PropCost cost(const Space&, const ModEventDelta&) const{
      return PropCost::quadratic(PropCost::LO,i.size());
    }
  };
  
}}}


#include <gecode/string/substring/base.hpp>
#endif
