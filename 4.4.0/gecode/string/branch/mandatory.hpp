/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-15 14:03:14 +0200 (Fri, 15 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14961 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace Gecode { namespace String { namespace Branch {
  
  Mandatory::Mandatory(Home home, 
                       ViewArray<StringView>& x0, 
                       VarSelect vs, 
                       IndexSelect is, 
                       CharValueSelect cvs, 
                       LengthSelect ls)
    : Brancher(home), 
      x(x0), 
      start_var(0), 
      v_sel(vs), 
      i_sel(is), 
      cv_sel(cvs), 
      l_sel(ls) {}
  
  void
  Mandatory::post(Home home, 
                  ViewArray<StringView>& x, 
                  VarSelect vs, 
                  IndexSelect is, 
                  CharValueSelect cvs, 
                  LengthSelect ls) {
    (void) new (home) Mandatory(home,x, vs, is, cvs, ls);
  }
  
  size_t 
  Mandatory::dispose(Space& home) {
    (void) Brancher::dispose(home);
    return sizeof(*this);
  }
  
  Mandatory::Mandatory(Space& home, bool share, Mandatory& b)
    : Brancher(home,share,b), 
      start_var(b.start_var),
      v_sel(b.v_sel), 
      i_sel(b.i_sel), 
      cv_sel(b.cv_sel), 
      l_sel(b.l_sel) {
    x.update(home,share,b.x);
  }
  
  Brancher* 
  Mandatory::copy(Space& home, bool share) {
    return new (home) Mandatory(home,share,*this);
  }
  /*
  * choice for value-length branching
  */
  forceinline
  Mandatory::PosIndVal::PosIndVal(const Mandatory& b, int p, int i, BitSet v)
    : Choice(b,2), pos(p), ind(i), val(v){}

  forceinline
  Mandatory::PosIndVal::PosIndVal(const Mandatory& b, int p, int l)
    : Choice(b,2), pos(p), ind(l), val(Empty) {}

  void 
  Mandatory::PosIndVal::archive(Archive& e) const {
    Choice::archive(e);
    BitSet mask = (Full) << sizeof(unsigned int);
    unsigned long int top = (mask & val) >> sizeof(unsigned int);
    unsigned long int bot = ~mask & val;
    e << pos << ind << static_cast<unsigned int>(top) << static_cast<unsigned int>(bot);
  }

  /*
  * value-length branching
  */
  forceinline int 
  Mandatory::select_length(int y) const {
    switch(l_sel) {
    case LEN_MIN:
      return x[y].lengthmin();
    case LEN_MAX:
      return x[y].lengthmax();
    case LEN_MED:
      return x[y].lengthmed();
    }
    GECODE_NEVER;
    return -1;
  }

  forceinline int
  Mandatory::select_index(int y) const {
    int p=0;
    unsigned int w;
    switch(i_sel) {
    case IND_NONE:
      for (int i=0;i<x[y].lengthmin();i++) {
        if (!x[y].charfixed(i))
          return i;
      }
      break;
    case IND_WIDTH_MIN:
      w = Numbits + 1;
      for (int i=0;i<x[y].lengthmin();i++) {
        if ((!x[y].charfixed(i)) && (static_cast<unsigned int>(x[y].charwidth(i)) < w)) {
          p = i; w = x[y].charwidth(i);
        }
      }
      if (w < Numbits + 1)
        return p;
      break;
    case IND_WIDTH_MAX:
      w = 0;
      for (int i=0;i<x[y].lengthmin();i++) {
        if ((!x[y].charfixed(i)) && (static_cast<unsigned int>(x[y].charwidth(i)) > w)) {
          p = i; w = x[y].charwidth(i);
        }
      }
      if (w > 0)
        return p;
      break;
    }
    return -1;
  }

  forceinline BitSet
  Mandatory::select_char(BitSet bs) const {
    BitSetIter iter(bs);
    switch(cv_sel) {
    case CHAR_VAL_MIN:
      return singletonfor(iter.val());
      break;
    case CHAR_VAL_MAX:
      {
  int val = iter.val();
  while(iter()) {
    val = iter.val();
    ++iter;
  }
  return singletonfor(val);
  break;
      }
    case CHAR_VAL_SPLIT:
      {
  int w = 0;
  while(iter()) {
    w++; ++iter;
  }
  assert(w > 1);
  w = (w+1) / 2;
  BitSet bottom = Empty;
  BitSetIter i2(bs);
  while (w--) {
    bottom = unionof(bottom,singletonfor(i2.val()));
    ++i2;
  }
  return bottom;
      }
    default:
      GECODE_NEVER;
      return Empty;
    }
    GECODE_NEVER;
    return Empty;
  }

  forceinline int
  Mandatory::select_var() const {
    for (int i = start_var; i<x.size();i++) {
      if (!x[i].assigned()) {
        return i;
      }
    }
    return -1;
  }

  bool 
  Mandatory::status(const Space&) const {
    for (int i = start_var; i<x.size();i++) {
      if (!x[i].assigned()) {
        start_var = i;
        return true;
      }
    }
    return false;
  }

  Choice* 
  Mandatory::choice(Space&) {
    assert(!x[start_var].assigned());
    int c_var = select_var();
    int c_ind = select_index(c_var);
    if (c_ind >= 0) {
      return new PosIndVal(*this,c_var,c_ind,select_char(x[c_var].domain(c_ind)));
    }
    return new PosIndVal(*this,c_var,select_length(c_var));
  }

  Choice* 
  Mandatory::choice(const Space&, Archive& e) {
    int pos, ind;
    unsigned int top, bot;
    e >> pos >> ind >> top >> bot;
    unsigned long int val = static_cast<unsigned long int>(top) << sizeof(unsigned int);
    val &= static_cast<unsigned long int>(bot);
    return new PosIndVal(*this, pos, ind, val);
  }


  ExecStatus 
  Mandatory::commit(Space& home, const Choice& c, unsigned int a) {
    const PosIndVal& pv = static_cast<const PosIndVal&>(c);
    int pos=pv.pos, ind=pv.ind;
    BitSet val=pv.val;
    if (val != Empty) {
      if (a == 0) {
        // By design, only mandatory characters are chosen.
        // So intersection/minus with a singleton BitSet
        // is (here) equivalent to ceq/cnq.
        return me_failed(x[pos].inter(home,ind,val)) ? ES_FAILED : ES_OK;
      }
      else {
        return me_failed(x[pos].minus(home,ind,val)) ? ES_FAILED : ES_OK;
      }
    }
    if (a == 0) {
      return me_failed(x[pos].llq(home,ind)) ? ES_FAILED : ES_OK;
    }
    else {
      return me_failed(x[pos].lgr(home,ind)) ? ES_FAILED : ES_OK;
    }
  }

  // print
  void 
  Mandatory::print(const Space&, const Choice& c,
                  unsigned int a,
                  std::ostream& o) const {
    const PosIndVal& pv = static_cast<const PosIndVal&>(c);
    int pos=pv.pos, ind=pv.ind;
    BitSet val=pv.val;
    if (val != Empty) {
      BitSetRangeIter vi(val);
      if (a == 0)
        o << "x[" << pos << "]_" << ind << " /\\ " << vi;
      else
        o << "x[" << pos << "]_" << ind << " \\= " << vi;
    } else {
      if (a == 0)
        o << "|x[" << pos << "]| <= " << ind;
      else
        o << "|x[" << pos << "]| >  " << ind;
    }
  }

}}}