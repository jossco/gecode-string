/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-05 10:24:04 +0200 (Tue, 05 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14947 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#ifndef __GECODE_STRING_REVERSE_HH__
#define __GECODE_STRING_REVERSE_HH__

#include <gecode/string.hh>

namespace Gecode { namespace String { namespace Reverse {
  template<class V0, class V1>
  class Base : 
    public MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY> {
  protected:
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x0;
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x1;
    
    /// Constructor for cloning \a p
    Base(Home home, bool share, Base<V0,V1>& p);
    
  public:
    /// Constructor for posting
    Base(Home home, V0 x0, V1 x1);
    
    /// Post propagator \f$ x_0 = x_1\f$
    static ExecStatus post(Space& home, V0 X, V1 Y);
    
    /// Perform propagation
    virtual ExecStatus propagate(Space& home, const ModEventDelta& med);

    /// Copy propagator during cloning
    virtual Actor* copy(Space& home, bool share);
    
    /// Cost function
    virtual PropCost cost(const Space& home, const ModEventDelta& med) const;
  };
  
  template<class V0, class V1>
  class Idem : 
    public MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY> {
  protected:
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x0;
    using MixBinaryPropagator<V0,PC_STRING_ANY,V1,PC_STRING_ANY>::x1;
    
    /// Constructor for cloning \a p
    Idem(Home home, bool share, Idem<V0,V1>& p);
    
  public:
    /// Constructor for posting
    Idem(Home home, V0 x0, V1 x1);
    
    /// Post propagator \f$ x_0 = x_1\f$
    static ExecStatus post(Space& home, V0 X, V1 Y);
    
    /// Perform propagation
    virtual ExecStatus propagate(Space& home, const ModEventDelta& med);

    /// Copy propagator during cloning
    virtual Actor* copy(Space& home, bool share);
    
    /// Cost function
    virtual PropCost cost(const Space& home, const ModEventDelta& med) const;
  };
  
}}}


#include <gecode/string/reverse/base.hpp>
#include <gecode/string/reverse/idem.hpp>
#endif
