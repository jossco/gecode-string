/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 *  Main authors:
 *     Christian Schulte <schulte@gecode.org>
 *     Joseph Scott <josephdscott@gmail.com>
 *
 *  Copyright:
 *     Christian Schulte, 2002
 *     Joseph Scott, 2016
 *
 *  Last modified:
 *     $Date: 2016-04-20 08:20:30 +0200 (Wed, 20 Apr 2016) $ by $Author: jossco $
 *     $Revision: 14968 $
 *
 *  This file is part of Gecode, the generic constraint
 *  development environment:
 *     http://www.gecode.org
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <gecode/string/concat.hh>

namespace Gecode {
  void 
  concat(Home home, StringVar _x, StringVar _y, StringVar _z) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::StringView y(_y);
    String::StringView z(_z);
    if (same(z,x)) {
      // z = x . ''
      GECODE_ME_FAIL(y.llq(home,0));
    }
    if (same(z,y)) {
      // z = '' . y
      GECODE_ME_FAIL(x.llq(home,0));
    }
    GECODE_ES_FAIL((String::Concat::Base< String::StringView, String::StringView, String::StringView >
                        ::post(home, x, y, z)));
  }
  
  void 
  concat(Home home, StringVar _x, StringVar _y, const IntArgs& _z) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::StringView y(_y);
    String::ConstStringView z(home,_z);
    GECODE_ES_FAIL((String::Concat::Base< String::StringView, String::StringView, String::ConstStringView >::post(home, x, y, z)));
  }
  
  void 
  concat(Home home, StringVar _x, const IntArgs& _y, StringVar _z) {
    if(home.failed()) return;
    String::StringView x(_x);
    String::ConstStringView y(home,_y);
    String::StringView z(_z);
    GECODE_ES_FAIL((String::Concat::Base< String::StringView, String::ConstStringView, String::StringView >::post(home, x, y, z)));
  }
  
  void 
  concat(Home home, const IntArgs& _x, StringVar _y, StringVar _z) {
    if(home.failed()) return;
    String::ConstStringView x(home,_x);
    String::StringView y(_y);
    String::StringView z(_z);
    GECODE_ES_FAIL((String::Concat::Base< String::ConstStringView, String::StringView, String::StringView >::post(home, x, y, z)));
  }
}