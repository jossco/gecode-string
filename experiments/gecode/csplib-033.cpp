/* -*- mode: C++; c-basic-offset: 2; indent-tabs-mode: nil; compile-command: "make csplib-033" -*-*/

/* http://www.csplib.org/Problems/prob033

This problem has its roots in Bioinformatics and Coding Theory.

Problem: find as large a set S of strings (words) of length 8 
over the alphabet W = { A,C,G,T } with the following properties:

    Each word in S has 4 symbols from { C,G };

    Each pair of distinct words in S differ in at least 4 positions;

    Each pair of words x and y in S (where x and y may be identical) 
    are such that xR and yC differ in at least 4 positions. 
    Here, ( x1,…,x8 )R = ( x8,…,x1 ) is the reverse of ( x1,…,x8 ) 
    and ( y1,…,y8 )C is the Watson-Crick complement of ( y1,…,y8 ), 
    i.e. the word where each A is replaced by a T and vice versa 
    and each C is replaced by a G and vice versa.
*/

#include "string-model.hh"
#include "aggregate-model.hh"

int decomp80[80][8] = 
{
  {1, 1, 1, 1, 2, 2, 2, 2},
  {1, 1, 1, 1, 3, 3, 3, 3},
  {1, 1, 2, 2, 1, 1, 2, 2},
  {1, 1, 2, 2, 2, 2, 1, 1},
  {1, 1, 2, 2, 3, 4, 3, 4},
  {1, 1, 2, 2, 4, 3, 4, 3},
  {1, 1, 2, 3, 1, 3, 3, 1},
  {1, 1, 2, 3, 2, 4, 4, 2},
  {1, 1, 2, 3, 3, 1, 1, 3},
  {1, 1, 2, 3, 4, 2, 2, 4},
  {1, 1, 3, 3, 1, 2, 1, 2},
  {1, 1, 3, 3, 2, 1, 2, 1},
  {1, 1, 3, 3, 4, 4, 3, 3},
  {1, 1, 3, 4, 1, 3, 2, 3},
  {1, 1, 3, 4, 3, 1, 3, 2},
  {1, 1, 4, 2, 1, 2, 3, 3},
  {1, 1, 4, 2, 3, 3, 1, 2},
  {1, 2, 1, 2, 2, 1, 1, 2},
  {1, 2, 1, 2, 1, 2, 2, 1},
  {1, 2, 1, 3, 1, 1, 3, 3},
  {1, 2, 1, 3, 4, 2, 4, 2},
  {1, 2, 1, 4, 2, 3, 3, 1},
  {1, 2, 1, 4, 3, 2, 1, 3},
  {1, 2, 2, 1, 1, 2, 4, 3},
  {1, 2, 2, 1, 2, 1, 2, 4},
  {1, 2, 2, 1, 3, 3, 1, 1},
  {1, 2, 2, 1, 4, 4, 3, 2},
  {1, 2, 3, 1, 2, 4, 1, 3},
  {1, 2, 3, 2, 1, 4, 4, 2},
  {1, 2, 3, 2, 4, 1, 3, 1},
  {1, 2, 3, 4, 3, 4, 2, 1},
  {1, 2, 3, 4, 4, 3, 1, 2},
  {1, 2, 4, 1, 1, 3, 2, 2},
  {1, 2, 4, 2, 4, 4, 2, 3},
  {1, 3, 1, 2, 1, 3, 1, 3},
  {1, 3, 1, 3, 1, 4, 2, 2},
  {1, 3, 2, 1, 3, 1, 4, 2},
  {1, 3, 2, 4, 1, 4, 3, 3},
  {1, 3, 2, 4, 4, 3, 2, 1},
  {1, 3, 3, 1, 4, 1, 2, 3},
  {1, 3, 3, 1, 1, 2, 3, 1},
  {1, 3, 3, 2, 3, 1, 1, 4},
  {1, 3, 4, 2, 2, 1, 4, 3},
  {1, 3, 4, 3, 3, 2, 1, 1},
  {1, 4, 1, 2, 2, 4, 3, 3},
  {1, 4, 1, 2, 4, 3, 2, 2},
  {1, 4, 2, 4, 2, 3, 1, 3},
  {1, 4, 2, 4, 3, 2, 3, 1},
  {1, 4, 3, 2, 4, 2, 1, 3},
  {1, 4, 4, 3, 2, 1, 3, 2},
  {2, 1, 1, 2, 1, 2, 4, 2},
  {2, 1, 1, 2, 2, 1, 3, 1},
  {2, 1, 1, 3, 4, 3, 2, 1},
  {2, 1, 1, 4, 2, 3, 1, 2},
  {2, 1, 2, 1, 4, 2, 1, 2},
  {2, 1, 2, 1, 1, 4, 2, 3},
  {2, 1, 2, 4, 2, 1, 4, 3},
  {2, 1, 3, 2, 1, 1, 1, 3},
  {2, 1, 3, 3, 4, 1, 4, 2},
  {2, 1, 4, 1, 3, 4, 3, 2},
  {2, 1, 4, 4, 2, 2, 2, 1},
  {2, 2, 1, 1, 4, 1, 2, 2},
  {2, 2, 2, 1, 1, 1, 3, 1},
  {2, 2, 2, 4, 1, 4, 1, 2},
  {2, 2, 4, 1, 3, 1, 1, 3},
  {2, 2, 4, 2, 4, 2, 1, 1},
  {2, 2, 4, 3, 1, 4, 2, 1},
  {2, 3, 1, 2, 3, 4, 4, 1},
  {2, 3, 2, 3, 4, 1, 1, 1},
  {2, 3, 3, 4, 1, 3, 1, 1},
  {2, 3, 4, 4, 1, 1, 3, 2},
  {2, 4, 3, 1, 2, 4, 2, 1},
  {2, 4, 3, 3, 1, 2, 4, 1},
  {3, 1, 1, 2, 4, 4, 3, 2},
  {3, 1, 2, 1, 3, 1, 2, 1},
  {3, 1, 3, 2, 1, 3, 4, 1},
  {3, 1, 4, 3, 4, 2, 3, 1},
  {3, 2, 1, 3, 1, 3, 1, 1},
  {3, 2, 2, 4, 4, 2, 4, 1},
  {3, 3, 1, 1, 1, 2, 1, 2}
};

int string80[80][8] = {
  {1, 1, 1, 1, 2, 2, 2, 2},
  {1, 1, 1, 1, 3, 3, 3, 3},
  {1, 1, 2, 2, 1, 1, 2, 2},
  {1, 1, 2, 2, 2, 2, 1, 1},
  {1, 1, 2, 2, 3, 3, 4, 4},
  {1, 1, 2, 2, 4, 4, 3, 3},
  {1, 1, 2, 3, 1, 2, 3, 4},
  {1, 1, 2, 3, 2, 1, 4, 3},
  {1, 1, 2, 3, 3, 4, 1, 2},
  {1, 1, 2, 3, 4, 3, 2, 1},
  {1, 1, 3, 2, 1, 2, 4, 3},
  {1, 1, 3, 2, 2, 1, 3, 4},
  {1, 1, 3, 2, 3, 4, 2, 1},
  {1, 1, 3, 2, 4, 3, 1, 2},
  {1, 1, 4, 4, 2, 2, 3, 3},
  {1, 1, 4, 4, 3, 3, 2, 2},
  {1, 2, 1, 2, 1, 1, 3, 3},
  {1, 2, 1, 2, 2, 2, 4, 4},
  {1, 2, 1, 2, 3, 3, 1, 1},
  {1, 2, 1, 2, 4, 4, 2, 2},
  {1, 2, 1, 3, 1, 2, 1, 2},
  {1, 2, 1, 3, 2, 1, 2, 1},
  {1, 2, 1, 3, 3, 4, 3, 4},
  {1, 2, 1, 3, 4, 3, 4, 3},
  {1, 2, 2, 1, 1, 2, 2, 1},
  {1, 2, 2, 1, 2, 1, 1, 2},
  {1, 2, 2, 1, 3, 4, 4, 3},
  {1, 2, 2, 1, 4, 3, 3, 4},
  {1, 2, 2, 4, 1, 3, 1, 3},
  {1, 2, 2, 4, 2, 4, 2, 4},
  {1, 2, 2, 4, 3, 1, 3, 1},
  {1, 2, 2, 4, 4, 2, 4, 2},
  {1, 2, 3, 1, 1, 3, 4, 2},
  {1, 2, 3, 1, 2, 4, 3, 1},
  {1, 2, 3, 1, 3, 1, 2, 4},
  {1, 2, 3, 1, 4, 2, 1, 3},
  {1, 2, 4, 2, 1, 3, 2, 4},
  {1, 2, 4, 2, 2, 4, 1, 3},
  {1, 2, 4, 2, 3, 1, 4, 2},
  {1, 2, 4, 2, 4, 2, 3, 1},
  {1, 3, 1, 3, 1, 3, 3, 1},
  {1, 3, 1, 3, 2, 4, 4, 2},
  {1, 3, 1, 3, 3, 1, 1, 3},
  {1, 3, 1, 3, 4, 2, 2, 4},
  {1, 3, 2, 1, 1, 4, 3, 2},
  {1, 3, 2, 1, 2, 3, 4, 1},
  {1, 3, 2, 1, 3, 2, 1, 4},
  {1, 3, 2, 1, 4, 1, 2, 3},
  {1, 3, 3, 4, 1, 4, 2, 3},
  {1, 3, 3, 4, 2, 3, 1, 4},
  {1, 3, 3, 4, 3, 2, 4, 1},
  {1, 3, 3, 4, 4, 1, 3, 2},
  {1, 4, 1, 4, 2, 3, 2, 3},
  {1, 4, 1, 4, 3, 2, 3, 2},
  {1, 4, 4, 1, 2, 3, 3, 2},
  {1, 4, 4, 1, 3, 2, 2, 3},
  {2, 1, 1, 2, 1, 2, 2, 1},
  {2, 1, 1, 2, 2, 1, 1, 2},
  {2, 1, 1, 2, 3, 4, 4, 3},
  {2, 1, 1, 2, 4, 3, 3, 4},
  {2, 1, 1, 3, 1, 3, 1, 3},
  {2, 1, 1, 3, 2, 4, 2, 4},
  {2, 1, 1, 3, 3, 1, 3, 1},
  {2, 1, 1, 3, 4, 2, 4, 2},
  {2, 1, 2, 1, 1, 1, 3, 3},
  {2, 1, 2, 1, 2, 2, 4, 4},
  {2, 1, 2, 1, 3, 3, 1, 1},
  {2, 1, 2, 1, 4, 4, 2, 2},
  {2, 1, 2, 4, 1, 2, 1, 2},
  {2, 1, 2, 4, 2, 1, 2, 1},
  {2, 1, 2, 4, 3, 4, 3, 4},
  {2, 1, 2, 4, 4, 3, 4, 3},
  {2, 1, 3, 1, 1, 3, 2, 4},
  {2, 1, 3, 1, 2, 4, 1, 3},
  {2, 1, 3, 1, 3, 1, 4, 2},
  {2, 1, 3, 1, 4, 2, 3, 1},
  {2, 1, 4, 2, 1, 3, 4, 2},
  {2, 1, 4, 2, 2, 4, 3, 1},
  {2, 1, 4, 2, 3, 1, 2, 4},
  {2, 1, 4, 2, 4, 2, 1, 3},
};




enum {MODEL_STRING, MODEL_AGGREGATE, MODEL_DECOMP, SEARCH_STRING_80, SEARCH_DECOMP_80, SEARCH_DFS};
class Benchmark : public StringModel {
protected:
  StringVarArray rev;
  StringVarArray comp;
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt), 
    rev(*this,opt.size(),8,8,4),
    comp(*this,opt.size(),8,8,4)
  {
    x = StringVarArray(*this,opt.size(),8,8,4);
    l = IntVarArray();
    // DFAs:
    REG re = *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4)));
    DFA dfa(re);
    
    // Constraints:
    
    for (int i = 0; i < x.size(); i++) {
      extensional(*this,x[i],dfa);
      reverse(*this, x[i], rev[i]);
      morph(*this, x[i], comp[i], IntArgs(4, 1,2,3,4), IntArgs(4, 4,3,2,1));
      distance(*this,rev[i],comp[i],IRT_GQ,4);
      for (int j = i+1; j < x.size(); j++) {
        distance(*this,x[i],x[j],IRT_GQ,4);
        distance(*this,rev[i],comp[j],IRT_GQ,4);
        distance(*this,rev[j],comp[i],IRT_GQ,4);
      }
      
    }
    
    switch (opt.search()) {
      case SEARCH_STRING_80:
        for (int i = 0; i < 80; i++) {
          IntArgs cs;
          cs << string80[i][0] << string80[i][1] << string80[i][2] << string80[i][3] << string80[i][4]<< string80[i][5] << string80[i][6] << string80[i][7];
          equal(*this,x[i],cs);
        }
        break;
      case SEARCH_DECOMP_80:
        for (int i = 0; i < 80; i++) {
          IntArgs cs; 
          cs << string80[i][0] << string80[i][1] << string80[i][2] << string80[i][3] << string80[i][4]<< string80[i][5] << string80[i][6] << string80[i][7];
          equal(*this,x[i],cs);
        }
        break;
      default: break;
    }
    
    post_brancher(x,opt);
  }
  
	Benchmark(bool share, Benchmark& s)
    : StringModel(share,s) {
      rev.update(*this,share,s.rev);
      comp.update(*this,share,s.comp);
    }

	virtual Space* copy(bool share) {
		return new Benchmark(share,*this);
	}
  
  virtual void print(std::ostream& os) const {
    for (int i = 0; i < x.size(); i++)
      os << x[i] << std::endl;
    os << "----------------------------" << std::endl;
  }
};

class DecompModel : public Script {
protected:
  IntVarArray bases;
  IntVarArray comps;
  int size;
public:
  DecompModel(const StringOptions& opt)
  : Script(opt), 
    bases(*this, opt.size()*8, 1,4),
    comps(*this,       opt.size()*8, 1,4),
    size(opt.size())
  {
    // DFAs:
    REG re = *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
           + *(REG(IntArgs(2, 1,4)));
    DFA dfa(re);
    
    Matrix<IntVarArray> b(bases,8,opt.size());
    Matrix<IntVarArray> c(comps,8,opt.size());
    // Constraints:
    
    // complements : A<->T and C<->G
    for (int i = 0; i < bases.size(); i++) {
      rel(*this, bases[i] == 5 - comps[i]);
    }
    for (int i = 0; i < opt.size(); i++) {
      // 4 symbols from { C,G }
      extensional(*this,b.row(i),dfa);
      
      {
        IntVarArgs rev;
        for(int j = 8; j--; ) {
          rev << b(j,i);
        }
      
        BoolVarArray diff(*this,8);
        for(int j = 8; j--; ) {
          diff[j] = expr(*this, rev[j] != c(j,i));
        }
        rel (*this, sum(diff) >= 4);
      }
      
      
      for (int i2 = i+1; i2 < opt.size(); i2++) {
        //distance(*this,x[i],x[i2],IRT_GQ,4);
        {
          BoolVarArray diff(*this,8);
          for(int j = 8; j--; ) {
            diff[j] = expr(*this, b(j,i) != b(j,i2));
          }
          rel (*this, sum(diff) >= 4);
        }
        //distance(*this,rev[i],comp[i2],IRT_GQ,4);
        {
          IntVarArgs rev;
          for(int j = 8; j--; ) {
            rev << b(j,i);
          }
          
          BoolVarArray diff(*this,8);
          for(int j = 8; j--; ) {
            diff[j] = expr(*this, rev[j] != c(j,i2));
          }
          rel (*this, sum(diff) >= 4);
        }
        //distance(*this,rev[i2],comp[i],IRT_GQ,4);
        {
          IntVarArgs rev;
          for(int j = 8; j--; ) {
            rev << b(j,i2);
          }
          
          BoolVarArray diff(*this,8);
          for(int j = 8; j--; ) {
            diff[j] = expr(*this, rev[j] != c(j,i));
          }
          rel (*this, sum(diff) >= 4);
        }
      }
      
    }
    
    switch (opt.search()) {
      case SEARCH_STRING_80:
        for (int i = 0; i < 80; i++) {
          for (int j = 0; j < 8; j++) {
            rel(*this,b(j,i) == string80[i][j]);
          }
        }
        break;
      case SEARCH_DECOMP_80:
        for (int i = 0; i < 80; i++) {
          for (int j = 0; j < 8; j++) {
            rel(*this,b(j,i) == decomp80[i][j]);
          }
        }
        break;
      default: break;
    }
    
    branch(*this,bases,INT_VAR_SIZE_MIN(),INT_VAL_MIN());
  }
  
     DecompModel(bool share, DecompModel& s)
    : Script(share,s), size(s.size) {
      bases.update(*this,share,s.bases);
      comps.update(*this,share,s.comps);
    }

	virtual Space* copy(bool share) {
		return new DecompModel(share,*this);
	}
  
  virtual void print(std::ostream& os) const {
    Matrix<IntVarArray> b(bases,8,size);
    for (int i = 0; i < size; i++)
      os << b.row(i) << std::endl;
    os << "----------------------------" << std::endl;
  }
};

// class AggModel : public Script {
// protected:
//   IntVarArray bases;
//   IntVarArray comps;
//   IntVarArray revs;
//   int size;
// public:
//   AggModel(const StringOptions& opt)
//   : Script(opt),
//     bases(*this, opt.size()*8, 1,4),
//     comps(*this,       opt.size()*8, 1,4),
//     revs(*this,       opt.size()*8, 1,4),
//     size(opt.size())
//   {
//     // DFAs:
//     REG re = *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
//            + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
//            + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
//            + *(REG(IntArgs(2, 1,4))) + REG(IntArgs(2, 2,3))
//            + *(REG(IntArgs(2, 1,4)));
//     DFA dfa(re);
//
//     Matrix<IntVarArray> b(bases,8,opt.size());
//     Matrix<IntVarArray> c(comps,8,opt.size());
//     Matrix<IntVarArray> r(revs,8,opt.size());
//     // Constraints:
//
//     // complements : A<->T and C<->G
//     for (int i = 0; i < bases.size(); i++) {
//       rel(*this, bases[i] == 5 - comps[i]);
//     }
//     for (int i = 0; i < opt.size(); i++) {
//       // 4 symbols from { C,G }
//       extensional(*this,b.row(i),dfa);
//       reverse(*this,b.row(i),r.row(i));
//
//       {
//         BoolVarArray diff(*this,8);
//         for(int j = 8; j--; ) {
//           diff[j] = expr(*this, r(j,i) != c(j,i));
//         }
//         rel (*this, sum(diff) >= 4);
//       }
//
//
//       for (int i2 = i+1; i2 < opt.size(); i2++) {
//         //distance(*this,x[i],x[i2],IRT_GQ,4);
//         {
//           BoolVarArray diff(*this,8);
//           for(int j = 8; j--; ) {
//             diff[j] = expr(*this, b(j,i) != b(j,i2));
//           }
//           rel (*this, sum(diff) >= 4);
//         }
//         //distance(*this,rev[i],comp[i2],IRT_GQ,4);
//         {
//           BoolVarArray diff(*this,8);
//           for(int j = 8; j--; ) {
//             diff[j] = expr(*this, r(j,i) != c(j,i2));
//           }
//           rel (*this, sum(diff) >= 4);
//         }
//         //distance(*this,rev[i2],comp[i],IRT_GQ,4);
//         {
//           BoolVarArray diff(*this,8);
//           for(int j = 8; j--; ) {
//             diff[j] = expr(*this, r(j,i2) != c(j,i));
//           }
//           rel (*this, sum(diff) >= 4);
//         }
//       }
//
//     }
//
//     switch (opt.search()) {
//       case SEARCH_STRING_80:
//         for (int i = 0; i < 80; i++) {
//           for (int j = 0; j < 8; j++) {
//             rel(*this,b(j,i) == string80[i][j]);
//           }
//         }
//         break;
//       case SEARCH_DECOMP_80:
//         for (int i = 0; i < 80; i++) {
//           for (int j = 0; j < 8; j++) {
//             rel(*this,b(j,i) == decomp80[i][j]);
//           }
//         }
//         break;
//       default: break;
//     }
//
//     branch(*this,bases,INT_VAR_SIZE_MIN(),INT_VAL_MIN());
//   }
//
//      AggModel(bool share, AggModel& s)
//     : Script(share,s), size(s.size) {
//       bases.update(*this,share,s.bases);
//       comps.update(*this,share,s.comps);
//     }
//
//   virtual Space* copy(bool share) {
//     return new AggModel(share,*this);
//   }
//
//   virtual void print(std::ostream& os) const {
//     Matrix<IntVarArray> b(bases,8,size);
//     for (int i = 0; i < size; i++)
//       os << b.row(i) << std::endl;
//     os << "----------------------------" << std::endl;
//   }
// };

int main(int argc, char* argv[]) {
  Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
  
	StringOptions opt("CSPLib-033::Word Design for DNA Computing Surfaces");
  opt.solutions(1);
  opt.size(60);
  
  opt.model(MODEL_STRING, "string", "model with native string type");
  opt.model(MODEL_AGGREGATE, "aggregate", "model with aggregate implementation propagators");
  opt.model(MODEL_DECOMP, "decomp", "decompose into integer arrays and simple constraints");
  opt.model(MODEL_STRING);
  
  opt.search(SEARCH_DFS, "dfs", "dfs");
  opt.search(SEARCH_STRING_80, "string-80", "confirm the string-80 solution");
  opt.search(SEARCH_DECOMP_80, "decomp-80", "confirm the decomp-80 solution");
  opt.search(SEARCH_DFS);

  StringModel::standardOptions(opt);
  opt.parse(argc,argv);

  switch(opt.search()){
  case SEARCH_STRING_80:
  case SEARCH_DECOMP_80:
    opt.size(80);break;
  default: break;
  }

  switch(opt.model()) {
  case MODEL_STRING:
    Script::run<Benchmark,DFS,StringOptions>(opt); break;
  case MODEL_AGGREGATE:
  case MODEL_DECOMP:
    Script::run<DecompModel,DFS,StringOptions>(opt); break;
  }
  
  return 0;
}
