/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), [('str.++',), ('var_3',), ('var_4',)], [('re.union',), [('re.++',), [('str.to.re',), 'a'], [('str.to.re',), 'b']], [('re.++',), [('str.to.re',), 'b'], [('str.to.re',), 'a']]]]]
[('assert',), [('and',), [('and',), [('and',), [('and',), [('and',), [('and',), [('<=',), [('+',), [('str.len',), ('var_4',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_4',)], [('-',), 1]]]], [('and',), [('<=',), [('+',), [('str.len',), ('var_3',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_3',)], [('-',), 1]]]]], [('and',), [('<=',), [('str.len',), ('var_5',)], 0], [('<=',), 0, [('str.len',), ('var_5',)]]]], [('and',), [('<=',), [('+',), [('+',), [('str.len',), ('var_6',)], [('*',), [('-',), 1], [('str.len',), ('var_2',)]]], 1], 0], [('<=',), 0, [('+',), [('+',), [('str.len',), ('var_6',)], [('*',), [('-',), 1], [('str.len',), ('var_2',)]]], 1]]]], [('and',), [('<=',), [('+',), [('str.len',), ('var_1',)], [('-',), 1]], 0], [('<=',), 0, [('+',), [('str.len',), ('var_1',)], [('-',), 1]]]]], [('<=',), 0, [('+',), [('str.len',), ('var_2',)], [('-',), 2]]]]]
[('check-sat',)]
*/

#include <gecode/driver.hh>
#include <gecode/minimodel.hh>
#include <gecode/string.hh>

#include "string-model.hh"

class Benchmark : public IntMinimizeScript {
  
protected:
  StringVar x;
  IntVar lenx;
  IntVarArray l;
  BoolVarArray b;
  int offset;
  
  void post_brancher(StringVarArgs y, const StringOptions& opt) {
    switch(opt.branching()) {
    case BR_WMIN_SPLIT_MIN:
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_WMAX_SPLIT_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_MIN_SPLIT_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_WMIN_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_WMAX_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_MIN_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_WMIN_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_WMAX_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_MIN_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_WMIN_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_MIN,String::LEN_MED); break;
    case BR_WMAX_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_MIN,String::LEN_MED); break;
    case BR_MIN_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_MIN,String::LEN_MED); break;
		}
  }
public:
  enum {BR_WMIN_SPLIT_MIN, BR_WMAX_SPLIT_MIN, BR_MIN_SPLIT_MIN, 
        BR_WMIN_SPLIT_MED, BR_WMAX_SPLIT_MED, BR_MIN_SPLIT_MED, 
        BR_WMIN_MIN_MIN,   BR_WMAX_MIN_MIN,   BR_MIN_MIN_MIN, 
        BR_WMIN_MIN_MED,   BR_WMAX_MIN_MED,   BR_MIN_MIN_MED};
        
  
    
	Benchmark(bool share, Benchmark& s)
	: IntMinimizeScript(share,s), offset(s.offset) {
    x.update(*this, share, s.x);
    lenx.update(*this,share,s.lenx);
    l.update(*this, share, s.l);
    b.update(*this, share, s.b);
	}

  static void standardOptions(StringOptions& opt) {
    opt.branching(Benchmark::BR_WMIN_SPLIT_MIN, "widthmin-split-min", "branching: index - width min; val - split; length - min");
    opt.branching(Benchmark::BR_WMAX_SPLIT_MIN, "widthmax-split-min", "branching: index - width max; val - split; length - min");
    opt.branching(Benchmark::BR_MIN_SPLIT_MIN, "min-split-min", "branching: index - min; val - split; length - min");
    opt.branching(Benchmark::BR_WMIN_SPLIT_MED, "widthmin-split-med", "branching: index - width min; val - split; length - med");
    opt.branching(Benchmark::BR_WMAX_SPLIT_MED, "widthmax-split-med", "branching: index - width max; val - split; length - med");
    opt.branching(Benchmark::BR_MIN_SPLIT_MED, "min-split-med", "branching: index - min; val - split; length - med");
    opt.branching(Benchmark::BR_WMIN_MIN_MIN, "widthmin-min-min", "branching: index - width min; val - min; length - min");
    opt.branching(Benchmark::BR_WMAX_MIN_MIN, "widthmax-min-min", "branching: index - width max; val - min; length - min");
    opt.branching(Benchmark::BR_MIN_MIN_MIN, "min-min-min", "branching: index - min; val - min; length - min");
    opt.branching(Benchmark::BR_WMIN_MIN_MED, "widthmin-min-med", "branching: index - width min; val - min; length - med");
    opt.branching(Benchmark::BR_WMAX_MIN_MED, "widthmax-min-med", "branching: index - width max; val - min; length - med");
    opt.branching(Benchmark::BR_MIN_MIN_MED, "min-min-med", "branching: index - min; val - min; length - med");
    opt.branching(Benchmark::BR_MIN_SPLIT_MIN);
  }
  
  int char2val(char a) const {
    return ((int)a) - offset;
  }
  
  char val2char(int n) const {
    return (char)(n + offset);
  }

  IntArgs str2IntArgs(std::string s) const {
    IntArgs arg;
    for (size_t i = 0; i < s.length(); i++)
      arg << char2val(s[i]);
    return arg;
  }
  
  std::string IntArg2str(IntArgs arg) const {
    std::string s;
    for (int i = 0; i < arg.size(); i++)
      s[i] = val2char(arg[i]);
    return s;
  }
  
  BitSet alphabetof(const DFA& dfa) const {
    BitSet alpha = Empty;
    for (DFA::Symbols sym(dfa); sym(); ++sym) {
      alpha = unionof(alpha,singletonfor(sym.val()));
    }
    return alpha;
  }
  
  Benchmark(const StringOptions& opt)
    : IntMinimizeScript(opt), 
      x(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block()),
      lenx(*this,1,opt.length()),
      b(*this,opt.length(),0,1), 
      offset(opt.min() - 1)
  {    
	//Variables:
	IntVarArgs _l;
	//Constraints:
  IntVar k(*this,1,opt.length());
  _l << k;
  IntVar kp(*this,0,opt.length());
  _l << kp;
  IntVarArray xs(*this, opt.length(), 0, opt.width());
  _l << xs;
  l = IntVarArray(*this,_l);
  
  reverse(*this,   x,x);
  alphabet(*this,  x,char2val('a'),char2val('z'));
  length(*this,    x,lenx);
  rel(*this,       lenx % 2 == 1);
  
  for (int i = 0; i < opt.length(); i++) {
    channel(*this,x,i,xs[i],b[i]);
    rel(*this,xs[i],IRT_NQ,0,b[i]);
  }
  count(*this, xs, char2val('a'), IRT_EQ, k);
  count(*this, xs, char2val('b'), IRT_EQ, k);
  count(*this, xs, char2val('c'), IRT_EQ, k);

  StringVarArgs _x;
  _x << x;
    branch(*this,lenx,INT_VAL_MIN());
    post_brancher(_x,opt);
  }


	virtual Space* copy(bool share) {
		return new Benchmark(share,*this);
	}

  virtual void print(std::ostream& os) const {
    os << '"' << x << '"';
    os << '(';
    for (int i = 2; i < l.size(); i++) {
      os  << l[i] << ',';
    }
    os << ')';
    os << std::endl << "k = " << l[0] << std::endl;
    os << "----------------------------" << std::endl;
  }
  
  virtual IntVar cost (void) const {
    return lenx;
  }
};

int main(int argc, char* argv[]) {
  Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
  StringOptions opt("Benchmark::palindrome");

  opt.solutions(1);
  opt.min(65);
  Benchmark::standardOptions(opt);
  opt.parse(argc,argv);

  Script::run<Benchmark,BAB,StringOptions>(opt);
	return 0;
}
    