#! /bin/bash

RESULTS=csplib-033-results/
for MODEL in "string" "decomp"
do
#    for SIZE in 60 70 80 85 90 100 107 112
    for SIZE in 86 87 88 89
    do
	PREFIX="$MODEL"-"$SIZE"
	echo -n "${PREFIX}...."
	./csplib-033 -model "$MODEL" -size "$SIZE" -file-sol "${RESULTS}${PREFIX}.sol" -file-stat "${RESULTS}${PREFIX}.stat" -time 300000
	if grep -q "time limit reached" "${RESULTS}${PREFIX}.stat"
	then
	    echo "timeout"
	    break
	else
	    echo "solved"
	fi
    done
done
