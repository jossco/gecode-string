#!/bin/bash

RES="norn-results/"
#for ID in 2 16 20 25 29 37 84 89 102 136 142 154 389 1005 1168 1225
for ID in 20
do
    for LEN in 256 512 1024
    do
	PREFIX="${RES}${ID}-string-len${LEN}"
	echo -n "${PREFIX}..."
	"./norn-benchmark-${ID}" -length "$LEN" -time 300000 -solutions 1 \
	    -file-stat "${PREFIX}.stat" -file-sol "${PREFIX}.sol"
	echo done
    done
done
