/*
[('set-logic',), ('QF_S',)]
[('declare-fun',), ('var_0',), [], ('String',)]
[('declare-fun',), ('var_1',), [], ('String',)]
[('declare-fun',), ('var_2',), [], ('String',)]
[('declare-fun',), ('var_3',), [], ('String',)]
[('declare-fun',), ('var_4',), [], ('String',)]
[('declare-fun',), ('var_5',), [], ('String',)]
[('declare-fun',), ('var_6',), [], ('String',)]
[('declare-fun',), ('var_7',), [], ('String',)]
[('declare-fun',), ('var_8',), [], ('String',)]
[('declare-fun',), ('var_9',), [], ('String',)]
[('declare-fun',), ('var_10',), [], ('String',)]
[('declare-fun',), ('var_11',), [], ('String',)]
[('declare-fun',), ('var_12',), [], ('String',)]
[('assert',), [('str.in.re',), ('var_1',), [('re.*',), [('re.range',), 'a', 'u']]]]
[('assert',), [('str.in.re',), ('var_1',), [('re.++',), [('re.*',), [('str.to.re',), 'a']], [('re.++',), [('str.to.re',), 'b'], [('re.*',), [('str.to.re',), 'b']]]]]]
[('assert',), [('str.in.re',), ('var_1',), [('re.++',), [('re.++',), [('re.++',), [('re.*',), [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'b']]], [('str.to.re',), 'b']], [('str.to.re',), 'a']], [('re.*',), [('re.union',), [('str.to.re',), 'a'], [('str.to.re',), 'b']]]]]]
[('assert',), [('not',), [('str.in.re',), '', ('re.nostr',)]]]
[('check-sat',)]
*/

#include "string-model.hh"

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {    
	//DFAs:
	REG r_0 = *(REG(IntArgs::create(char2val('u')-char2val('a')+1,char2val('a'))));
	DFA d_0(r_0);
	BitSet a_0 = alphabetof(d_0);
	REG r_1 = (*(REG(char2val('a')))) + ((REG(char2val('b'))) + (*(REG(char2val('b')))));
	DFA d_1(r_1);
	BitSet a_1 = alphabetof(d_1);
	REG r_2 = (((*((REG(char2val('a'))) | (REG(char2val('b'))))) + (REG(char2val('b')))) + (REG(char2val('a')))) + (*((REG(char2val('a'))) | (REG(char2val('b')))));
	DFA d_2(r_2);
	BitSet a_2 = alphabetof(d_2);
	REG r_3;
	DFA d_3(r_3);
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar var_1(*this,opt.minlength(),opt.length(),opt.width(),interof(interof(interof(Full,a_0),a_1),a_2),opt.block());
	_x << var_1;
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	//Constraints:
	extensional(*this,var_1,d_0);
	extensional(*this,var_1,d_1);
	extensional(*this,var_1,d_2);
	extensional(*this,str2IntArgs(""),d_3);

    
        post_brancher(x,opt);
      }
  
    	Benchmark(bool share, Benchmark& s)
        : StringModel(share,s) {}

    	virtual Space* copy(bool share) {
    		return new Benchmark(share,*this);
    	}
  
      virtual void print(std::ostream& os) const {
        os << x << std::endl;
        os << l << std::endl;
        os << "----------------------------" << std::endl;
      }
    };

    int main(int argc, char* argv[]) {
      Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
	StringOptions opt("Benchmark::ab/norn-benchmark-102");

      opt.solutions(1);
      opt.min(65);
      StringModel::standardOptions(opt);
      opt.parse(argc,argv);

      Script::run<Benchmark,DFS,StringOptions>(opt);
    	return 0;
    }
    