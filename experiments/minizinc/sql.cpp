#include <cstring>
#include "string-model.hh"

/*
var 0..M - 1: n;
var 0..M - 1: m;

var string(M): pref;
var string(M): suff;
var string(M): expr;

constraint str_len(expr) > 0;
constraint sql =
  pref ++ expr ++ str_pow(" ", n) ++ "=" ++ str_pow(" ", m) ++ expr ++ suff;
*/

std::string in_sql[] = {
	"Zz1xJr2yi3kD0njK4mjOWu5HaEnNDg9Ha7o8AniRkfU66m8EpyDsD5yYEF4PAtfcK1fI2aO0xPMfA1gtXhV685G9bVD6MX7urD0Uxq5P2lGERM6iqQYjpgZhuRMNDUCccQHMcnGvUofrvJskrn2vbrKFwvPaNcKlLnqQql7Ut39SxWLnS0kcASqfnDKMLiQLTvXOkcZ0 =09GXSOVfpYV3pELJMYcqwPm5H1O0IQXnE8o3KvItPWQFQopN",
	"V3EZkXFxqfvMvzNhvrIoI0yp5WhvETOZxtWUQ47HmIkQ8OawmDmG1HlGVxtjK8gZblVXp1bqXEe06gUvXlmsr1A7HKQlQbEzgMvQ3T1sLm3VS8CptcqG6KluEDBFE9cNAjHDzeUNYgTv68TVWDbGjnGkjcekG4fqa0GI3252PAkikslnZZz23AojbS9chDhUvrzrNVrTjABkewQiseG36qsgikKLFQT047vzNBr2Ffi=iD1qI0nNTKDJXoSSzJZRZNm4JflzJMW8Pb860MGtF74joUN0qTDzEDZLcuWCtE7J8OcDP2GSwF1NCWCT6XA1VWFnXzFVHDOXn7RCQ8F4KWgy6XrFqXlNnDtIgNNUuYXHOeZVY5piP0UFQOpqhJQPveoROORlqLPnrL4VxVHzm4K1V3kOvBp59UI2LAet9ZIb8wrFI3REsY3mxfF397VQBNy7foQR9q061SxKMnvVleqFNtTCtWc0uqWnkDvTrmhuAFDo5ukV",
	"TTtJJ6fXKPfvP2ISgRtEg7RXKW9BfD52BB0Omjk53ZLkCOxtLOYrFF8yiKj8Fi8c1fcKfIkNMj6bDSUU41SLtbCVkt0bjgVHsjlXkO5bHAm6SUhC1Ck9TcyxnhjQEDhMSc3MLZAl05enYYotuknkIlrmNz4Z0qh5ugOVNalkNUBTRr7bf7M9xwFFFceI1pu63Aw7sMVbKDW7Ki4XNemwRwgMahecHYaaxTHIPU660DolMGNmIRLP2vDVzvsywp24i3nT8cqzEKJfn3He9fLBI72ySn86X1jtT3MpiYQTtJr2MoaKcaieLHgUcihfX5EHyfmStybOaQFnT80eigvRz7yz1AWEW5bBprjVcByIrTwFCRThjbyD50AlWCKvJ1qhjraWrzIATSbUpHa95v6eCwYgLAA3z5FGfl18cPtv1cE0ZlSwkpBTECQi71tIIlWxIWISQQiKRGVcfcaPP81oFa9gpXmrEzF18bgz3yCojytmtUUNjMAvmlnEOHCZjuCS8cNc47stnjAfYDoUmRKrDXs42onRmo8yJwUrGK1wZ2Pz3GmsRq1awnBQwkcmDO3vDqhNuvjYvOOZ33DeJFl6ZoOwoGqIiYSEM3lV2 =23uhNDcB5cj1VtOuOYBjqsjwNtK0BiSkiA9Z5LoNRSkheygVVoXHK6ijq7FrjF8PnkLW2An6b44PJqGwiTSBYRWOyx2Jn4cMvJ4fEmVGwax1NbNsTJzmJBW2MLAXW35UwAXNChBSDNBGjvIgIUhsxDFlViC67DkU7MTXY14VJcmnAjSiKI69Hu54qr8CAHLBD7sqEC2tP2lG3J2oc5RePk2lUL2i62bu08Pxhw3rAK4URk2uniCTCMMHDreOZCDbhaOcUK1RCi8q1rFw8V5obOhscEcJPU1WPgqAKGBs0y2mtsHpX3xuXHAqERDUXQAxkSnLt7JBhjNqm0EKe6pp5kwyHogfpMkCKjYI2ZjrT2BnJ8NlYoDb7LbWjPpNTmb5n"
};

class Benchmark : public StringModel {
public:
  Benchmark(const StringOptions& opt)
  : StringModel(opt)
  {    
	char* lit = " ";
	size_t len_lit = std::strlen(lit);
	
	//DFAs:
	REG r = *REG(char2val(' '));
	DFA d(r);
	//BitSet alpha = alphabetof(d);
	
	
	//Variables:
	StringVarArgs _x; IntVarArgs _l;
	StringVar pref(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << pref;
	StringVar suff(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << suff;
	StringVar expr(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << expr;
	StringVar sql(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << sql;
	// temp vars for replacing the str_pow expressions
	StringVar pow_1(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << pow_1;
	StringVar pow_2(*this,opt.minlength(),opt.length(),opt.width(),Full,opt.block());
	_x << pow_2;
	
	StringVar cat_1(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_1;
	StringVar cat_2(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_2;
	StringVar cat_3(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_3;
	StringVar cat_4(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_4;
	StringVar cat_5(*this,opt.minlength(),opt.length()*4,opt.width(),Full,opt.block());
	_x << cat_5;
	
	IntVar n(*this,opt.minlength(),opt.length());
	_l << n;
	IntVar m(*this,opt.minlength(),opt.length());
	_l << m;
	// temp vars for str_len constraints
	IntVar len_expr(*this,opt.minlength(),opt.length());
	_l << len_expr;
	
	x = StringVarArray(*this,_x);
	l = IntVarArray(*this,_l);
	
	//Constraints:
	// str_pow
	//extensional(*this,pow_1,d);
	alphabet(*this, pow_1, char2val(' '), char2val(' '));
	length(*this,pow_1,n);
	
	// str_pow
	//extensional(*this,pow_2,d);
	alphabet(*this, pow_2, char2val(' '), char2val(' '));
	length(*this,pow_2,m);
	
	// concat
	concat(*this, pref, expr, cat_1);
	concat(*this, pow_1, str2IntArgs("="), cat_2);
	concat(*this, cat_1, cat_2, cat_3);
	concat(*this, pow_2, expr, cat_4);
	concat(*this, cat_4, suff, cat_5);
	concat(*this, cat_3, cat_5, sql);
    
    // FIXME: Why unsatisfiable?
		//equal(*this, sql, str2IntArgs(" 0 =0"));
		// this works:
		//equal(*this, sql, str2IntArgs(" 0= 0"));
	switch(opt.length()) {
		case 250: equal(*this, sql, str2IntArgs(in_sql[0])); break;
		case 500: equal(*this, sql, str2IntArgs(in_sql[1])); break;
		case 1000: equal(*this, sql, str2IntArgs(in_sql[2])); break;
		default: equal(*this, sql, str2IntArgs("aaaaaaaaaa0 =0bbbbbbbbbz"));
	}
	
	
	// sql length
	length(*this,expr,len_expr);
	rel(*this, 1 <= len_expr);

  post_brancher(x,opt);
	
  }

	Benchmark(bool share, Benchmark& s)
    : StringModel(share,s) {}

	virtual Space* copy(bool share) {
		return new Benchmark(share,*this);
	}
};

int main(int argc, char* argv[]) {
  Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
  
	  StringOptions opt("SQL Injection");


  opt.solutions(1);
  opt.min(32);
  StringModel::standardOptions(opt);
  opt.parse(argc,argv);
	

  Script::run<Benchmark,DFS,StringOptions>(opt);
	return 0;
}