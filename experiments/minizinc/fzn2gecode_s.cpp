#define BOOST_VARIANT_MINIMIZE_SIZE
#define BOOST_VARAINT_MAX_MULTIVIZITOR_PARAMS 4
#define FZN2GECODE_OFFSET 64

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/optional.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/spirit/include/qi_eol.hpp>
#include <boost/spirit/include/qi_char_class.hpp>
#include <boost/variant/multivisitors.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <ctime>

#include "fzn-model.hh"

using Gecode::Script;
using Gecode::Home;

namespace parser {
	namespace qi = boost::spirit::qi;
	namespace ascii = boost::spirit::ascii;
	namespace fusion = boost::fusion;
	namespace phoenix = boost::phoenix;
	
	std::map<std::string,std::string> type_map;
	std::map< std::string, std::vector<std::string> > var_annotations;
	
	int char2val(char a) {
	    if(a == ' ')
				return 1;
			if(a == '=')
				return 2;
			if(a >= '0' && a <= '9')
				return a - 45;
			if(a >= 'A' && a <= 'Z')
				return a - 52;
			if(a >= 'a' && a <= 'z')
				return a - 84; // maps onto uppercase
	    return 63;
	  }
  
	  char val2char(int n) {
	    if(n == 1)
				return ' ';
			if(n == 2)
				return '=';
			if(n >= 3 && n <= 12)
				return n + 45;
			if(n >= 13 && n <= 38)
				return n + 52;
	    return '!';
	  }

  Gecode::IntArgs str2IntArgs(std::string s) {
    Gecode::IntArgs arg;
    for (size_t i = 0; i < s.length(); i++)
      arg << char2val(s[i]);
    return arg;
  }
	
	Gecode::IntArgs chars2IntArgs(std::vector<char>& s) {
		Gecode::IntArgs arg;
		for(std::vector<char>::size_type i = 0; i < s.size(); i++) {
			arg << char2val(s[i]);
		}
		return arg;
	}
  
  std::string IntArg2str(Gecode::IntArgs arg) {
    std::string s;
    for (int i = 0; i < arg.size(); i++)
      s[i] = val2char(arg[i]);
    return s;
  }
	
	struct str_literal {
		Gecode::IntArgs str;
		
		str_literal& operator=(std::string s) {
			str = str2IntArgs(s);
			return *this;
		}
	};
	
	struct int_range {
		int min;
		int max;
	};
	
	struct int_array_def {
		int_range range;
		std::string name;
		std::vector<int> values;
	};
	std::map<std::string, int_array_def> int_array_map;
	
	struct str_var_def {
		boost::optional<int> size;
		std::string name;
	};
	std::vector<str_var_def> str_var_table;
	std::map<std::string, int> str_var_index;
	
	
	struct int_param_def {
		std::string name;
		int value;
	};
	std::map<std::string, int> int_param_value;
	
	struct int_var_def {
		boost::optional<int_range> range;
		std::string name;
	};
	std::vector<int_var_def> int_var_table;
	std::map<std::string, int> int_var_index;
	
	typedef
		boost::variant<
			str_var_def,
			int_var_def
		> var_type;
	
	struct var_def {
		var_type var;
		std::vector<std::string> annotations;
	};
	

	
///	arguments that can have more than one type:
	typedef
		boost::variant<
			str_literal,
			std::string
		> var_string_arg;

	typedef
		boost::variant<
			std::string,
			int
		> var_int_arg;
			
	typedef
		boost::variant<
			std::string,
			std::vector<int>
		> int_array_arg;
			
	typedef
		boost::variant<
			int_range,
			std::vector<int>
		> int_set_arg;
			
	// constraint predicates:
	struct str_dfa_cstr {
		std::string var;
		int maxstate;
		std::vector<char> alphabet;
		int_array_arg transitions;
		int start;
		int_set_arg final;
	};
	
	struct str_range_cstr {
		std::string var;
		char min;
		char max;
	};
	
	struct str_eq_cstr {
		std::string var_1;
		std::string var_2;
	};
	
	struct str_dom_cstr {
		std::string var;
		std::vector<char> dom;
	};
	
	struct str_len_cstr {
		std::string strvar;
		var_int_arg lenvar;
	};
	
	struct str_concat_cstr {
		std::string var_1;
		std::string var_2;
		std::string var_3;
	};
	
	struct str_pow_cstr {
		std::string strarg;
		var_int_arg intvar;
		std::string strvar;
	};
	
	struct int_le_cstr {
		var_int_arg var_1;
		var_int_arg var_2;
	};
	
	struct int_lt_cstr {
		var_int_arg var_1;
		var_int_arg var_2;
	};
	
	struct int_lin_eq_cstr {
		int_array_arg a;
		std::vector<std::string> b;
		int c;
	};
	
	struct int_lin_le_cstr {
		int_array_arg a;
		std::vector<std::string> b;
		int c;
	};
	
	typedef
		boost::variant<
			str_dfa_cstr,
			str_len_cstr,
			str_dom_cstr,
			str_range_cstr,
			str_eq_cstr,
			str_concat_cstr,
			str_pow_cstr,
			int_le_cstr,
			int_lt_cstr,
			int_lin_le_cstr,
			int_lin_eq_cstr
		> cstr_type;
			
	struct predicate_def {
		cstr_type cstr;
		std::vector<std::string> annotations;
	};
	std::vector<predicate_def> cstr_table;
	
	struct solve_stmt {
		std::string goal;
		std::vector<std::string> annotations;
	};
	
	typedef 
		boost::variant< 
			int_param_def,
			int_array_def,
			var_def,
			predicate_def,
			solve_stmt
		> statement;
}

BOOST_FUSION_ADAPT_STRUCT(
	parser::str_literal,
	(Gecode::IntArgs, str)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::int_range,
	(int, min)
	(int, max)
)

BOOST_FUSION_ADAPT_STRUCT(
	parser::int_array_def,
	(parser::int_range, range)
	(std::string, name)
	(std::vector<int>, values)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::str_var_def,
	(boost::optional<int>, size)
	(std::string, name)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::int_var_def,
	(boost::optional< parser::int_range >, range)
	(std::string, name)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::var_def,
	(parser::var_type, var)
	(std::vector<std::string>, annotations)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::int_param_def,
	(std::string, name)
	(int, value)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::solve_stmt,
	(std::string, goal)
	(std::vector<std::string>, annotations)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::str_dfa_cstr, 
	(std::string, var)
	(int, maxstate)
	(std::vector<char>, alphabet)
	(parser::int_array_arg, transitions)
	(int, start)
	(parser::int_set_arg, final)
)
		
BOOST_FUSION_ADAPT_STRUCT(
		parser::str_range_cstr,
		(std::string, var)
		(char, min)
		(char, max)
)
	
BOOST_FUSION_ADAPT_STRUCT(
	parser::str_eq_cstr, 
		(std::string, var_1)
		(std::string, var_2)
)
	
BOOST_FUSION_ADAPT_STRUCT(
			parser::str_dom_cstr,
		(std::string, var)
		(std::vector<char>, dom)
)
	
BOOST_FUSION_ADAPT_STRUCT(
	parser::str_len_cstr, 
		(std::string, strvar)
		(parser::var_int_arg, lenvar)
)

BOOST_FUSION_ADAPT_STRUCT(
	parser::str_concat_cstr, 
	(std::string, var_1)
	(std::string, var_2)
	(std::string, var_3)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::str_pow_cstr, 
		(std::string, strarg)
		(parser::var_int_arg, intvar)
		(std::string, strvar)
)
				
BOOST_FUSION_ADAPT_STRUCT(
	parser::int_le_cstr,
	(parser::var_int_arg, var_1)
	(parser::var_int_arg, var_2)
)
	
BOOST_FUSION_ADAPT_STRUCT(
	parser::int_lt_cstr,
	(parser::var_int_arg, var_1)
	(parser::var_int_arg, var_2)
)

BOOST_FUSION_ADAPT_STRUCT(
	parser::int_lin_eq_cstr,
	(parser::int_array_arg, a)
	(std::vector<std::string>, b)
	(int, c)
)
	
BOOST_FUSION_ADAPT_STRUCT(
	parser::int_lin_le_cstr,
	(parser::int_array_arg, a)
	(std::vector<std::string>, b)
	(int, c)
)
		
BOOST_FUSION_ADAPT_STRUCT(
	parser::predicate_def,
	(parser::cstr_type, cstr)
	(std::vector<std::string>, annotations)
)
		
namespace parser {
	

		class var_name_visitor : public boost::static_visitor<std::string> {
		public:
			template <typename T>
				std::string operator()(T const& var) const {
					return var.name;
				}
		};
		
		void push_str_var(str_var_def const& var) {
			str_var_index.insert(std::make_pair(var.name, str_var_table.size()));
			str_var_table.push_back(var);
			type_map.insert(std::make_pair(var.name, "string"));
		}
		
		void push_int_var(int_var_def const& var) {
			int_var_index.insert(std::make_pair(var.name, int_var_table.size()));
			int_var_table.push_back(var);
			type_map.insert(std::make_pair(var.name, "int"));
		}
		
		void push_int_array(int_array_def const& array) {
			int_array_map.insert(std::make_pair(array.name, array));
			type_map.insert(std::make_pair(array.name, "array"));
		}
		
		void push_int_param(int_param_def const& param) {
			int_param_value.insert(std::make_pair(param.name, param.value));
		}
		
		void push_cstr(predicate_def const& cstr) {
			cstr_table.push_back(cstr);
		}
		
		void push_annotations(var_def const& var) {
			std::string name = boost::apply_visitor(var_name_visitor(), var.var);
			var_annotations.insert(std::make_pair(name, var.annotations));
		}
		
	template <typename Iterator>
	struct fzn_parser : qi::grammar<Iterator, std::vector<statement>(), ascii::space_type> {
		fzn_parser() : fzn_parser::base_type(fzn, "fzn") {
			using ascii::char_;
			using qi::lit;
			using qi::eol;
			using qi::eoi;
			using qi::eps;
			using qi::lexeme;
			using qi::int_;
			using ascii::alnum;
			using ascii::alpha;
			using ascii::punct;
			using qi::on_error;
			using qi::fail;
			using qi::_val;
			using phoenix::at_c;
			//using namespace qi::labels;
			
			using phoenix::construct;
			using phoenix::val;
			
			id %= lexeme[ alpha >> *(alnum | char_('_')) ];
			id.name("id");
			
			charlit %= lexeme[lit('"') >> -(char_ - '"') >> lit('"')];
			charlit.name("char lit");
			
			strlit = lexeme[char_('"') >> *(char_ - '"') >> char_('"')];
			strlit.name("str lit");
			
			intrange %= int_ >> lit("..") >> int_;
			intrange.name("int range");
			
			charset %= lit('{') > (charlit % lit(',')) > lit('}');
			charset.name("char set");
			
			intset %= lit('{') > (int_ % lit(',')) > lit('}');
			charset.name("int set");
			
			idarray %= lit('[') > (id % ',') > lit(']');
			idarray.name("id array");
			
			intarray %= 
				lit('[')
				>> (int_ % lit(','))
				>> lit(']');
			intarray.name("int array literal");
			
			annotations %= *(lit("::") >> 
											lexeme[ 
													+(	alnum | char_('_')) >>
													-(
															char_('(') >> 
															+( alnum | char_('_')) >> 
															char_(')')
													 )
											] 
										);
			annotations.name("annotations");
			
			intarraydef %= lit("array") >> 
					'[' >> intrange >> lit(']') >>
					lit("of") >> lit("int") > ':' > 
					id >
					lit('=') > 
					intarray;
			intarraydef.name("int array definition");
			
			intparamdef %= lit("int") >>
				':' >
				id > 
				'=' >
				int_;
			intparamdef.name("int param definition");
			
			strvardecl %= lit("string") > 
					-( lit('(') > int_ > lit(')') ) >
					lit(':') > id;
			strvardecl.name("string var decl");
			
			intvardecl %= ( intrange | lit("int") ) > 
					lit(':') > id ;
			intvardecl.name("int var decl");
			
			vardecl %= lit("var") > 
					(		strvardecl [ &push_str_var ]
						| intvardecl [ &push_int_var ]
					) > annotations;
			vardecl.name("var decl");
			
			varstringarg %=  id | strlit;
			varstringarg.name("var string argument");
//
// 			intliteral %= int_ [_val = _1];
// 			intparameter %= id  [_val = _1];
// 			intarg %= intliteral | intparameter;
// 			intarg.name("int argument");
//
			varintarg %= int_ | id;
			varintarg.name("var int argument");
			
			intsetarg %= intset | intrange;
			intsetarg.name("int set argument");
			
			intarrayarg %= intarray | id;
				
			str_dfa %= lit("str_dfa") 
				> lit('(')		> id
					> lit(',')	> int_
					> lit(',')	> charset
					> lit(',')	> intarrayarg
					> lit(',')	> int_					
					> lit(',')	> intsetarg
				>	 lit(')');
			str_dfa.name("str_dfa");
			
			str_len %= lit("str_len")
				> lit('(')		> id
					> lit(',')	> varintarg
				> lit(')');
			str_len.name("str_len");
			
			str_dom %= lit("str_dom")
				> lit('(')		> id
					> lit(',')	> charset
				> lit(')');
			str_dom.name("str_dom");
			
			str_eq %= lit("str_eq")
				> lit('(')		> varstringarg
					> lit(',') 	> varstringarg
				> lit(')');
			str_eq.name("str_eq");
			
			str_concat %= lit("str_concat")
				> lit('(')		> varstringarg
					> lit(',') 	> varstringarg
					> lit(',') 	> varstringarg
				> lit(')');
			str_concat.name("str_concat");
			
			str_range %= lit("str_range")
				> lit('(')		> id
					> lit(',')	> charlit
					> lit(',')	> charlit
				> lit(')');
			str_range.name("str_range");
			
			str_pow %= lit("str_pow")
					> lit ('(')		> strlit
					> lit (',')	> varintarg
					> lit (',')	> varstringarg
				> lit (')');
			str_pow.name("str_pow");
			
			int_le %= lit("int_le")
				> lit('(')		> varintarg
					> lit(',') 	> varintarg
				> lit(')');
			int_le.name("int_le");
			
			int_lt %= lit("int_lt")
				> lit('(')		> varintarg
					> lit(',') 	> varintarg
				> lit(')');
			int_lt.name("int_lt");
			
			int_lin_le %= lit("int_lin_le")
				> lit('(')		> intarrayarg
					> lit(',')	> idarray
					> lit(',')	> int_
				> lit(')');
			int_lin_le.name("int_lin_le");
			
			int_lin_eq %= lit("int_lin_eq")
				> lit('(')		> intarrayarg
					> lit(',')	> idarray
					> lit(',')	> int_
				> lit(')');
			int_lin_eq.name("int_lin_eq");
			
			constraint %= lit("constraint") 
				>	(	str_dfa
					|	str_len
					|	str_concat
					| str_eq
					| str_dom
					| str_range	
					| str_pow
					| int_le
					| int_lt
					| int_lin_le
					| int_lin_eq
						)
				>>	annotations;
			constraint.name("constraint");

			solve %= lit("solve") > id > annotations;
			solve.name("solve");
			
			stmt %= intparamdef	[ &push_int_param ]
						| intarraydef	[ &push_int_array ]
						| vardecl			[ &push_annotations ]
						| constraint	[ &push_cstr ]
						| solve;
			stmt.name("stmt");
			
			fzn %= stmt % ';' >> ';';
			fzn.name("fzn");
			
			on_error<fail>
			(
					fzn
				, std::cout
							<< val("Error! Expecting ")
							<< qi::labels::_4																// what failed?
							<< val(" here: \"")
							<< construct<std::string>(qi::labels::_3, qi::labels::_2)		// iterators to error-pos, end
							<< val("\"")
							<< std::endl
			);
		}
	
		// simple arguments
		qi::rule<Iterator, std::string()> id;
		qi::rule<Iterator, std::vector<std::string>(), ascii::space_type> idarray;
		qi::rule<Iterator, char()> charlit;
		qi::rule<Iterator, std::string()> strlit;
		qi::rule<Iterator, int_range()> intrange;
		qi::rule<Iterator, std::vector<char>(), ascii::space_type> charset;
		qi::rule<Iterator, std::vector<int>(), ascii::space_type> intset;
		qi::rule<Iterator, std::vector<int>(), ascii::space_type> intarray;
		
		// compound arguments
		qi::rule<Iterator, std::string(), ascii::space_type> varstringarg;
		//qi::rule<Iterator, int_arg(), ascii::space_type> intliteral;
		//qi::rule<Iterator, int_arg(), ascii::space_type> intparameter;
		//qi::rule<Iterator, int_arg(), ascii::space_type> intarg;
		qi::rule<Iterator, var_int_arg(), ascii::space_type> varintarg;
		qi::rule<Iterator, int_set_arg(), ascii::space_type> intsetarg;
		qi::rule<Iterator, int_array_arg(), ascii::space_type> intarrayarg;
		
		qi::rule<Iterator, std::vector<std::string>(), ascii::space_type> annotations;
		
		// variable declarations
		qi::rule<Iterator, str_var_def(), ascii::space_type> strvardecl;
		qi::rule<Iterator, int_var_def(), ascii::space_type> intvardecl;
		qi::rule<Iterator, var_def(), ascii::space_type> vardecl;
		
		// constraint predicates
		qi::rule<Iterator, str_dfa_cstr(), ascii::space_type> str_dfa;
		qi::rule<Iterator, str_dom_cstr(), ascii::space_type> str_dom;
		qi::rule<Iterator, str_len_cstr(), ascii::space_type> str_len;
		qi::rule<Iterator, str_range_cstr(), ascii::space_type> str_range;
		qi::rule<Iterator, str_concat_cstr(), ascii::space_type> str_concat;
		qi::rule<Iterator, str_eq_cstr(), ascii::space_type> str_eq;
		qi::rule<Iterator, str_pow_cstr(), ascii::space_type> str_pow;
		qi::rule<Iterator, int_le_cstr(), ascii::space_type> int_le;
		qi::rule<Iterator, int_lt_cstr(), ascii::space_type> int_lt;
		qi::rule<Iterator, int_lin_le_cstr(), ascii::space_type> int_lin_le;
		qi::rule<Iterator, int_lin_eq_cstr(), ascii::space_type> int_lin_eq;
		
		qi::rule<Iterator, int_param_def(), ascii::space_type> intparamdef;
		qi::rule<Iterator, int_array_def(), ascii::space_type> intarraydef;
		qi::rule<Iterator, predicate_def(), ascii::space_type> constraint;
		qi::rule<Iterator, solve_stmt(), ascii::space_type> solve;
		qi::rule<Iterator, statement(), ascii::space_type> stmt;
		qi::rule<Iterator, std::vector<statement>(), ascii::space_type> fzn;
		
	};
}

void printIntArgs (const Gecode::IntArgs& arg) {
	std::cout << "IntArgs(";
	std::cout << arg.size();
	for (int i = 0; i < arg.size(); i++) 
		std::cout << ", " << arg[i];
	std::cout << ")";
}

class FznInstance : public FznModel {

public:
  
  int getIntVar(std::string name) {
  	return parser::int_var_index.find(name)->second;
  }

  int getStrVar(std::string name) {
  	return parser::str_var_index.find(name)->second;
  }
	
	FznInstance(const FznOptions& opt);
	
	FznInstance(bool share, FznInstance& s)
	: FznModel(share,s) {
    x.update(*this, share, s.x);
    l.update(*this, share, s.l);
	}
  
	virtual Space* copy(bool share) {
		return new FznInstance(share,*this);
	}

  virtual void print(std::ostream& os) const {
		for(std::vector<std::string>::size_type i = 0; 
				i < parser::str_var_table.size(); 
				++i) {
			parser::str_var_def var = parser::str_var_table[i];
			std::vector<std::string> annotations = parser::var_annotations.find(var.name)->second;
			for (std::vector<std::string>::size_type j = 0; 
					j < annotations.size(); 
					++j) {
						if (annotations[j] == "output_var") {
							os << var.name << ": \"";
							int ind = parser::str_var_index.find(var.name)->second;
							for (int k = 0; k < x[ind].size(); k++)
								os << val2char(x[ind].charval(k));
							os	 << '"' << std::endl;
						}
			}
		}
		for(std::vector<std::string>::size_type i = 0; 
				i < parser::int_var_table.size(); 
				++i) {
			parser::int_var_def var = parser::int_var_table[i];
			std::vector<std::string> annotations = parser::var_annotations.find(var.name)->second;
			for (std::vector<std::string>::size_type j = 0; 
					j < annotations.size(); 
					++j) {
						if (annotations[j] == "output_var") {
							os << var.name << ": " 
								 << l[parser::int_var_index.find(var.name)->second] 
								 << std::endl;
						}
			}
		}
    os << "----------------------------" << std::endl;
  }
};

class int_le_visitor: public boost::static_visitor<> {
public:
	FznInstance& model;
	bool verbose;
	
	int_le_visitor(FznInstance& m, bool v): model(m), verbose(v) {}
	
	void operator()(std::string& v1, std::string& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " <= " << v2 << ");\n";
		Gecode::rel(model, model.l[model.getIntVar(v1)] <= model.l[model.getIntVar(v2)]);
	}
	
	void operator()(int& v1, std::string& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " <= " << v2 << ");\n";
		Gecode::rel(model, v1 <= model.l[model.getIntVar(v2)]);
	}
	
	void operator()(std::string& v1, int& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " <= " << v2 << ");\n";
		Gecode::rel(model, model.l[model.getIntVar(v1)] <= v2);
	}
	
	template<typename V1, typename V2>
	void operator()(V1& v1, V2& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " <= " << v2 << ");\n";
		std::cout << "unhandled int_le constraint" << std::endl;
	}
};

class int_lt_visitor: public boost::static_visitor<> {
public:
	FznInstance& model;
	bool verbose;
	
	int_lt_visitor(FznInstance& m, bool v): model(m), verbose(v) {}
	
	void operator()(std::string& v1, std::string& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " < " << v2 << ");\n";
		Gecode::rel(model, model.l[model.getIntVar(v1)] < model.l[model.getIntVar(v2)]);
	}
	
	void operator()(int& v1, std::string& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " < " << v2 << ");\n";
		Gecode::rel(model, v1 < model.l[model.getIntVar(v2)]);
	}
	
	void operator()(std::string& v1, int& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " < " << v2 << ");\n";
		Gecode::rel(model, model.l[model.getIntVar(v1)] < v2);
	}
	
	template<typename V1, typename V2>
	void operator()(V1& v1, V2& v2) const {
		if(verbose)
			std::cout << "rel(*this, " << v1 << " < " << v2 << ");\n";
		std::cout << "unhandled int_lt constraint" << std::endl;
	}
};

class int_lin_le_visitor: public boost::static_visitor<> {
public:
	FznInstance& model;
	std::vector<std::string> b;
	int c;
	bool verbose;
	
	int_lin_le_visitor(FznInstance& m, std::vector<std::string> b, int c, bool v)
		: model(m), b(b), c(c), verbose(v) {}
	
	void operator()(std::string& _a) const {
		parser::int_array_def def = parser::int_array_map.find(_a)->second;
		Gecode::IntArgs a(def.values);
		if(verbose) {
			std::cout << "linear(*this, ";
			printIntArgs(a);
			std::cout << ", IntVarArgs()";
		}
		Gecode::IntVarArgs x;
		for (std::vector<std::string>::size_type i = 0; i < b.size(); i++) {
			x << model.l[model.getIntVar(b[i])];
			if(verbose)
				std::cout << "<<" << b[i];
		}
		if(verbose) 
			std::cout << ", IRT_LQ, " << c << ");\n";
		Gecode::linear(model, a, x, Gecode::IRT_LQ, c);
	}

	void operator()(std::vector<int>& _a) const {
		Gecode::IntArgs a(_a);
		if(verbose) {
			std::cout << "linear(*this, ";
			printIntArgs(a);
			std::cout << ", IntVarArgs()";
		}
		Gecode::IntVarArgs x;
		for (std::vector<std::string>::size_type i = 0; i < b.size(); i++) {
			x << model.l[model.getIntVar(b[i])];
			if(verbose)
				std::cout << "<<" << b[i];
		}
		if(verbose) 
			std::cout << ", IRT_LQ, " << c << ");\n";
		Gecode::linear(model, a, x, Gecode::IRT_LQ, c);
	}
};


class int_lin_eq_visitor: public boost::static_visitor<> {
public:
	FznInstance& model;
	std::vector<std::string> b;
	int c;
	bool verbose;
	
	int_lin_eq_visitor(FznInstance& m, std::vector<std::string> b, int c, bool v)
		: model(m), b(b), c(c), verbose(v) {}
	
	void operator()(std::string& _a) const {
		parser::int_array_def def = parser::int_array_map.find(_a)->second;
		Gecode::IntArgs a(def.values);
		if(verbose) {
			std::cout << "linear(*this, ";
			printIntArgs(a);
			std::cout << ", IntVarArgs()" ;
		}
		Gecode::IntVarArgs x;
		for (std::vector<std::string>::size_type i = 0; i < b.size(); i++) {
			x << model.l[model.getIntVar(b[i])];
			if(verbose)
				std::cout << "<<" << b[i];
		}
		if(verbose) {
			std::cout << ", IRT_EQ, " << c << ");\n";
		}
		Gecode::linear(model, a, x, Gecode::IRT_EQ, c);
	}

	void operator()(std::vector<int>& _a) const {
		Gecode::IntArgs a(_a);
		if(verbose) {
			std::cout << "linear(*this, ";
			printIntArgs(a);
			std::cout << ", IntVarArgs()" ;
		}
		Gecode::IntVarArgs x;
		for (std::vector<std::string>::size_type i = 0; i < b.size(); i++) {
			x << model.l[model.getIntVar(b[i])];
			if(verbose)
				std::cout << "<<" << b[i];
		}
		if(verbose) 
			std::cout << ", IRT_EQ, " << c << ");\n";
		Gecode::linear(model, a, x, Gecode::IRT_EQ, c);
	}
};

class str_len_visitor: public boost::static_visitor<> {
public:
	FznInstance& model;
	std::string strvar;
	bool verbose;

	str_len_visitor(FznInstance& m, std::string s, bool v): model(m), strvar(s), verbose(v) {}

	void operator()(std::string& length) const {
		if(verbose)
			std::cout << "length(*this, " << strvar << ", " << length << ");\n";
		Gecode::length(model, model.x[model.getStrVar(strvar)], model.l[model.getIntVar(length)]);
	}
	void operator()(int& length) const {
		if(verbose)
			std::cout << "length(*this, " << strvar << ", " << length << ");\n";
		Gecode::length(model, model.x[model.getStrVar(strvar)], length);
	}
};

class str_pow_visitor: public boost::static_visitor<> {
public:
	FznInstance& model;
	std::string strarg;
	std::string strvar;
	bool verbose;

	str_pow_visitor(FznInstance& m, std::string a, std::string s, bool v): model(m), strarg(a), strvar(s), verbose(v) {}

	void operator()(std::string& intvar) const {
		if (strarg[0] == '"') {
			Gecode::IntArgs const_str = model.str2IntArgs(strarg.substr(1,strarg.length()-2));
			if (const_str.size() == 1) {
				if(verbose) {
					std::cout << "alphabet(*this, " << strvar << ", " << const_str[0] << ", " << const_str[0] << ");" << std::endl;
					std::cout << "length(*this, " << strvar << ", " << intvar << ");" << std::endl;
				}
				Gecode::alphabet(model, model.x[model.getStrVar(strvar)], const_str[0], const_str[0]);
				Gecode::length(model, model.x[model.getStrVar(strvar)], model.l[model.getIntVar(intvar)]);
			} else {
				Gecode::REG r;
				for (int i = 0; i < const_str.size(); i++) {
					r += Gecode::REG(const_str[i]);
				}
				Gecode::DFA dfa(*r);
				Gecode::extensional(model, model.x[model.getStrVar(strvar)], dfa);
				// FIXME: must introduce a temp int var here, and do a rel to the length
				//Gecode::length(model, model.x[model.getStrVar(strvar)], model.l[model.getIntVar(length)]);
			}
		} else {
			// FIXME: can't rewrite this case, need a str_pow propagator
		}
		
	}
	void operator()(int& pow) const {
		std::string unrolled = strarg.substr(1,strarg.length()-2);
		for (int i = 0; i < pow - 1; i++) {
			unrolled.append(unrolled);
		}
		if(verbose)
			std::cout << "equal(*this, " << strvar << ", '" << unrolled << "');\n";
		Gecode::equal(model, model.x[model.getStrVar(strvar)], model.str2IntArgs(unrolled));
	}
};

// class str_eq_visitor: public boost::static_visitor<> {
// public:
// 	FznInstance& model;
//
// 	str_eq_visitor(FznInstance& m): model(m) {}
//
// 	void operator()(parser::var_str_lookup& a, parser::var_str_lookup& b) const {
// 		Gecode::equal(model, model.getStrVar(a.name), model.getStrVar(b.name));
// 	}
//
// 	void operator()(parser::var_str_lookup& a, std::string& b) const {
// 		Gecode::equal(model, model.getStrVar(a.name), model.str2IntArgs(b));
// 	}
//
// 	void operator()(std::string& a, parser::var_str_lookup& b) const {
// 		Gecode::equal(model, model.str2IntArgs(a), model.getStrVar(b.name));
// 	}
//
// 	template <typename A, typename B>
// 	void operator()(A& a, B& b) const {
// 		std::cout << "unhandled str_len constraint" << std::endl;
// 	}
// };

// class str_concat_visitor: public boost::static_visitor<> {
// public:
// 	FznInstance& model;
//
// 	str_concat_visitor(FznInstance& m): model(m) {}
//
// 	void operator()(std::string& a, std::string& b, std::string& c) const {
// 		Gecode::concat(model, model.getStrVar(a), model.getStrVar(b), model.getStrVar(c));
// 	}
//
// 	void operator()(std::string& a, std::string& b, parser::str_literal& c) const {
// 		Gecode::concat(model, model.getStrVar(a), model.getStrVar(b), c.str);
// 	}
//
// 	void operator()(std::string& a, parser::str_literal& b, std::string& c) const {
// 		Gecode::concat(model, model.getStrVar(a), b.str, model.getStrVar(c));
// 	}
//
// 	void operator()(parser::str_literal a, std::string& b, std::string& c) const {
// 		Gecode::concat(model, a.str, model.getStrVar(b) , model.getStrVar(c));
// 	}
//
// 	template <typename A, typename B, typename C>
// 	void operator()(A& a, B& b, C& c) const {
// 		std::cout << "unhandled str_concat constraint" << std::endl;
// 	}
// };

class int_array_arg_visitor: public boost::static_visitor<Gecode::IntArgs> {
public:
	Gecode::IntArgs operator()(std::vector<int>& array) const {
		Gecode::IntArgs a(array);
		return a;
	}
	Gecode::IntArgs operator()(std::string& name) const {
		parser::int_array_def def = parser::int_array_map.find(name)->second;
		std::vector<int> array = def.values;
		Gecode::IntArgs a(array);
		return a;
	}
};

class int_set_visitor: public boost::static_visitor< std::vector<int> > {
public:
	std::vector<int> operator()(std::vector<int>& set) const {
		return set;
	}
	std::vector<int> operator()(parser::int_range& range) const {
		std::vector<int> set;
		for (int i = range.min; i <= range.max; i++) {
			set.push_back(i);
		}
		return set;
	}
};

class constraint_visitor: public boost::static_visitor<> {
public:
	FznInstance& model;
	bool verbose;
	
  constraint_visitor(FznInstance& m, bool v): model(m), verbose(v) {}
	
	void operator()(parser::str_dfa_cstr& cstr) const {
		Gecode::StringVar var = model.x[model.getStrVar(cstr.var)];
		Gecode::IntArgs delta = boost::apply_visitor(int_array_arg_visitor(), cstr.transitions);
		//std::cout << "delta array:" << delta << std::endl;
		
		// |transitions| = |states| * |symbols|;
    int noOfTrans = 0;
		int symbols = cstr.alphabet.size();
    for (int i=1; i<=cstr.maxstate; i++) {
      for (int j=1; j<=symbols; j++) {
        if (delta[(i-1)*symbols+(j-1)] > 0)
          noOfTrans++;
      }
    }
		
		Gecode::DFA::Transition t[noOfTrans+1]; 
		noOfTrans = 0;
		std::vector<char> alpha = cstr.alphabet;
		if(verbose) {
			for (int i = 0; i < symbols; i++)
				std::cout << alpha[i] << ":" << model.char2val(alpha[i]) << "\t";
			std::cout << std::endl;
		}
		for (int i=1; i<=cstr.maxstate; i++) {
			for (int j=0; j<symbols; j++) {
				if (delta[(i-1)*symbols+j] > 0) {
					int symbol = model.char2val(alpha[j]);
					int o_state = delta[(i-1)*symbols+j];
					if(verbose) {
						std::cout << "[" << i << "]- " << symbol << " >[" << o_state << "]\n";
					}
					t[noOfTrans] = Gecode::DFA::Transition(i, symbol, o_state);
					noOfTrans++;
				}
			}
		}
		t[noOfTrans] = Gecode::DFA::Transition(-1,0,0);
		
		std::vector<int> _f = boost::apply_visitor(int_set_visitor(), cstr.final);
		int num_finals = _f.size();
		int f[num_finals + 1];
		for (int i = 0; i < num_finals; i++) {
			f[i] = _f[i];
		}
		f[num_finals] = -1;
		
		if (verbose) {
			for (int i = 0; i < num_finals; i++)
				std::cout << f[i] << "\t";
			std::cout << std::endl;
		}
		
		Gecode::DFA dfa(cstr.start,t,f);
		if (verbose) {
			std::cout << "extensional(*this, " << cstr.var << " x[" << model.getStrVar(cstr.var) << "], " << dfa << ");\n";
		}
		Gecode::extensional(model, var, dfa);
	}
	
	void operator()(parser::str_range_cstr& cstr) const {
		if(verbose) {
			std::cout << "alphabet(*this, " << cstr.var << ", " << model.char2val(cstr.min) << ", " << model.char2val(cstr.max) << ");";
			std::cout << "\t// x[" << model.getStrVar(cstr.var) << "] in " << model.char2val(cstr.min) << ".." << model.char2val(cstr.max) << std::endl;
		}
		Gecode::alphabet(model, model.x[model.getStrVar(cstr.var)], model.char2val(cstr.min), model.char2val(cstr.max));
	}
	
	void operator()(parser::str_dom_cstr& cstr) const {
		Gecode::IntArgs dom_ = model.chars2IntArgs(cstr.dom);
		Gecode::IntSet dom(dom_);
		if(verbose) {
			std::cout << "alphabet(*this," << cstr.var << ", " << dom << ");";
			std::cout << "\t// x[" << model.getStrVar(cstr.var) << "] in " << dom << std::endl;
		}
		Gecode::alphabet(model, model.x[model.getStrVar(cstr.var)], dom);
	}
	
	void operator()(parser::str_concat_cstr& cstr) const {
		//boost::apply_visitor(str_concat_visitor(model), cstr.var_1, cstr.var_2, cstr.var_3);
		if (cstr.var_1[0] == '"') {
			Gecode::IntArgs const_str = model.str2IntArgs(cstr.var_1.substr(1,cstr.var_1.length()-2));
			if(verbose) {
				std::cout << "concat(*this, ";
				printIntArgs(const_str);
				std::cout << ", " 
					<< cstr.var_2 << ", " 
					<< cstr.var_3 << ");";
				std::cout << "\t//" 
					<< "\"" << const_str << "\" . " 
					<< " x[" << model.getStrVar(cstr.var_2) << "] = " 
					<< " x[" << model.getStrVar(cstr.var_3) << "]\n";
			}
			Gecode::concat(model, 
				const_str, 
				model.x[model.getStrVar(cstr.var_2)], 
				model.x[model.getStrVar(cstr.var_3)]);
		} else if (cstr.var_2[0] == '"') {
			Gecode::IntArgs const_str = model.str2IntArgs(cstr.var_2.substr(1,cstr.var_2.length()-2));
			if(verbose) {
				std::cout << "concat(*this, " 
					<< cstr.var_1 << ", ";
				printIntArgs(const_str);
				std::cout << ", " 
					<< cstr.var_3 << ");";
				std::cout << "\t//" 
					<< " x[" << model.getStrVar(cstr.var_1) << "] . " 
					<< "\"" << const_str << "\" = " 
					<< " x[" << model.getStrVar(cstr.var_3) << "]\n";
			}
			Gecode::concat(model, 
				model.x[model.getStrVar(cstr.var_1)],
				const_str, 
				model.x[model.getStrVar(cstr.var_3)]);
		} else if (cstr.var_3[0] == '"') {
			Gecode::IntArgs const_str = model.str2IntArgs(cstr.var_3.substr(1,cstr.var_3.length()-2));
			if(verbose) {
				std::cout << "concat(*this, " 
					<< cstr.var_1 << ", " 
					<< cstr.var_2 << ", " ;
				printIntArgs(const_str);
				std::cout << ");";
				std::cout << "\t//" 
					<< " x[" << model.getStrVar(cstr.var_1) << "] . " 
					<< " x[" << model.getStrVar(cstr.var_2) << "] = " 
					<< " \"" << const_str << "\"\n";
			}
			Gecode::concat(model, 
				model.x[model.getStrVar(cstr.var_1)], 
				model.x[model.getStrVar(cstr.var_2)], 
				const_str);
		} else {
			if(verbose) {
				std::cout << "concat(*this, " 
					<< cstr.var_1 << ", " 
					<< cstr.var_2 << ", " 
					<< cstr.var_3 << ");";
				std::cout << "\t//" 
					<< " x[" << model.getStrVar(cstr.var_1) << "] . " 
					<< " x[" << model.getStrVar(cstr.var_2) << "] = " 
					<< " x[" << model.getStrVar(cstr.var_3) << "]\n";
			}
			Gecode::concat(model, 
				model.x[model.getStrVar(cstr.var_1)],
				model.x[model.getStrVar(cstr.var_2)],
				model.x[model.getStrVar(cstr.var_3)]);
		}
	}
	
	void operator()(parser::str_eq_cstr& cstr) const {
		//boost::apply_visitor(str_eq_visitor(model), cstr.var_1, cstr.var_2);
		if(verbose) {
			std::cout << "equal(*this, " << cstr.var_1 << ", " << cstr.var_2 << ");";
			std::cout << "\t// x[" << model.getStrVar(cstr.var_1) << "] = x[" << model.getStrVar(cstr.var_2) << "]\n";
		}
		if (cstr.var_1[0] == '"') {
			Gecode::IntArgs const_str = model.str2IntArgs(cstr.var_1.substr(1,cstr.var_1.length()-2));
			Gecode::equal(model, 
				const_str, 
				model.x[model.getStrVar(cstr.var_2)]);
		} else if (cstr.var_2[0] == '"') {
			Gecode::IntArgs const_str = model.str2IntArgs(cstr.var_2.substr(1,cstr.var_2.length()-2));
			Gecode::equal(model, 
				model.x[model.getStrVar(cstr.var_1)], 
				const_str);
		} else {
			Gecode::equal(model, 
				model.x[model.getStrVar(cstr.var_1)], 
				model.x[model.getStrVar(cstr.var_2)]);
		}
	}
	
	void operator()(parser::str_len_cstr& cstr) const {
		boost::apply_visitor(str_len_visitor(model, cstr.strvar, verbose), cstr.lenvar);
	}
	
	void operator()(parser::str_pow_cstr& cstr) const {
		boost::apply_visitor(str_pow_visitor(model, cstr.strarg, cstr.strvar, verbose), cstr.intvar);
	}
	
	void operator()(parser::int_le_cstr& cstr) const {
		boost::apply_visitor(int_le_visitor(model, verbose), cstr.var_1, cstr.var_2);
	}
	
	void operator()(parser::int_lt_cstr& cstr) const {
		boost::apply_visitor(int_lt_visitor(model, verbose), cstr.var_1, cstr.var_2);
	}
	
	void operator()(parser::int_lin_eq_cstr& cstr) const {
		boost::apply_visitor(int_lin_eq_visitor(model, cstr.b, cstr.c, verbose), cstr.a);
	}
	
	void operator()(parser::int_lin_le_cstr& cstr) const {
		boost::apply_visitor(int_lin_le_visitor(model, cstr.b, cstr.c, verbose), cstr.a);
	}
	
	template <typename Cstr>
	void operator()(Cstr& cstr) const {
		std::cout << "unrecognized constraint" << std::endl;
	}
};

	
FznInstance::FznInstance(const FznOptions& opt)
: FznModel(opt) {    
	using namespace Gecode;
	// String Variables:
	int longest = 0;
	for(std::vector<std::string>::size_type i = 0; i < parser::str_var_table.size(); ++i) {
		parser::str_var_def var = parser::str_var_table[i];
		if(var.size)
			longest = std::max(longest, *(var.size));
	}
	longest *= 2; // long enough to hold results of concatenations
	StringVarArgs _x;
	if(opt.verbose())
		std::cout << "StringVarArgs _x;\n";
	for(std::vector<std::string>::size_type i = 0; 
			i < parser::str_var_table.size(); 
			++i) 
	{
		parser::str_var_def var = parser::str_var_table[i];
		
		// output variables should not be the empty string
		int minLength = opt.minlength();
		// if (minLength < 1)
		// {
		// 	std::vector<std::string> annotations =
		// 		parser::var_annotations.find(var.name)->second;
		//
		// 	for (std::vector<std::string>::size_type j = 0;
		// 			j < annotations.size();
		// 			++j)
		// 	{
		// 				if (annotations[j] == "output_var")
		// 					minLength = 1;
		// 	}
		// }
		
		if(opt.verbose()) 
		{
			std::cout << "StringVar " << var.name << "(*this, " << minLength << ", ";
			std::cout << (var.size ? *(var.size) : longest) << ", ";
			std::cout << opt.width() << ", Full, " << opt.block() << ");";
			std::cout << "\t_x << " << var.name << ";";
			std::cout << "\t//at index:" << (parser::str_var_index.find(var.name)->second) << std::endl;
		}
		StringVar temp(
				*this,
				minLength,
				var.size ? *(var.size) : longest,
				opt.width(),
				Full,
				opt.block()
		);
		_x << temp;
	}
	if(opt.verbose())
		std::cout << "x = StringVarArray(*this,_x);\n";
	x = StringVarArray(*this,_x);
	
	// Int Variables:
	IntVarArgs _l;
	if(opt.verbose())
		std::cout << "IntVarArgs _l;\n";
	for(std::vector<std::string>::size_type i = 0; i < parser::int_var_table.size(); ++i) {
		parser::int_var_def var = parser::int_var_table[i];
		if (opt.verbose()) {
			std::cout << "IntVar " << var.name << "(*this, ";
			std::cout << (var.range ? var.range->min : opt.minlength()) << ", ";
			std::cout << (var.range ? var.range->max : opt.length()) << ");";
			std::cout << "\t_l << " << var.name << ";";
			std::cout << "\t//at index:" << (parser::int_var_index.find(var.name)->second) << std::endl;
		}
		IntVar temp(
				*this,
				var.range ? var.range->min : opt.minlength(),
				var.range ? var.range->max : opt.length()
		);
		_l << temp;
	}
	if(opt.verbose())
		std::cout << "l = IntVarArray(*this,_l);\n";
	l = IntVarArray(*this,_l);


	//Constraints:
	for(std::vector<parser::predicate_def>::size_type i = 0; i < parser::cstr_table.size(); ++i) {
		boost::apply_visitor(constraint_visitor(*this, opt.verbose()), parser::cstr_table[i].cstr);
	}
	
	if(opt.verbose())
		std::cout << "post_brancher(x,opt);\n";
  post_brancher(x,opt);
}


		
int main(int argc, char* argv[]) {
	
	using Gecode::DFS;
	
  Gecode::VarImpDisposer<Gecode::String::StringVarImp> disposer;
	FznOptions opt("flatzinc parser for gecode+strings");

  opt.solutions(1);
  opt.min(FZN2GECODE_OFFSET + 1);
  FznModel::standardOptions(opt);
  opt.parse(argc,argv);

	
	
		std::ifstream in(opt.instance(), std::ios_base::in);

		if (!in)
		{
				std::cerr << "Error: Could not open input file: "
						<< opt.instance() << std::endl;
				return 1;
		}

		std::string storage; // We will read the contents here.
		in.unsetf(std::ios::skipws); // No white space skipping!
		std::copy(
				std::istream_iterator<char>(in),
				std::istream_iterator<char>(),
				std::back_inserter(storage));

		clock_t pstart = clock();
		
		typedef parser::fzn_parser<std::string::const_iterator> fzn_parser;
		fzn_parser g; // Our grammar
		std::vector<parser::statement> ast;

		using boost::spirit::ascii::space;
		std::string::const_iterator iter = storage.begin();
		std::string::const_iterator end = storage.end();
		bool r = phrase_parse(iter, end, g, space, ast);

		clock_t pend = clock();
		double ptime = (pend - pstart) / static_cast<double>( CLOCKS_PER_SEC );
		std::cout << "parse time (s): "<< ptime << std::endl;
		
		if (r && iter == end)
		{
			std::cout << "-------------------------\n";
			std::cout << "\tParsing succeeded\n";
			std::cout << "-------------------------\n";
			
			if (opt.verbose()) {
				for (std::vector<parser::statement>::size_type i = 0; i < ast.size(); ++i) {
					std::cout << i << ": ";
					//parser::stmt_printer printer;
					//printer(ast[i]);
				}
				std::cout << "-------------------------\n";
			}
			
	    Script::run<FznInstance,DFS,FznOptions>(opt);
	  	return 0;
		}
		else
		{
				std::string::const_iterator some = iter+60;
				std::string context(iter, (some>end)?end:some);
				std::cout << "-------------------------\n";
				std::cout << "Parsing failed\n";
				std::cout << "stopped at: \"" << context << "...\"\n";
				std::cout << "-------------------------\n";
				return 1;
		}

    
}
