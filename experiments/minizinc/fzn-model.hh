#ifndef __STRING_MODEL_HH__
#define __STRING_MODEL_HH__

#include <iostream>

#include <gecode/driver.hh>
#include <gecode/minimodel.hh>
#include <gecode/string.hh>


using Gecode::String::Full;
using Gecode::String::Empty;
using Gecode::String::BitSet;
using Gecode::String::interof;
using Gecode::String::unionof;
using Gecode::String::singletonfor;

/// \brief %Options for %Golf example
class FznOptions : public Gecode::InstanceOptions {
	
protected:
  Gecode::Driver::IntOption _length; //< maximum string length
  Gecode::Driver::IntOption _block; //< blocksize for list representations
  Gecode::Driver::IntOption _width; //< width of the alphabet
  Gecode::Driver::IntOption _min; //< width of the alphabet
  Gecode::Driver::BoolOption _zero; // allow zero length strings
  Gecode::Driver::IntOption _size; // instance size
	Gecode::Driver::BoolOption _verbose;
public:
  /// Constructor
  FznOptions(const char* name)
    : InstanceOptions(name),
      _length("-length","maximum string length",32),
      _block("-block","blocksize for lists",16),
      _width("-width","width of the alphabet",63),
      _min("-min","smallest ascii value of any symbol", 32),
      _zero("-zero","allow zero length strings",true),
      _size("-size","instance size for benchmark",32),
			_verbose("-verbose","verbose",false) {
    add(_length);
    add(_block);
    add(_width);
    add(_min);
    add(_zero);
    add(_size);
		add(_verbose);
  }
  /// Return max string length
  int length(void) const { return _length.value(); }
  /// Return blocksize
  int block(void) const { return _block.value(); }
  /// Return alphabet width
  int width(void) const { return _width.value(); }
  /// Return min symbol ascii code
  int min(void) const { return _min.value(); }
  /// Return zero length flag
  bool zero(void) const { return _zero.value(); }
  /// Return least allowable length
  int minlength(void) const { return _zero.value()?0:1; }
  /// Return instance size
  int size(void) const { return _size.value(); }
  /// Return verbose flag
  bool verbose(void) const { return _verbose.value(); }
	
  /// set default min symbol ascii code
  void min(int m) { _min.value(m); }
  /// set instance size
  void size(int m) { _size.value(m); }
};

class FznModel : public Gecode::Script {
public:
  Gecode::IntVarArray l;
  Gecode::StringVarArray x;
  int offset;
  
  void post_brancher(Gecode::StringVarArgs y, const FznOptions& opt) {
		using namespace Gecode;
		
    switch(opt.branching()) {
    case BR_WMIN_SPLIT_MIN:
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_WMAX_SPLIT_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_MIN_SPLIT_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_SPLIT,String::LEN_MIN); break;
    case BR_WMIN_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_WMAX_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_MIN_SPLIT_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_SPLIT,String::LEN_MED); break;
    case BR_WMIN_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_WMAX_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_MIN_MIN_MIN: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_MIN,String::LEN_MIN); break;
    case BR_WMIN_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MIN,String::CHAR_VAL_MIN,String::LEN_MED); break;
    case BR_WMAX_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_WIDTH_MAX,String::CHAR_VAL_MIN,String::LEN_MED); break;
    case BR_MIN_MIN_MED: 
      mandatory(*this, y,String::VAR_NONE,String::IND_NONE,String::CHAR_VAL_MIN,String::LEN_MED); break;
		}
  }
  
public:
  enum {BR_WMIN_SPLIT_MIN, BR_WMAX_SPLIT_MIN, BR_MIN_SPLIT_MIN, 
        BR_WMIN_SPLIT_MED, BR_WMAX_SPLIT_MED, BR_MIN_SPLIT_MED, 
        BR_WMIN_MIN_MIN,   BR_WMAX_MIN_MIN,   BR_MIN_MIN_MIN, 
        BR_WMIN_MIN_MED,   BR_WMAX_MIN_MED,   BR_MIN_MIN_MED};
        
  
	FznModel(const FznOptions& opt)
    : Gecode::Script(opt), offset(opt.min() - 1) {}
	FznModel(bool share, FznModel& m)
	: Gecode::Script(share,m), offset(m.offset) {
    x.update(*this, share, m.x);
    l.update(*this, share, m.l);
	}

  static void standardOptions(FznOptions& opt) {
    opt.branching(FznModel::BR_WMIN_SPLIT_MIN, "widthmin-split-min", "branching: index - width min; val - split; length - min");
    opt.branching(FznModel::BR_WMAX_SPLIT_MIN, "widthmax-split-min", "branching: index - width max; val - split; length - min");
    opt.branching(FznModel::BR_MIN_SPLIT_MIN, "min-split-min", "branching: index - min; val - split; length - min");
    opt.branching(FznModel::BR_WMIN_SPLIT_MED, "widthmin-split-med", "branching: index - width min; val - split; length - med");
    opt.branching(FznModel::BR_WMAX_SPLIT_MED, "widthmax-split-med", "branching: index - width max; val - split; length - med");
    opt.branching(FznModel::BR_MIN_SPLIT_MED, "min-split-med", "branching: index - min; val - split; length - med");
    opt.branching(FznModel::BR_WMIN_MIN_MIN, "widthmin-min-min", "branching: index - width min; val - min; length - min");
    opt.branching(FznModel::BR_WMAX_MIN_MIN, "widthmax-min-min", "branching: index - width max; val - min; length - min");
    opt.branching(FznModel::BR_MIN_MIN_MIN, "min-min-min", "branching: index - min; val - min; length - min");
    opt.branching(FznModel::BR_WMIN_MIN_MED, "widthmin-min-med", "branching: index - width min; val - min; length - med");
    opt.branching(FznModel::BR_WMAX_MIN_MED, "widthmax-min-med", "branching: index - width max; val - min; length - med");
    opt.branching(FznModel::BR_MIN_MIN_MED, "min-min-med", "branching: index - min; val - min; length - med");
    opt.branching(FznModel::BR_WMIN_SPLIT_MIN);
  }
  
//   int char2val(char a) const {
//     if(a == ' ')
// 			return 63;
//     return ((int)a) - offset;
//   }
//
//   char val2char(int n) const {
// 		if(n == 63)
// 			return ' ';
//     return n + offset;
//   }
	
	int char2val(char a) const {
	    if(a == ' ')
				return 1;
			if(a == '=')
				return 2;
			if(a >= '0' && a <= '9')
				return a - 45;
			if(a >= 'A' && a <= 'Z')
				return a - 52;
			if(a >= 'a' && a <= 'z')
				return a - 84; // maps onto uppercase
	    return 63;
	  }
  
	  char val2char(int n) const {
	    if(n == 1)
				return ' ';
			if(n == 2)
				return '=';
			if(n >= 3 && n <= 12)
				return n + 45;
			if(n >= 13 && n <= 38)
				return n + 52;
	    return '!';
	  }

  Gecode::IntArgs str2IntArgs(std::string s) const {
    Gecode::IntArgs arg;
    for (size_t i = 0; i < s.length(); i++)
      arg << char2val(s[i]);
    return arg;
  }
	
	Gecode::IntArgs chars2IntArgs(std::vector<char>& s) const {
		Gecode::IntArgs arg;
		for(std::vector<char>::size_type i = 0; i < s.size(); i++) {
			arg << char2val(s[i]);
		}
		return arg;
	}
  
  std::string IntArg2str(Gecode::IntArgs arg) const {
    std::string s;
    for (int i = 0; i < arg.size(); i++)
      s[i] = val2char(arg[i]);
    return s;
  }
  
  BitSet alphabetof(const Gecode::DFA& dfa) const {
    BitSet alpha = Empty;
    for (Gecode::DFA::Symbols sym(dfa); sym(); ++sym) {
      alpha = unionof(alpha,singletonfor(sym.val()));
    }
    return alpha;
  }
  
	virtual Gecode::Space* copy(bool share) {
		return new FznModel(share,*this);
	}

	virtual void print(std::ostream& os) const {
    os << x << ", " << l << std::endl;
	}
};
#endif