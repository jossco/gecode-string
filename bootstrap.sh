#!/usr/bin/env bash

# copy any public keys into authorized_keys
for file in /vagrant/.ssh/*.pub
do
  grep -F -s -f $file ./.ssh/authorized_keys
  if [[ $? -ne 0 ]]; then
    cat $file >> ./.ssh/authorized_keys
  fi
done

apt-get update
# gecode/mzn dependencies:
apt-get -y install build-essential
apt-get -y install autoconf
apt-get -y install qt5-default
apt-get -y install bison flex cmake
# workflow stuff:
apt-get -y install python-pip python-dev
apt-get -y install tmux
#apt-get -y install mosh
apt-get -y install gdb valgrind libtclap-dev
apt-get -y install libboost-dev

pip install -q colout
pip install -q sh

# openFST:
apt-get -y install curl
#curl --silent http://openfst.org/twiki/pub/FST/FstDownload/openfst-1.5.0.tar.gz --output openfst-1.5.0.tar.gz
#tar xfz openfst-1.5.0.tar.gz
#cd openfst-1.5.0/
#./configure --enable-bin=no --enable-static=no
#make && make install
#cd ..

grep vagrant /etc/security/limits.conf
if [[ $? -ne 0 ]]; then
  cat <<EOF >> /etc/security/limits.conf
vagrant soft core 100000
vagrant hard core -1
EOF
fi

grep 'customized during provisioning' /home/vagrant/.bashrc
if [[ $? -ne 0 ]]; then
  cat <<'EOF' >> /home/vagrant/.bashrc
export HISTIGNORE="&:ls:ll:ls -* :[bf]g:exit:wget *:qget *"
export CLICOLOR=1

# OPTIONS (customized during provisioning)
set -o ignoreeof        # ctrl-D will not exit shell
shopt -s cdspell        # correct common cd spelling errors
shopt -s cmdhist        # multi-line commands stored in history as 1 line
#shopt -s dotglob       # path expansion includes files beginning with .
shopt -s extglob        # ksh-88 egrep-style file globbing
shopt -s histappend histreedit
shopt -s no_empty_cmd_completion
shopt -s nocaseglob

export PS1="\[\033[1;32m\][\$(date +%H:%M)][\u:\W]$\[\033[0m\] "
EOF
fi

grep 'tmux has-session' /home/vagrant/.bashrc
if [[ $? -ne 0 ]]; then
  cat <<'EOF' >> /home/vagrant/.bashrc
if [[ $- =~ "i" ]]; then
        if [[ -z "$TMUX" ]] ;then
                tmux has-session &> /dev/null
                if [ $? -eq 1 ]; then
                        exec tmux -2 -CC
                        exit
                else
                        exec tmux -2 -CC attach
                        exit
                fi
        fi
fi
EOF

# fix stupid Apple language snafu that breaks matplotlib:
grep 'LC_CTYPE=en_US.UTF-8' /home/vagrant/.profile
if [[ $? -ne 0 ]]; then
    cat <<'EOF' >> /home/vagrant.profile
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
EOF
fi

source ~/.bashrc
