#!/usr/bin/env bash

grep 'tmux has-session' /home/vagrant/.bashrc
if [[ $? -ne 0 ]]; then
  cat <<'EOF' >> /home/vagrant/.bashrc
if [[ $- =~ "i" ]]; then
        if [[ -z "$TMUX" ]] ;then
                tmux has-session &> /dev/null
                if [ $? -eq 1 ]; then
                        exec tmux -2 -CC
                        exit
                else
                        exec tmux -2 -CC attach
                        exit
                fi
        fi
fi
EOF

grep shell_integration /home/vagrant/.bashrc
if [[ $? -ne 0 ]]; then
  curl -L https://iterm2.com/misc/`basename $SHELL`_startup.in >> /home/vagrant/.iterm2_shell_integration.`basename $SHELL`
  cat <<'EOF' >> /home/vagrant/.bashrc
source /home/vagrant/.iterm2_shell_integration.`basename $SHELL`
EOF